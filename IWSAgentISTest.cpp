#include "IWSAgentIS.h"
#include "ErrorHandler.h"
#include "Constants.h"
#include <iostream>
/*
using namespace std;

const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
const DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
const string recipientCertStore = ADDRESSBOOK;
const string signerCertStore = MY;
const string digestArithmetic = SHA256;
const string symmetryArithmetic = TDES;
const DWORD recipientCertIndex = 7;
const DWORD signerCertIndex = 0;
const string recipientDefaultDN = "FR, Ldf, Paris, Infosec, Dpt_Buz, User Client Encryption Infosec, enc@infosec.com";
const string signerDefaultDN = "FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com";

const string form1 = "Infosec form1";
const string form2 = "Infosec form2";

const string filePath1 = "example.txt";
const string filePath2 = PRIVATE_KEY_RSA_1;

const string containerName = "testContainer";
const string csp = MS_KEY_STORAGE_PROVIDER_STR;

string plainText;
string signedText;
string envelopedMsg;
string certDN;
string signerCertDN;
string recipientCertDN;
DWORD* errorCode = new DWORD();

IWSAgentIS agent = IWSAgentIS();

void cleanUp();

void testIWSAGetVersionIS(){

	cout << "Welcome to NetSignCNGIS.exe program. Version:" << agent.IWSAGetVersionIS() << endl;
}

void testIWSAFormFileSignIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileSignIS(errorCode, signedText, signerCertIndex, digestArithmetic);
	if (*errorCode || 0==signedText.size()) { MyCertHandleError("[x] FormFileSign."); }
	cleanUp();
}

void testIWSAFormFileSignEmptyIS() {

	agent.IWSAFormFileSignIS(errorCode, signedText, signerCertIndex, digestArithmetic);
	if (*errorCode || 0 == signedText.size()) { MyCertHandleError("[x] FormFileSign."); }
	cleanUp();
}

void testIWSAFormFileSignEmptyFormIS() {

	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileSignIS(errorCode, signedText, signerCertIndex, digestArithmetic);
	if (*errorCode || 0 == signedText.size()) { MyCertHandleError("[x] FormFileSign."); }
	cleanUp();
}

void testIWSAFormFileSignEmptyFileIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAFormFileSignIS(errorCode, signedText, signerCertIndex, digestArithmetic);
	if (*errorCode || 0 == signedText.size()) { MyCertHandleError("[x] FormFileSign."); }
	cleanUp();
}

void testIWSAFormFileSignSingleFileIS() {

	agent.IWSAAddFileIS(filePath1);
	agent.IWSAFormFileSignIS(errorCode, signedText, signerCertIndex, digestArithmetic);
	if (*errorCode || 0 == signedText.size()) { MyCertHandleError("[x] FormFileSign."); }
	cleanUp();
}

void testIWSAFormFileSignSingleFormIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAFormFileSignIS(errorCode, signedText, signerCertIndex, digestArithmetic);
	if (*errorCode || 0 == signedText.size()) { MyCertHandleError("[x] FormFileSign."); }
	cleanUp();
}

void testIWSAFormFileSignDefaultDNIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileSignDefaultDNIS(errorCode, signedText, signerDefaultDN, digestArithmetic);
	if (*errorCode || 0 == signedText.size()) { MyCertHandleError("[x] FormFileSign."); }
	cleanUp();
}

void testIWSAFormFileVerifyIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileSignIS(errorCode, signedText, signerCertIndex, digestArithmetic);
	if (*errorCode || 0 == signedText.size()) { MyCertHandleError("[x] FormFileSign."); }
	agent.IWSAFormFileVerifyIS(errorCode, signerCertDN, signedText);
	if (*errorCode || 0 == signerCertDN.size()) { MyCertHandleError("[x] FormFileSign."); }
	cleanUp();
}

void testIWSAFormFileEncryptedEnvelopIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileEncryptedEnvelopIS(errorCode, envelopedMsg, recipientCertIndex, symmetryArithmetic);
	if (*errorCode || 0 == envelopedMsg.size()) { MyCertHandleError("[x] FormFileEncryptedEnvelope."); }

	cleanUp();
}

void testIWSAFormFileEncryptedEnvelopDefaultDNIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileEncryptedEnvelopDefaultDNIS(errorCode, envelopedMsg, recipientDefaultDN, symmetryArithmetic);
	if (*errorCode || 0 == envelopedMsg.size()) { MyCertHandleError("[x] FormFileEncryptedEnvelope."); }

	cleanUp();
}

void testIWSAFormFileDecryptEnvelopIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileEncryptedEnvelopIS(errorCode, envelopedMsg, recipientCertIndex, symmetryArithmetic);
	if (*errorCode || 0 == envelopedMsg.size()) { MyCertHandleError("[x] FormFileEncryptedEnvelope."); }
	agent.IWSAFormFileDecryptEnvelopIS(errorCode, recipientCertDN, envelopedMsg);
	if (*errorCode || 0 == recipientCertDN.size()) { MyCertHandleError("[x] FormFileDecryptEnvelop."); }

	cleanUp();
}

void testIWSAFormFileEncryptedSignEnvelopIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileEncryptedSignEnvelopIS(errorCode, envelopedMsg, signerCertIndex,recipientCertIndex, digestArithmetic, symmetryArithmetic);
	if (*errorCode || 0 == envelopedMsg.size()) { MyCertHandleError("[x] FormFileEncryptedSignEnvelope."); }

	cleanUp();
}

void testIWSAFormFileEncryptedSignEnvelopDefaultDNIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileEncryptedSignEnvelopDefaultDNIS(errorCode, envelopedMsg, signerDefaultDN, recipientDefaultDN, digestArithmetic, symmetryArithmetic);
	if (*errorCode || 0 == envelopedMsg.size()) { MyCertHandleError("[x] FormFileEncryptedSignEnvelopeDefaultDN."); }

	cleanUp();
}

void testIWSAFormFileDecryptEnvelopSignIS() {

	agent.IWSAAddFormIS(form1);
	agent.IWSAAddFormIS(form2);
	agent.IWSAAddFileIS(filePath1);
	agent.IWSAAddFileIS(filePath2);
	agent.IWSAFormFileEncryptedSignEnvelopIS(errorCode, envelopedMsg, signerCertIndex, recipientCertIndex, digestArithmetic, symmetryArithmetic);
	if (*errorCode || 0 == envelopedMsg.size()) { MyCertHandleError("[x] FormFileEncryptedSignEnvelope."); }

	agent.IWSAFormFileDecryptSignEnvelopIS(errorCode, signerCertDN, recipientCertDN, envelopedMsg);
	if (*errorCode || 0 == recipientCertDN.size() || 0 == signerCertDN.size()) { MyCertHandleError("[x] FormFileDecryptSignEnvelop."); }

	cleanUp();
}

void testIWSAGetAllCertsListInfoIS() {

	//cout << agent.IWSAGetAllCertsListInfoIS("", "Sign", 0) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "Encrypt", 0) << endl;
	//cout << agent.IWSAGetAllCertsListInfoIS("", "SignAndEncrypt", 0) <<endl;
}

void testIWSAGetAllCertsListInfoFilteredIS() {

	cout << agent.IWSAGetAllCertsListInfoIS("", "Sign", 2) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "Encrypt", 2) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "SignAndEncrypt", 2) << endl;
}

void testIWSAGetAllCertsListInfoDefaultDNIS() {

	string defaultDNs = signerDefaultDN + "|" + recipientDefaultDN;
	cout << agent.IWSAGetAllCertsListInfoByCertDNIS("", "SignAndEncrypt", defaultDNs,0 ) << endl;
}

void testIWSAGetAllCertsListInfoByIssuerDNAndCertSNIS() {

	string issuerDN = "FR, Ldf, Paris, Infosec, Administration, Pavle Chang, chang@infosec.com";
	string certSN = "5"; // not 05

	cout<< agent.IWSAGetAllCertsListInfoByIssuerDNAndCertSNIS("", "SignAndEncrypt", issuerDN, certSN, 0)<<endl;

}

void testIWSASetCertsListInfoCacheIS() {
	if (0 != agent.IWSASetCertsListInfoCacheIS(TRUE)) { MyCertHandleError("[x] testIWSASetCertsListInfoCacheIS."); }
}

void testIWSASetCertsListInfoCacheToGetAllCertsListInfoIS() {
	if (0 != agent.IWSASetCertsListInfoCacheIS(TRUE)) { MyCertHandleError("[x] testIWSASetCertsListInfoCacheIS."); }
	cout << agent.IWSAGetAllCertsListInfoIS("", "Sign", 2) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "Encrypt", 2) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "Sign", 2) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "Sign", 2) << endl;

	if (0 != agent.IWSASetCertsListInfoCacheIS(FALSE)) { MyCertHandleError("[x] testIWSASetCertsListInfoCacheIS."); }
	cout << agent.IWSAGetAllCertsListInfoIS("", "Sign", 2) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "Encrypt", 2) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "Sign", 2) << endl;
	cout << agent.IWSAGetAllCertsListInfoIS("", "Sign", 2) << endl;

}

void testIWSAGetCertInfoForIndexIS() {
	cout << agent.IWSAGetCertInfoForIndexIS(0) << endl;
}

void testIWSAGetCertPublicKeyInfoForIndexIS() {
	cout << agent.IWSAGetCertPublicKeyInfoForIndexIS(0) << endl;
}

void testIWSASetPlanTextConvertModeIS() {
	agent.IWSASetPlanTextConvertModeIS("\r\n");
	if ("\r\n" != agent.strMode) { MyCertHandleError("[x] testIWSASetPlanTextConvertModeIS."); }
	agent.IWSASetPlanTextConvertModeIS("\n");
}

void testIWSA_rsa_csp_listProviderIS() {
	cout << agent.IWSA_rsa_csp_listProviderIS()<<endl;
}

void testIWSA_rsa_csp_getCountOfCertIS(){
	cout << agent.IWSA_rsa_csp_getCountOfCertIS() << endl;
}

void testIWSA_rsa_csp_getCertInfoIS() {
	cout << agent.IWSA_rsa_csp_getCertInfoIS(0)<<endl;
}

void testIWSA_rsa_csp_AdvgenContainerP10() {
	string p10Value;
	string encKeyPair;
	string keySize = "1024";
	const string testdn = "C = FR, S = Ldf, O = Infosec, OU = Dpt_Test, CN = Test p10, E = p10@infosec.com";
	agent.IWSA_rsa_csp_AdvgenContainerP10IS(errorCode, p10Value, encKeyPair, keySize, testdn, FALSE);
}

void testShowSubjectFormats() {
	agent.showSubjectFormats();
}

void testIWSA_rsa_csp_GenSignCertp7() {
	cout << agent.IWSA_rsa_csp_GenSignCertp7() <<endl;
}

void testIWSA_rsa_csp_GenEncCertp7() {
	cout << agent.IWSA_rsa_csp_GenEncCertp7() << endl;
}

void testIWSA_rsa_csp_GenEncPrikey() {
	cout << agent.IWSA_rsa_csp_GenEncPrikey() << endl;
}

void testIWSA_rsa_csp_GenUKEK() {
	cout << agent.IWSA_rsa_csp_GenUKEK() << endl;
}

void testIWSA_rsa_csp_AdvImportSignEncCertIS() {
	const string certType = "P7";
	string encPriKey = agent.IWSA_rsa_csp_GenEncPrikey();
	string ukek = agent.IWSA_rsa_csp_GenUKEK();
	string signCert = agent.IWSA_rsa_csp_GenSignCertp7();
	string encCert = agent.IWSA_rsa_csp_GenEncCertp7();
	agent.IWSA_rsa_csp_AdvImportSignEncCertIS(errorCode, certType, signCert, encCert, encPriKey, ukek);
}

void testIWSA_rsa_csp_setProviderIS() {
	agent.IWSA_rsa_csp_setProviderIS(MS_KEY_STORAGE_PROVIDER_STR);
}

void testenumContainers() {
	agent.enumContainers();
}

void testIWSA_rsa_csp_createContainerIS() {
	agent.IWSA_rsa_csp_createContainerIS(containerName);
	agent.enumContainers();
}

void testIWSA_rsa_csp_deleteContainerIS() {
	agent.IWSA_rsa_csp_deleteContainerIS(containerName);
}

void testIWSA_rsa_csp_deleteContainerCspIS() { // only once
	string issuerDN = "C=FR, S=Ldf, L=Paris, O=Infosec, OU=Administration, CN=Pavle Chang, E=chang@infosec.com";
	string containerName = issuerDN + "|2";
	agent.IWSA_rsa_csp_deleteContainerIS(csp, containerName);
}

void testIWSA_rsa_csp_genContainerP10IS() {
	const string testdn = "C = FR, S = Ldf, O = Infosec, OU = Dpt_Test, CN = Test p10, E = p10@infosec.com";
	string container, p10;
	cout << agent.IWSA_rsa_csp_genContainerP10IS(errorCode, container, p10, TRUE, "1024", testdn, SHA256, "", RSASHA256, TRUE, TRUE)<<endl;
	cout << agent.IWSA_rsa_csp_genContainerP10IS(errorCode, container, p10, TRUE, "1024", testdn, MD5, "", RSASHA1, TRUE, TRUE) << endl;
	cout << agent.IWSA_rsa_csp_genContainerP10IS(errorCode, container, p10, FALSE, "1024", testdn, SHA256, RSA, "", TRUE, TRUE) << endl;
}

void testIWSA_rsa_csp_genP10IS() {
	const string testdn = "C = FR, S = Ldf, O = Infosec, OU = Dpt_Test, CN = Test p10, E = p10@infosec.com";
	string container = "testContainerGenP10", p10;
	cout << agent.IWSA_rsa_csp_genP10IS(errorCode, p10, container, TRUE, "1024", testdn, SHA256, "", RSASHA256, TRUE, TRUE) << endl;
}

void testIWSA_rsa_csp_genEncKeyPairIS() {
	string data;
	agent.IWSA_rsa_csp_genEncKeyPairIS(errorCode,data);
	cout << data << endl;
	if (*errorCode) { MyCertHandleError("[x] Error GenEncKeyPairIS."); }
}

void testIWSA_rsa_csp_delEncKeyPairIS() {
	string data;
	agent.IWSA_rsa_csp_genEncKeyPairIS(errorCode, data);
	if (*errorCode) { MyCertHandleError("[x] Error GenEncKeyPairIS."); }
	if (agent.IWSA_rsa_csp_delEncKeyPairIS()) { MyCertHandleError("[x] Error DelEncKeyPairIS."); }
}

void testEnumCertificateStores() {
	agent.enumCertificateStores();
}

void testIWSA_rsa_csp_importX509CertToStoreIS() {
	string x509Cert = agent.IWSAGetCertInfoForIndexBase64(0);
	string x509CertBak = "MIIE3TCCA8WgAwIBAgIBBTANBgkqhkiG9w0BAQsFADCBjjELMAkGA1UEBhMCRlIxDDAKBgNVBAgMA0xkZjEOMAwGA1UEBwwFUGFyaXMxEDAOBgNVBAoMB0luZm9zZWMxFzAVBgNVBAsMDkFkbWluaXN0cmF0aW9uMRQwEgYDVQQDDAtQYXZsZSBDaGFuZzEgMB4GCSqGSIb3DQEJARYRY2hhbmdAaW5mb3NlYy5jb20wHhcNMjAwOTExMTMxNjE5WhcNMjEwOTExMTMxNjE5WjB7MQswCQYDVQQGEwJGUjEMMAoGA1UECAwDTGRmMRAwDgYDVQQKDAdJbmZvc2VjMRAwDgYDVQQLDAdEcHRfQnV6MRkwFwYDVQQDDBBVc2VyIENsaWVudCBTaWduMR8wHQYJKoZIhvcNAQkBFhBzaW5nQGluZm9zZWMuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA71sva9920N88AHxe2DfWCb4/2UbBZXEIYeTMXdL+2D30yCmr0hLKDWaMF8oPs8jgAWrSlLM6adM1qMBQC1wsFOYW5GpPVoQ0+umGApv6KVC/pT8ICSk1Ug8cGXSCb6mDJxHndbk39KD4AdphjBPCRq9wpzhpseReF59SeLfQhjf6fQd1Dt30On+nKVYm2XQes13raMtVKBt+YPgiz1e7xxCNdjpVes2aot9Vys3VDqoQ4BZLdrdExAztAUsC7gzU3N0TsRv8SvKi+dUk0EEsSUWykH5xqLwgo1EvZ2sIpiN4ag7qC9OZ40HxmiBPnpuneCg28LPAbDCO4VlgFrffaQIDAQABo4IBVjCCAVIwCQYDVR0TBAIwADARBglghkgBhvhCAQEEBAMCB4AwMwYJYIZIAYb4QgENBCYWJE9wZW5TU0wgR2VuZXJhdGVkIENsaWVudCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUxkL7htl0qwh8Y1OF8zYk2xsfKXowgbgGA1UdIwSBsDCBraGBlKSBkTCBjjELMAkGA1UEBhMCRlIxDDAKBgNVBAgMA0xkZjEOMAwGA1UEBwwFUGFyaXMxEDAOBgNVBAoMB0luZm9zZWMxFzAVBgNVBAsMDkFkbWluaXN0cmF0aW9uMRQwEgYDVQQDDAtQYXZsZSBDaGFuZzEgMB4GCSqGSIb3DQEJARYRY2hhbmdAaW5mb3NlYy5jb22CFHwgHERiBczNCdZwD2iyUoqqLkiBMA4GA1UdDwEB/wQEAwIHgDATBgNVHSUEDDAKBggrBgEFBQcDAjANBgkqhkiG9w0BAQsFAAOCAQEAOkVH/uV+Mn7JnM4amSKtYCR/kS1AqhxIeF4h7Wzj8Lgbmaf5/bWxe8roCVSUEBVm58aEwCq0xI74Rbd1e37NkGUBZms5X1Vock15aPfCBosEg3wW4T1oR1Hjtd+gMaEOSDxupsLPbDZPHgpSxACdqCfZ8zFaG3W6i9fdvgmydu/On/0X1d4v0rHxTS6oWilPmCi7BwSQCuxLZ4gG7kX9HIRfVN+0EyEcF2+iGjSNpI1p8EAkigZx3+0PX8f0NTNU4P6fp77mTG12dDlHt+GJPax3IN51pkdkFxLJFgd5wsvfTtm2FopbNSIA3n6qN9rhjeBfMGG76AfegwFnNOEdpQ==";
	agent.IWSA_rsa_csp_importX509CertToStoreIS(x509Cert,FALSE);
}

void testIWSA_rsa_csp_delX509CertInStoreIS() {
	string issuerDN = "FR, Ldf, Paris, Infosec, Administration, Pavle Chang, chang@infosec.com";
	string certSN = "5"; // not 05
	agent.IWSA_rsa_csp_delX509CertInStoreIS(issuerDN, certSN);
	string x509CertBak = "MIIE3TCCA8WgAwIBAgIBBTANBgkqhkiG9w0BAQsFADCBjjELMAkGA1UEBhMCRlIxDDAKBgNVBAgMA0xkZjEOMAwGA1UEBwwFUGFyaXMxEDAOBgNVBAoMB0luZm9zZWMxFzAVBgNVBAsMDkFkbWluaXN0cmF0aW9uMRQwEgYDVQQDDAtQYXZsZSBDaGFuZzEgMB4GCSqGSIb3DQEJARYRY2hhbmdAaW5mb3NlYy5jb20wHhcNMjAwOTExMTMxNjE5WhcNMjEwOTExMTMxNjE5WjB7MQswCQYDVQQGEwJGUjEMMAoGA1UECAwDTGRmMRAwDgYDVQQKDAdJbmZvc2VjMRAwDgYDVQQLDAdEcHRfQnV6MRkwFwYDVQQDDBBVc2VyIENsaWVudCBTaWduMR8wHQYJKoZIhvcNAQkBFhBzaW5nQGluZm9zZWMuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA71sva9920N88AHxe2DfWCb4/2UbBZXEIYeTMXdL+2D30yCmr0hLKDWaMF8oPs8jgAWrSlLM6adM1qMBQC1wsFOYW5GpPVoQ0+umGApv6KVC/pT8ICSk1Ug8cGXSCb6mDJxHndbk39KD4AdphjBPCRq9wpzhpseReF59SeLfQhjf6fQd1Dt30On+nKVYm2XQes13raMtVKBt+YPgiz1e7xxCNdjpVes2aot9Vys3VDqoQ4BZLdrdExAztAUsC7gzU3N0TsRv8SvKi+dUk0EEsSUWykH5xqLwgo1EvZ2sIpiN4ag7qC9OZ40HxmiBPnpuneCg28LPAbDCO4VlgFrffaQIDAQABo4IBVjCCAVIwCQYDVR0TBAIwADARBglghkgBhvhCAQEEBAMCB4AwMwYJYIZIAYb4QgENBCYWJE9wZW5TU0wgR2VuZXJhdGVkIENsaWVudCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUxkL7htl0qwh8Y1OF8zYk2xsfKXowgbgGA1UdIwSBsDCBraGBlKSBkTCBjjELMAkGA1UEBhMCRlIxDDAKBgNVBAgMA0xkZjEOMAwGA1UEBwwFUGFyaXMxEDAOBgNVBAoMB0luZm9zZWMxFzAVBgNVBAsMDkFkbWluaXN0cmF0aW9uMRQwEgYDVQQDDAtQYXZsZSBDaGFuZzEgMB4GCSqGSIb3DQEJARYRY2hhbmdAaW5mb3NlYy5jb22CFHwgHERiBczNCdZwD2iyUoqqLkiBMA4GA1UdDwEB/wQEAwIHgDATBgNVHSUEDDAKBggrBgEFBQcDAjANBgkqhkiG9w0BAQsFAAOCAQEAOkVH/uV+Mn7JnM4amSKtYCR/kS1AqhxIeF4h7Wzj8Lgbmaf5/bWxe8roCVSUEBVm58aEwCq0xI74Rbd1e37NkGUBZms5X1Vock15aPfCBosEg3wW4T1oR1Hjtd+gMaEOSDxupsLPbDZPHgpSxACdqCfZ8zFaG3W6i9fdvgmydu/On/0X1d4v0rHxTS6oWilPmCi7BwSQCuxLZ4gG7kX9HIRfVN+0EyEcF2+iGjSNpI1p8EAkigZx3+0PX8f0NTNU4P6fp77mTG12dDlHt+GJPax3IN51pkdkFxLJFgd5wsvfTtm2FopbNSIA3n6qN9rhjeBfMGG76AfegwFnNOEdpQ==";
	agent.IWSA_rsa_csp_importX509CertToStoreIS(x509CertBak, FALSE);
}

void testIWSA_rsa_csp_importSignX509CertIS() {
	string container = "signx509";
	//string x509Cert = "";
	//testIWSAGetAllCertsListInfoIS();
	//string x509Cert = agent.IWSAGetCertInfoForIndexBase64(3);
	// C=FR, S=Ldf, O=Infosec, OU=Dpt_Buz, CN=User Client Sign, E=sing@infosec.com
	string x509CertStr = "MIID0DCCArigAwIBAgIUaQkXThLedQzuQIRPaDCa7biS1hMwDQYJKoZIhvcNAQELBQAwgY4xCzAJBgNVBAYTAkZSMQwwCgYDVQQIDANMZGYxDjAMBgNVBAcMBVBhcmlzMRAwDgYDVQQKDAdJbmZvc2VjMRcwFQYDVQQLDA5BZG1pbmlzdHJhdGlvbjEUMBIGA1UEAwwLUGF2bGUgQ2hhbmcxIDAeBgkqhkiG9w0BCQEWEWNoYW5nQGluZm9zZWMuY29tMB4XDTIwMDkyNjExNTgxOVoXDTIxMDkyNjExNTgxOVowgZgxCzAJBgNVBAYTAkZSMQwwCgYDVQQIDANMZGYxDjAMBgNVBAcMBVBhcmlzMRAwDgYDVQQKDAdJbmZvc2VjMRAwDgYDVQQLDAdEcHRfQnV6MScwJQYDVQQDDB5Vc2VyIENsaWVudCBFbmNyeXB0aW9uIEluZm9zZWMxHjAcBgkqhkiG9w0BCQEWD2VuY0BpbmZvc2VjLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANfVshAYjmOxQi0qADdnk5DoGXD55PcYSJWq1d2RTLw8LfVCz0XvdXa21XrfXU/JP5qoNVlsBYx2OBTlQKNAxM8XC0i4h1LCO1v+oLV/Q7kM3Xf08nZZqTg3L8Eyj60Gpc+etUyOjoQpCc2m2yRrPvmUcZHWPGmu2UUsL57WSrw5uZn8L8X9OsJE8ZGsXmMyLDAvivxCAYgjDdbR+RmJ/qkbNDA3exTImDFfcrZIcwtEV0eF3VwLBN1oG3Nilp5DDRF8SKEPQAiXUOXARUr+cFH1h+LnHjmFBeZWrXM9QfBIJMv2f0O0/+oc+UyjUlnPfJzk4xKAA32qMa0Cjsfyhz8CAwEAAaMaMBgwCQYDVR0TBAIwADALBgNVHQ8EBAMCBBAwDQYJKoZIhvcNAQELBQADggEBAJVNspcGh0MaUd2KckKDtYRLebgbZsPX38DkAuc0cbTOM7SbosV9oi/t6vfhn0nABF/KYEHMIZUR5Z5uMjxLpg3bmVIf6D7m244SwbUWljGB2uVQtQgwMHqIZf3jnKrJ+G0RVt1ps1rArPjFMH8PLGh//R/cUTo3lI/+xVE3DUPfkM+JzBoIXVqNTQsCui9gEswKVv+tCUaoAtCx7pozijAgF3aAmk1HiJFpwB1KKLBu9TnE/HDNS6E/g4iDh6aEjF+i1cTI/BhxEDWtp0VDtjCJVM70pYigiOUuN0tMHAjlJ1UJ/PgFG94XN8G+k3Td3e91Lzb6OTGByq3oGlbM4zo=";
	agent.IWSA_rsa_csp_createContainerIS(container);
	agent.IWSA_rsa_csp_importSignX509CertIS(container, x509CertStr);
}

void cleanUp() {
	plainText="";
	signedText = "";
	envelopedMsg = "";
	certDN = "";
	signerCertDN = "";
	recipientCertDN = "";
	*errorCode=-1;
	agent.IWSAClearFileListIS();
	agent.IWSAClearFormListIS();
	agent = IWSAgentIS();
}
/*
void testImportCerts() {
	
	Avant de lancer ce programme, assuez que vos magasins de certificats sont tous vides. N'importe CA, ROOT, MY ou ADDRESSEBOOK.
	Le programme ne s'adresse qu'aux utilisateurs actuels.
	1, Importer les certificats root dans le magasin racine.
	2, Importer les certificats ca dans le magasin interm�diaire.
	3, Importer les sign certificats dans le magasin MY.
	4, Importer les autres certificats dans le magasin ADDRESSBOOK. Les certificats sont normalement les certificats du chiffrement et ceux du d�chiffrement de l'autre c�t�, les certificats de la v�rification de la signature de l'autre c�t�.
	

	IWSAgentIS agent;
	DWORD* errorCode = new DWORD();
	string dir = ".\\Certs\\certs\\";
	string rootCert = agent.readCrttoBase64(dir + "rootCrt.pem");
	string caCert = agent.readCrttoBase64(dir + "ca\\certs\\caCrt.pem");
	string encryptContainer = "encrypt";
	string encryptCert = agent.readCrttoBase64(dir + "encryptCrt.pem");
	string encryptKey = agent.readPriKeytoBase64(dir + "encrypt.pem");
	string signContainer = "sign";
	string signCert = agent.readCrttoBase64(dir + "signCrt.pem");
	string signP7Container = "signP7";
	string signP7Cert = agent.readCrttoBase64(dir + "signP7.pem");
	string encryptP7Container = "encryptP7";
	string encryptP7Cert = agent.readCrttoBase64(dir + "encryptP7.pem");
	string ukek = agent.IWSA_rsa_csp_GenUKEK();
	string pfxContent;
	string pfxContainer = "encrypt";
	string issuerDN = "FR, Ldf, Paris, Infosec, Dpt_CA, Infosec CA, ca@infosec.com";
	string certSN = "2";
	string password = "123456";
	*/
	/* 
	Importer ensemble les certificats suivants:

	agent.IWSA_rsa_csp_importX509CertToStoreIS(rootCert,TRUE);
	agent.IWSA_rsa_csp_importX509CertToStoreIS(caCert, FALSE);
	agent.IWSA_rsa_csp_importEncX509CertIS(encryptContainer,encryptCert,encryptKey,ukek,TRUE,TRUE);
	agent.IWSA_rsa_csp_importSignX509CertIS(signContainer,signCert);
	*/

	/*
	Importer individuellement les certificats p7:
	
	Importer un seul certificat p7 comme tous
	agent.IWSA_rsa_csp_importSignP7CertIS(signP7Container, signP7Cert);
	Importer un seul certificat p7 comme tous
	agent.IWSA_rsa_csp_importEncP7CertIS(encryptP7Container, encryptP7Cert, encryptKey, ukek, TRUE, TRUE);
	*/

	/*
	Importer un certificat de chiffrement et celui de signature en m�me temps avec une api avanc�e. Les certificats peuvent �tre X509 ou P7.

	agent.IWSA_rsa_csp_AdvImportSignEncCertIS(errorCode, CERT_TYPE_P7, signP7Cert, encryptP7Cert, encryptKey, ukek);
	agent.IWSA_rsa_csp_AdvImportSignEncCertIS(errorCode, CERT_TYPE_X509, signCert, encryptCert, encryptKey, ukek);
	*/

	/*
	Exporter les certificats:
	
	cout << agent.IWSA_rsa_csp_exportEncX509CertIS(encryptContainer) << endl;
	cout << agent.IWSA_rsa_csp_exportSignX509CertIS(signContainer) << endl;
	agent.IWSA_rsa_csp_exportPfxCertIS(errorCode, pfxContent, issuerDN, certSN, password, TRUE);
	cout << pfxContent << endl;
	agent.IWSA_rsa_csp_exportContainerPfxCertIS(errorCode, pfxContent, pfxContainer, FALSE, password, TRUE);
	cout << pfxContent << endl;
	

}*/
/*
void testP10() {
	
	Ce programme va cr�er les requ�tes P10 qui sont notamment les csr fichiers.
	1, D�signer le nom du conteneur lorsque de la cr�ation.
	2, Ne D�signer pas le nom du conteneur lorsque de la cr�ation.
	

	IWSAgentIS agent;
	string container1 = "p10Container1", container2;
	string p10, p10_2, p10_3;
	DWORD* errorCode = new DWORD();
	string dn = "C = FR, S = Ldf, O = Infosec, OU = Dpt_Test, CN = Test Sign p10, E = p10Sign@infosec.com";
	string dn2 = "C = FR, S = Ldf, O = Infosec, OU = Dpt_Test, CN = Test Sign p10 2, E = p10Sign2@infosec.com";
	string dn3 = "C = FR, S = Ldf, O = Infosec, OU = Dpt_Test, CN = Test Sign p10 3, E = p10Sign3@infosec.com";
	string encKeyPair;
	// Cr�er un P10 sign
	agent.IWSA_rsa_csp_genP10IS(errorCode, p10, container1, TRUE, KEY_SIZE_1024, dn, SHA256, RSA, RSASHA256, TRUE, FALSE);
	cout << p10 << endl;
	agent.IWSA_rsa_csp_genContainerP10IS(errorCode, container2, p10_2, TRUE, KEY_SIZE_1024, dn2, SHA256, RSA, RSASHA256, TRUE, FALSE);
	cout << container2 << endl << p10_2 << endl;
	agent.IWSA_rsa_csp_AdvgenContainerP10IS(errorCode, p10_3, encKeyPair, KEY_SIZE_1024, dn3, TRUE);
	cout << encKeyPair << endl << p10_3 << endl;
}*/
/*
void testContainer() {
	
	Ce programme va cr�er des conteneurs:
	1, Librement sans aucune contrainte. How is the container name generated ?
	2, Avec une cl� du conteneur.
	Et puis, le programme va supprimer:
	1, Ne que le conteneur.
	2, Le conteneur ainsi que son certificat. Les certificats sont stock�s comme un conteneur de cl� et un certificat. 
	
	IWSAgentIS agent;
	string container1, container2 = "container2", encryptContainer = "encrypt", signContainer = "sign";
	DWORD* errorCode = new DWORD();
	// agent.IWSA_rsa_csp_genContainerIS(errorCode, container1);
	agent.IWSA_rsa_csp_createContainerIS(container2);
	agent.IWSA_rsa_csp_deleteContainerIS(container2);
	agent.IWSA_rsa_csp_deleteContainerIS(MS_KEY_STORAGE_PROVIDER_STR, signContainer);
	agent.IWSA_rsa_csp_deleteContainerIS(MS_KEY_STORAGE_PROVIDER_STR, encryptContainer);
}*/
/*
void testKeyPair() {
	
	Ce programme va cr�er un rsa key pair et puis le supprimer.
	Tout le processus est fait dans un champ de l'object BCRYPT_KEY_HANDLE hEncKeyPair;
	
	DWORD* errorCode = new DWORD();
	string data;
	agent.IWSA_rsa_csp_genEncKeyPairIS(errorCode, data);
	cout << data << endl;
	agent.IWSA_rsa_csp_delEncKeyPairIS();
}
*/