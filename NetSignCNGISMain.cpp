﻿#include "IWSAgentISTest.h"
#include "TestHeader.h"
#include <windows.h>
#include <wincrypt.h>
#include <cryptuiapi.h>
#include <tchar.h>
#include <iostream>
#include "TestGmp.h"
#include "NetSignCNGISServer.h"




#pragma comment (lib, "crypt32.lib")
#pragma comment (lib, "cryptui.lib")

#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
using namespace std;

void maintest2();
// 
int mainNetSignCNGTest() {

   cout << "Welcome to NetSignCNGIS.exe program." << endl;
   /*
   testIWSAGetVersionIS();
   testIWSAFormFileSignIS();
   testIWSAFormFileSignDefaultDNIS();
   testIWSAFormFileSignEmptyIS();
   testIWSAFormFileSignEmptyFormIS();
   testIWSAFormFileSignEmptyFileIS();
   testIWSAFormFileSignSingleFileIS();
   testIWSAFormFileSignSingleFormIS();
   testIWSAFormFileVerifyIS();
   testIWSAFormFileEncryptedEnvelopIS();
   testIWSAFormFileEncryptedEnvelopDefaultDNIS();
   testIWSAFormFileDecryptEnvelopIS();
   testIWSAFormFileEncryptedSignEnvelopIS();
   testIWSAFormFileEncryptedSignEnvelopDefaultDNIS();
   testIWSAFormFileDecryptEnvelopSignIS();
   testIWSAGetAllCertsListInfoIS();
   testIWSAGetAllCertsListInfoFilteredIS();
   testIWSAGetAllCertsListInfoDefaultDNIS();
   testIWSAGetAllCertsListInfoByIssuerDNAndCertSNIS();

   testIWSASetCertsListInfoCacheIS();
   testIWSASetCertsListInfoCacheToGetAllCertsListInfoIS();
   testIWSAGetCertInfoForIndexIS();
   testIWSAGetCertPublicKeyInfoForIndexIS();
   testIWSASetPlanTextConvertModeIS();
   */
   //testIWSA_rsa_csp_listProviderIS();
   //testIWSA_rsa_csp_getCountOfCertIS();
   //testIWSA_rsa_csp_getCertInfoIS();
   //testShowSubjectFormats();
   //testIWSA_rsa_csp_AdvgenContainerP10();

   //testIWSAGetAllCertsListInfoIS();
  // testIWSA_rsa_csp_GenSignCertp7();
   //testIWSA_rsa_csp_GenEncCertp7();
  //testIWSA_rsa_csp_GenEncPrikey();
   //testIWSA_rsa_csp_GenUKEK();

 //  testIWSA_rsa_csp_AdvImportSignEncCertIS();

   // testIWSA_rsa_csp_setProviderIS();
   //testenumContainers();
  // testIWSA_rsa_csp_createContainerIS();
   
  // testIWSAGetAllCertsListInfoIS();
  // testIWSA_rsa_csp_deleteContainerCspIS();
  // testIWSAGetAllCertsListInfoIS();

   //testIWSA_rsa_csp_AdvgenContainerP10();
   //testIWSA_rsa_csp_genContainerP10IS(); 
   
   // testIWSA_rsa_csp_genP10IS();
  // testIWSA_rsa_csp_genEncKeyPairIS();
   //testIWSA_rsa_csp_delEncKeyPairIS();

   //testEnumCertificateStores();
   //testIWSA_rsa_csp_importX509CertToStoreIS();
   //testIWSA_rsa_csp_delX509CertInStoreIS();
   

   //testIWSA_rsa_csp_importSignX509CertIS();

   //testImportCerts();
  // testP10();
   //testKeyPair();
   //testContainer();
  
  // run();

   //testDecryptEnvelopIS();
   testDetachedSignIS();
   testDetachedSignIS();
   return 0;

}

void testCertInfo() {
    /* PHASE 1
    // testGmp();
    //testIWSAGetAllCertsListInfoISMY0();
    //maintest2();
    testIWSAGetAllCertsListInfoISNULL();
    testIWSAGetAllCertsListInfoISMY0();
    testIWSAGetAllCertsListInfoISMY2();
    testIWSAGetAllCertsListInfoISCA0();
    testIWSAGetAllCertsListInfoISCA2();
    testIWSAGetAllCertsListInfoISROOT0();
    testIWSAGetAllCertsListInfoISROOT2();
    // testIWSAGetAllCertsListInfoISADDRESSBOOK0();
    // testIWSAGetAllCertsListInfoISADDRESSBOOK2();
    //run();
    */
    //testIWSAGetAllCertsListInfoISADDRESSBOOK0();
    //testIWSAGetAllCertsListInfoISADDRESSBOOK2();
   // testIWSAGetAllCertsListInfoISTRUST0();
    //testSignAndEncryptIS
    //testIWSAGetAllCertsListInfoISMY0();
    //testIWSAGetAllCertsListInfoISADDRESSBOOK0();

    //testEncryptIS();
    //testSignIS();
    //testSignAndEncryptIS();
    //run();

    /*Phase 2
    //testIWSAGetAllCertsListInfoISADDRESSBOOK0();

    testBase64EncodeIS();
    testBase64DecodeIS();
    testSignIS();
    testSignByIndexIS();
    testDetachedSignIS();
    testDetachedSignDefaultDNIS();
    testDetachedVerifyIS();
    testAttachedSignIS();
    testAttachedSignDefaultDNIS();
    testAttachedVerifyIS();
    testRawSignIS();
    testRawSignFilterCNGIS();
    testRawSignDefaultDNCNGIS();
    testRawVerifyIS();
    testRawVerifyDefaultDNIS();
    testEncryptedEnvelopIS();
    testEncryptedEnvelopDESIVIS();
    testEncryptedEnvelopDefaultDNIS();
    testDecryptEnvelopIS();
    testEncryptedSignEnvelopIS();
    testEncryptedSignEnvelopDefaultDNIS();
    testDecryptSignEnvelopIS();
    */

    /*Phase 3
    testAddFormIS();
    testAddFormsIS();
    testAddFileIS();
    testAddFilesIS();
    testClearFormListIS();
    testClearFileListIS();
    testGetAlreadyFormListIS();
    testGetAlreadyFileListIS();
    */
}