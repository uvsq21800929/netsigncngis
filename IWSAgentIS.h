#pragma once
#include "gmpxx.h" // doit �tre import� au premier very peculiar
#include "ErrorHandler.h"
#include "CertificateIS.h"
#include "Constants.h"
#include "Outil.h"
#include <windows.h>
#include <wincrypt.h>
#include <ncrypt.h>
#include <certenroll.h>
#include <cryptuiapi.h>
#include <tchar.h>
#include <sstream>
#include <timezoneapi.h>
#include <vector>
#include <atlbase.h>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <vector>
#include <algorithm> 
#include <iterator>

using namespace std;
class IWSAgentIS
{
private:
	// params
	vector<string> formVector;
	vector<string> fileVector;
	string version;
	BOOL isStore;
	IX509CertificateRequestPkcs7* pPkcs7SignCert;
	IX509CertificateRequestPkcs7* pPkcs7EncCert;
	string certType;
	string tmpContainerName;
	NCRYPT_KEY_HANDLE hEncPriKey;
	BCRYPT_KEY_HANDLE hSymmetryKey;
	BCRYPT_KEY_HANDLE hEncKeyPair; // telecharger 
	NCRYPT_PROV_HANDLE hNCryptProvider;

public:
	vector<CertificateIS> cachedCerts;// anything found
	string cachedCertsType;
	string strMode;
	string defaultSignCertStore;
	string defaultEncryptCertStore;
	string defaultDecryptCertStore;
	vector<string> storeNames;
	vector<DWORD> systemStoreLocation;
	string pfxFile;
	wstring csp;

	//constructor
	IWSAgentIS();

	// getters & setters
	void setIsStore(const BOOL isStore);
	BOOL& getIsStore();
	vector<string>& getFormVector();// reference non automatique
	vector<string>& getFileVector();
	void setTmpContainerName(const string& tmpContainerName);
	string& getTmpContainerName();
	void setPkcs7SignCert(IX509CertificateRequestPkcs7*& pPkcs7SignCert);
	void setPkcs7EncCert(IX509CertificateRequestPkcs7*& pPkcs7EncCert);
	void setCertType(const string& certType);
	void setEncPrivateKey(const NCRYPT_KEY_HANDLE& hEncPriKey);
	void setSymmetryKey(const BCRYPT_KEY_HANDLE& hSymmetryKey);
	void setNCryptProvider(const NCRYPT_PROV_HANDLE& nCryptProvider);
	void setEncKeyPair(const BCRYPT_KEY_HANDLE& hEncKeyPair);
	BCRYPT_KEY_HANDLE& getEncKeyPair();

	// sub functions
	void showSubjectFormats();
	void enumContainers();
	HCERTSTORE& openAllStore();
	HCERTSTORE& openMyAndAddressbookStore();
	void enumCertificateStores();
	string readCrttoBase64(const string& file);
	string readPriKeytoBase64(const string& file);
	BYTE* SignAndEncryptIS(const BYTE* pbToBeSignedAndEncrypted, DWORD cbToBeSignedAndEncrypted, DWORD* pcbSignedAndEncryptedBlob, LPCSTR signerSubject, LPCSTR signerCertStore, LPCSTR recipientSubject, LPCSTR recipientCertStore);
	BYTE* EncryptIS(const BYTE* pbToBeEncrypted, DWORD cbToBeEncrypted, DWORD* pcbEncryptedBlob, LPCSTR recipientSubject, LPCSTR certStore);
	BOOL SignIS(const BYTE*& pbToBeSigned, const DWORD& cbToBeSigned, BYTE*& pbSignedBlob, DWORD& cbSignedBlob, const string& signerSubject, const BOOL isKeySpec, const BOOL isDetached, const string& digestArithmetic, const string& signerCertStore);
	BOOL SignIS(const BYTE*& pbToBeSigned, const DWORD& cbToBeSigned, BYTE*& pbSignedBlob, DWORD& cbSignedBlob, const DWORD& signerCertIndex, const BOOL isKeySpec, const BOOL isDetached, const string& digestArithmetic, const string& signerCertStore);

	// api functions
	string& IWSAGetVersionIS();
	void IWSABase64EncodeIS(DWORD* errorCode, string& textData, const BYTE* plainText, const DWORD& plainTextSize);
	void IWSABase64DecodeIS(DWORD* errorCode, BYTE*& textData, DWORD& cbBinary, const string& plainText);
	void IWSADetachedSignIS(DWORD*& errorCode, string& signedData, const string& portGrade, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& certIndex, const BOOL isKeySpec, const string& digestArithmetic, const string& signerCertStore);
	void IWSADetachedVerifyIS(DWORD*& errorCode, const string& portGrade, const string& signedData, const BYTE*& plainText, const DWORD& plainTextSize);
	void IWSAEncryptedEnvelopIS(DWORD*& errorCode, string& envelopedMsg, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& recipientCertIndex, const string& symmetryArithmetic);
	void IWSADecryptEnvelopIS(DWORD*& errorCode, string& plainText, string& certDN, const string& envelopedMsg);
	void IWSAEncryptedSignEnvelopIS(DWORD*& errorCode, string& envelopedMsg, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& signerCertIndex, const DWORD& recipientCertIndex, const string& digestArithmetic, const string& symmetryArithmetic);
	void IWSADecryptSignEnvelopIS(DWORD*& errorCode, string& plainText, string& signerDefaultDN, string& recipientDefaultDN, const string& envelopedMsg);
	string IWSAGetAllCertsListInfoIS(const string& certStoreSM2, const string& certStoreRSA, const DWORD& keySpec);
	string IWSAGetCertPublicKeyInfoForIndexIS(const DWORD& certIndex);
	string IWSA_rsa_csp_listProviderIS();
	void IWSA_rsa_csp_AdvgenContainerP10IS(DWORD*& errorCode, string& p10Value, string& encKeyPair, const string& keySize, const string& dn, const BOOL bDoubleCert);
	void IWSA_rsa_csp_AdvImportSignEncX509CertIS(DWORD*& errorCode, const string& cspName, const string& signCert, const string& encCert, const string& encPriKey, const string& ukek);
	void IWSA_rsa_csp_AdvImportSignEncP7CertIS(DWORD*& errorCode, const string& cspName, const string& signCert, const string& encCert, const string& encPriKey, const string& ukek);
	DWORD IWSA_rsa_csp_getCountOfCertIS();
	string IWSA_rsa_csp_getCertInfoIS(const DWORD& certIndex);
	DWORD IWSA_rsa_csp_delContainerIS(const string& csp, const string& container);
	DWORD IWSA_rsa_csp_deleteContainerIS(const string&csp, const string& container);
	void IWSA_rsa_csp_genContainerIS(DWORD*& errorCode, string& container);
	DWORD IWSA_rsa_csp_createContainerIS(const string& csp, const string& container);
	string IWSA_rsa_csp_genP10IS(DWORD*& errorCode, string& p10, const string& container, BOOL bSign, const string& keySize, const string& dn, const string& digestOID, const string& pubKeyAlgOID, const string& signAlgOID, BOOL bExport, BOOL bProtect);
	string IWSA_rsa_csp_genContainerP10IS(DWORD*& errorCode, string& container, string& p10, BOOL bSign, const string& keySize, const string& dn, const string& digestOID, const string& pubKeyAlgOID, const string& signAlgOID, BOOL bExport, BOOL bProtect);
	void IWSA_rsa_csp_genEncKeyPairIS(DWORD*& errorCode, string& data);
	DWORD IWSA_rsa_csp_delEncKeyPairIS();
	DWORD IWSA_rsa_csp_importX509CertToStoreIS(const string& x509Cert, BOOL bRoot);
	DWORD IWSA_rsa_csp_delX509CertInStoreIS(const string& issuerDN, const string& certSN);
	DWORD IWSA_rsa_csp_importSignX509CertIS(const string& container, const string& x509Cert);
	DWORD IWSA_rsa_csp_importEncX509CertIS(const string& container, const string& x509Cert, const string& encPriKey, const string& ukek, BOOL bExport, BOOL bProtect);
	DWORD IWSA_rsa_csp_importSignP7CertIS(const string& container, const string& p7Cert);
	DWORD IWSA_rsa_csp_importEncP7CertIS(const string& container, const string& p7Cert, const string& encPriKey, const string& ukek, BOOL bExport, BOOL bProtect);
	void IWSA_rsa_csp_exportSignX509CertIS(DWORD*& errorCode, string& data, const string& container);
	void IWSA_rsa_csp_exportEncX509CertIS(DWORD*& errorCode, string& data, const string& container);
	void IWSA_rsa_csp_exportPfxCertIS(DWORD*& errorCode, string& data, const string& issuerDN, const string& certSN, const string& password, BOOL bSaveAs);
	void IWSA_rsa_csp_exportContainerPfxCertIS(DWORD*& errorCode, string& data, const string& container, BOOL bSignCert, const string& password, BOOL bSaveAs);
	string IWSA_rsa_csp_GenUKEK();

};