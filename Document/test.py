#!/usr/bin/python
# -*- coding: UTF-8 -*-
import socket            
 
s = socket.socket()       
host = 'localhost'  
port = 6000               

print '[*] Test connection'
s.connect((host, port))
print '[*] Connection success.'
print '[*] Testing IWSAGetAllCertsListInfo?CertStoreSM2=&CertStoreRSA=MY&Keyspec=0.'
s.send('IWSAGetAllCertsListInfo?CertStoreSM2=&CertStoreRSA=MY&Keyspec=0')
print s.recv(4096)
print '[v] Testing IWSAGetAllCertsListInfo?CertStoreSM2=&CertStoreRSA=MY&Keyspec=0 passed.'
print ''
print '[*] Testing IWSAGetAllCertsListInfo?CertStoreSM2=&CertStoreRSA=MY&Keyspec=1.'
s.send('IWSAGetAllCertsListInfo?CertStoreSM2=&CertStoreRSA=MY&Keyspec=1')
s.send('IWSAGetAllCertsListInfo?CertStoreSM2=&CertStoreRSA=MY&Keyspec=1')
print s.recv(4096)
print '[v] Testing IWSAGetAllCertsListInfo?CertStoreSM2=&CertStoreRSA=MY&Keyspec=1 passed.'
s.send('FIN');
s.close()
