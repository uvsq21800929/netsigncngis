﻿/*
Il faut toujours ajouter les certificats de signature dans le magasin MY et ceux d'encryption dans le magasin ADDRESSBOOK pour que les certificats soient mis en ordre lorsque l'on choisit les certificats utilisés par la fonction GetAllCertsListInfo.
Ce programme offre la possibité du chiffrement et de la signature par RSA.
Digest Algorithm supporté: MD5 SHA1 SHA256
Symmetric Algorithm supporté: DES 3DES RC2
Les choix du store certificats: utilisateur/ROOT CA MY ADDRESSBOOK
*/

#include "IWSAgentIS.h"
#include <ntstatus.h>

#pragma comment(lib, "crypt32.lib")
#pragma comment(lib, "ncrypt.lib")

//#define MY_TYPE   X509_ASN_ENCODING
#define MY_TYPE (X509_ASN_ENCODING | PKCS_7_ASN_ENCODING) // cert encoding and message encoding
#define MAX_NAME  128
#define NT_SUCCESS(Status)          (((NTSTATUS)(Status)) >= 0)
#define STATUS_UNSUCCESSFUL         ((NTSTATUS)0xC0000001L)

using namespace std;

IWSAgentIS::IWSAgentIS() {
	version = VERSION0;
	isStore = FALSE; // default non cache
	strMode = "\n";
	defaultSignCertStore = MY;
	defaultEncryptCertStore = ADDRESSBOOK;
	storeNames.push_back(ROOT);
	storeNames.push_back(CA);
	storeNames.push_back(MY);
	storeNames.push_back(ADDRESSBOOK);
	systemStoreLocation.push_back(CERT_SYSTEM_STORE_CURRENT_USER);
	pfxFile = DEFAULT_PFX_FILE;
	certType = CERT_TYPE_X509;
	csp = MS_PRIMITIVE_PROVIDER;
}

BOOL& IWSAgentIS::getIsStore() {
	return isStore;
}

void IWSAgentIS::setIsStore(const BOOL isStore) {
	this->isStore = isStore;
}

void IWSAgentIS::setTmpContainerName(const string& tmpContainerName) {
	this->tmpContainerName = tmpContainerName;
}
string& IWSAgentIS::getTmpContainerName() {
	return this->tmpContainerName;
}

void IWSAgentIS::setPkcs7SignCert(IX509CertificateRequestPkcs7*& pPkcs7SignCert) {
	this->pPkcs7SignCert = pPkcs7SignCert;
}

void IWSAgentIS::setPkcs7EncCert(IX509CertificateRequestPkcs7*& pPkcs7EncCert) {
	this->pPkcs7EncCert = pPkcs7EncCert;
}

void IWSAgentIS::setCertType(const string& certType) {
	this->certType = certType;
}

void IWSAgentIS::setEncPrivateKey(const NCRYPT_KEY_HANDLE& hEncPriKey) {
	this->hEncPriKey = hEncPriKey;
}

void IWSAgentIS::setSymmetryKey(const BCRYPT_KEY_HANDLE& hSymmetryKey) {
	this->hSymmetryKey = hSymmetryKey;
}

void IWSAgentIS::setNCryptProvider(const NCRYPT_PROV_HANDLE& ncryptProvider) {
	this->hNCryptProvider = ncryptProvider;
}

void IWSAgentIS::setEncKeyPair(const BCRYPT_KEY_HANDLE& hEncKeyPair) {
	this->hEncKeyPair = hEncKeyPair;
}

BCRYPT_KEY_HANDLE& IWSAgentIS::getEncKeyPair() {
	return this->hEncKeyPair;
}

void IWSAgentIS::showSubjectFormats() {
	HCERTSTORE hCertStore;
	//---------------------------------------------------------------
	// pSignerCertContext will be the certificate of 
	// the message signer.

	PCCERT_CONTEXT pCertContext = NULL;
	TCHAR pszNameString[256];
	DWORD type;

	// Open a system certificate store.
	if (hCertStore = CertOpenSystemStore(NULL, DEFAULTSTORE)) { fprintf(stderr, "The %s store has been opened. \n", DEFAULTSTORE); }
	else { MyCertHandleError("The store was not opened."); }

	//---------------------------------------------------------------
	// Get the certificate for the signer.
	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
		if (CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, MAX_NAME) > 1)
		{
			_tprintf(TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")TEXT("%s \n"), pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		if (CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, 0, NULL, pszNameString, MAX_NAME) > 1)
		{
			_tprintf(TEXT("The RDM_TYPE message signer's name is %s \n"), pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		type = CERT_SIMPLE_NAME_STR;
		if (CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, 0, &type, pszNameString, MAX_NAME) > 1)
		{
			_tprintf(TEXT("The RDM_TYPE CERT_SIMPLE_NAME_STR message signer's name is %s \n"), pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		type = CERT_OID_NAME_STR;
		if (CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, 0, &type, pszNameString, MAX_NAME) > 1)
		{
			_tprintf(TEXT("The RDM_TYPE CERT_OID_NAME_STR message signer's name is %s \n"), pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		type = CERT_X500_NAME_STR;
		if (CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, 0, &type, pszNameString, MAX_NAME) > 1)
		{
			_tprintf(TEXT("The RDM_TYPE CERT_X500_NAME_STR message signer's name is %s \n"), pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
		cout << endl;
	}

}

BYTE* IWSAgentIS::SignAndEncryptIS(const BYTE* pbToBeSignedAndEncrypted, DWORD cbToBeSignedAndEncrypted, DWORD* pcbSignedAndEncryptedBlob, LPCSTR signerSubject, LPCSTR signerCertStore, LPCSTR recipientSubject, LPCSTR recipientCertStore)
{

	//---------------------------------------------------------------
	// Declare and initialize local variables.

	HCERTSTORE hSignerCertStore, hRecipientCertStore;

	//---------------------------------------------------------------
	// pSignerCertContext will be the certificate of 
	// the message signer.

	PCCERT_CONTEXT pSignerCertContext = NULL;

	//---------------------------------------------------------------
	// pRecipientCertContext will be the certificate of the 
	// message receiver.

	PCCERT_CONTEXT pRecipientCertContext = NULL;

	TCHAR pszNameString[256];
	CRYPT_SIGN_MESSAGE_PARA SignPara;
	CRYPT_ENCRYPT_MESSAGE_PARA EncryptPara;
	DWORD cRecipientCert = 0;
	PCCERT_CONTEXT rgpRecipientCert[5];
	BYTE* pbSignedAndEncryptedBlob = NULL;
	CERT_NAME_BLOB Subject_Blob;
	BYTE* pbDataIn;
	DWORD dwKeySpec;
	HCRYPTPROV hCryptProv;

	CRYPT_ALGORITHM_IDENTIFIER EncryptAlgorithm;
	BYTE* pbEncryptedBlob;
	DWORD    cbEncryptedBlob;

	DWORD EncryptParamsSize = sizeof(EncryptPara);
	DWORD EncryptAlgSize = sizeof(EncryptAlgorithm);

	//-------------------------------------------------------------------
	// Get a handle to a cryptographic provider.
	if (CryptAcquireContext(
		&hCryptProv,        // Address for handle to be returned.
		NULL,               // Use the current user's logon name.
		NULL,               // Use the default provider.
		PROV_RSA_FULL,      // Need to both encrypt and sign.
		NULL))              // No flags needed.
	{
		printf("A CSP has been acquired. \n");
	}
	else
	{
		MyCertHandleError("Cryptographic context could not be acquired.");
	}

	// Open a system certificate store.
	if (hSignerCertStore = CertOpenSystemStore(NULL, signerCertStore)) { fprintf(stderr, "The %s store has been opened. \n", signerCertStore); }
	else { MyCertHandleError("The store was not opened."); }

	//---------------------------------------------------------------
	// Get the certificate for the signer.

	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, signerSubject, -1, NULL, 0);
	LPWSTR lpSigner = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, signerSubject, -1, lpSigner, wchars_num);

	if (!(pSignerCertContext = CertFindCertificateInStore(
		hSignerCertStore,
		MY_TYPE,
		0,
		CERT_FIND_SUBJECT_STR,
		lpSigner,
		NULL))) {
		MyCertHandleError("Cert not found.\n");
	}
	else {
		//---------------------------------------------------------------
	// Get and print the name of the message signer.
	// The following two calls to CertGetNameString with different
	// values for the second parameter get two different forms of 
	// the certificate subject's name.
		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
				TEXT("%s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		if (CertGetNameString(
			pSignerCertContext,
			CERT_NAME_RDN_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The RDM_TYPE message signer's name is %s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
		if (!(CryptAcquireCertificatePrivateKey(
			pSignerCertContext,
			0,
			NULL,
			&hCryptProv,
			&dwKeySpec,
			NULL))) {
			MyCertHandleError(TEXT("CryptAcquireCertificatePrivateKey.\n"));
		}
		else {
			cout << "Acquire Private KeyUsage Success: " << dwKeySpec << endl;
		}
	}

	// Open a system certificate store.
	if (hRecipientCertStore = CertOpenSystemStore(NULL, recipientCertStore)) { fprintf(stderr, "The %s store has been opened. \n", recipientCertStore); }
	else { MyCertHandleError("The store was not opened."); }

	wchars_num = MultiByteToWideChar(CP_UTF8, 0, recipientSubject, -1, NULL, 0);
	LPWSTR lpRecipient = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, recipientSubject, -1, lpRecipient, wchars_num);

	if (!(pRecipientCertContext = CertFindCertificateInStore(
		hRecipientCertStore,
		MY_TYPE,
		0,
		CERT_FIND_SUBJECT_STR,
		lpRecipient,
		NULL)))
	{
		MyCertHandleError(TEXT("Receiver certificate not found."));
	}
	else {
		if (CertGetNameString(
			pRecipientCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The SIMPLE_DISPLAY_TYPE message signer's name is ")
				TEXT("%s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }

		if (CertGetNameString(
			pRecipientCertContext,
			CERT_NAME_RDN_TYPE,
			0,
			NULL,
			pszNameString,
			MAX_NAME) > 1)
		{
			_tprintf(
				TEXT("The RDM_TYPE message signer's name is %s \n"),
				pszNameString);
		}
		else { MyCertHandleError(TEXT("Getting the name of the signer failed.\n")); }
		// Create a RecipientCertArray.
		rgpRecipientCert[0] = pRecipientCertContext;
		cRecipientCert += 1;
	}

	//---------------------------------------------------------------
	// Initialize variables and data structures
	// for the call to CryptSignAndEncryptMessage.

	SignPara.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
	SignPara.dwMsgEncodingType = MY_TYPE;
	SignPara.pSigningCert = pSignerCertContext;
	string tmpSign = szOID_RSA_SHA1RSA; // const -> non const
	SignPara.HashAlgorithm.pszObjId = (LPSTR)tmpSign.c_str();
	SignPara.HashAlgorithm.Parameters.cbData = 0;
	SignPara.pvHashAuxInfo = NULL;
	SignPara.cMsgCert = 1;
	SignPara.rgpMsgCert = &pSignerCertContext;
	SignPara.cMsgCrl = 0;
	SignPara.rgpMsgCrl = NULL;
	SignPara.cAuthAttr = 0;
	SignPara.rgAuthAttr = NULL;
	SignPara.cUnauthAttr = 0;
	SignPara.rgUnauthAttr = NULL;
	SignPara.dwFlags = 0;
	SignPara.dwInnerContentType = 0;

	memset(&EncryptPara, 0, EncryptParamsSize);
	EncryptPara.cbSize = EncryptParamsSize;
	EncryptPara.dwMsgEncodingType = MY_TYPE;
	EncryptPara.hCryptProv = hCryptProv;
	EncryptAlgSize = sizeof(EncryptAlgorithm);
	memset(&EncryptAlgorithm, 0, EncryptAlgSize);
	string tmpEnc = szOID_RSA_RC2CBC;
	EncryptAlgorithm.pszObjId = (LPSTR)tmpEnc.c_str();
	EncryptAlgorithm.Parameters.cbData = 0;
	EncryptPara.ContentEncryptionAlgorithm = EncryptAlgorithm;
	EncryptPara.pvEncryptionAuxInfo = NULL;

	if (CryptSignAndEncryptMessage(
		&SignPara,
		&EncryptPara,
		cRecipientCert,
		rgpRecipientCert,
		pbToBeSignedAndEncrypted,
		cbToBeSignedAndEncrypted,
		NULL, // the pbSignedAndEncryptedBlob
		pcbSignedAndEncryptedBlob))
	{
		_tprintf(TEXT("%d bytes for the buffer .\n"),
			*pcbSignedAndEncryptedBlob);
	}
	else { cout << unsigned long long(GetLastError()) << endl; MyCertHandleError(TEXT("Getting the buffer length failed.")); }

	//---------------------------------------------------------------
   // Allocate memory for the buffer.

	if (!(pbSignedAndEncryptedBlob =
		(unsigned char*)malloc(*pcbSignedAndEncryptedBlob)))
	{
		MyCertHandleError(TEXT("Memory allocation failed."));
	}

	//---------------------------------------------------------------
	// Call the function a second time to copy the signed and 
	// encrypted message into the buffer.

	if (CryptSignAndEncryptMessage(
		&SignPara,
		&EncryptPara,
		cRecipientCert,
		rgpRecipientCert,
		pbToBeSignedAndEncrypted,
		cbToBeSignedAndEncrypted,
		pbSignedAndEncryptedBlob,
		pcbSignedAndEncryptedBlob))
	{
		_tprintf(TEXT("The message is signed and encrypted.\n"));
	}
	else
	{
		MyCertHandleError(
			TEXT("The message failed to sign and encrypt."));
	}

	// Call CryptEncryptMessage.
	/*
	if (CryptEncryptMessage(
		&EncryptPara,
		cRecipientCert,
		rgpRecipientCert,
		pbToBeSignedAndEncrypted,
		cbToBeSignedAndEncrypted,
		NULL,
		pcbSignedAndEncryptedBlob))
	{
		printf("The encrypted message is %d bytes. \n", pcbSignedAndEncryptedBlob);
	}
	else
	{
		cout << GetLastError() << endl;
		MyCertHandleError("Getting EncryptedBlob size failed.");
	}



	// First, get the size of the signed BLOB.
	if (CryptSignMessage(
		&SignPara,
		FALSE,
		1,
		&pbToBeSignedAndEncrypted,
		&cbToBeSignedAndEncrypted,
		NULL,
		pcbSignedAndEncryptedBlob))
	{
		_tprintf(TEXT("%d bytes needed for the encoded BLOB.\n"),
			pcbSignedAndEncryptedBlob);
	}
	else
	{
		MyCertHandleError(TEXT("Getting signed BLOB size failed"));
	}*/

	delete[] lpSigner, lpRecipient;
	cout << "==================" << endl;
	return pbSignedAndEncryptedBlob;

}

BYTE* IWSAgentIS::EncryptIS(const BYTE* pbToBeEncrypted, DWORD cbToBeEncrypted, DWORD* pcbEncryptedBlob, LPCSTR recipientSubject, LPCSTR certStore) {
	HCRYPTPROV hCryptProv;                      // CSP handle
	HCERTSTORE hStoreHandle;
	PCCERT_CONTEXT pRecipientCert;
	PCCERT_CONTEXT RecipientCertArray[1];
	DWORD EncryptAlgSize;
	CRYPT_ALGORITHM_IDENTIFIER EncryptAlgorithm;
	CRYPT_ENCRYPT_MESSAGE_PARA EncryptParams;
	DWORD EncryptParamsSize;
	BYTE* pbEncryptedBlob;
	DWORD    cbEncryptedBlob;

	//-------------------------------------------------------------------
	//  Begin processing.

	printf("About to begin with the message %s.\n", pbToBeEncrypted);
	printf("The message length is %d bytes. \n", cbToBeEncrypted);

	//-------------------------------------------------------------------
	// Get a handle to a cryptographic provider.

	if (CryptAcquireContext(
		&hCryptProv,        // Address for handle to be returned.
		NULL,               // Use the current user's logon name.
		NULL,               // Use the default provider.
		PROV_RSA_FULL,      // Need to both encrypt and sign.
		NULL))              // No flags needed.
	{
		printf("A CSP has been acquired. \n");
	}
	else
	{
		MyCertHandleError("Cryptographic context could not be acquired.");
	}
	//-------------------------------------------------------------------
	// Open a system certificate store.

	if (hStoreHandle = CertOpenSystemStore(
		hCryptProv,
		certStore))
	{
		printf("The %s store is open. \n", certStore);
	}
	else
	{
		MyCertHandleError("Error getting store handle.");
	}

	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, recipientSubject, -1, NULL, 0);
	LPWSTR lp = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, recipientSubject, -1, lp, wchars_num);

	if (!(pRecipientCert = CertFindCertificateInStore(
		hStoreHandle,
		MY_TYPE,
		0,
		CERT_FIND_SUBJECT_STR,
		//L"FR, Ldf, Infosec, Dpt_Buz, Encrypt",only have pub key ne marche pas ?
	   // L"FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com",// has to use private key
		//L"FR, Ldf, Paris, Infosec, Dpt_Buz, User Client Encryption Infosec, enc@infosec.com",
		lp,
		NULL)))
	{
		MyCertHandleError(TEXT("Receiver certificate not found."));
	}

	//-------------------------------------------------------------------
   // Create a RecipientCertArray.

	RecipientCertArray[0] = pRecipientCert;

	//-------------------------------------------------------------------
	// Initialize the algorithm identifier structure.

	EncryptAlgSize = sizeof(EncryptAlgorithm);

	//-------------------------------------------------------------------
	// Initialize the structure to zero.

	memset(&EncryptAlgorithm, 0, EncryptAlgSize);

	//-------------------------------------------------------------------
	// Set the necessary member.

	string tmp = szOID_RSA_RC2CBC;
	EncryptAlgorithm.pszObjId = (LPSTR)tmp.c_str();

	//-------------------------------------------------------------------
	// Initialize the CRYPT_ENCRYPT_MESSAGE_PARA structure. 

	EncryptParamsSize = sizeof(EncryptParams);
	memset(&EncryptParams, 0, EncryptParamsSize);
	EncryptParams.cbSize = EncryptParamsSize;
	EncryptParams.dwMsgEncodingType = MY_TYPE;
	EncryptParams.hCryptProv = hCryptProv;
	EncryptParams.ContentEncryptionAlgorithm = EncryptAlgorithm;

	//-------------------------------------------------------------------
	// Call CryptEncryptMessage.

	if (CryptEncryptMessage(
		&EncryptParams,
		1,
		RecipientCertArray,
		pbToBeEncrypted,
		cbToBeEncrypted,
		NULL,
		&cbEncryptedBlob))
	{
		printf("The encrypted message is %d bytes. \n", cbEncryptedBlob);
	}
	else
	{
		cout << GetLastError() << endl;
		MyCertHandleError("Getting EncryptedBlob size failed.");
	}
	//-------------------------------------------------------------------
	// Allocate memory for the returned BLOB.

	if (pbEncryptedBlob = (BYTE*)malloc(cbEncryptedBlob))
	{
		printf("Memory has been allocated for the encrypted BLOB. \n");
	}
	else
	{
		MyCertHandleError("Memory allocation error while encrypting.");
	}
	//-------------------------------------------------------------------
	// Call CryptEncryptMessage again to encrypt the content.

	if (CryptEncryptMessage(
		&EncryptParams,
		1,
		RecipientCertArray,
		pbToBeEncrypted,
		cbToBeEncrypted,
		pbEncryptedBlob,
		&cbEncryptedBlob))
	{
		printf("Encryption succeeded. \n");
	}
	else
	{
		MyCertHandleError("Encryption failed.");
	}

	cout << "==================" << endl;

	return pbEncryptedBlob;

}

BOOL IWSAgentIS::SignIS(const BYTE*& pbToBeSigned, const DWORD& cbToBeSigned, BYTE*& pbSignedBlob, DWORD& cbSignedBlob, const DWORD& signerCertIndex, const BOOL isKeySpec, const BOOL isDetached, const string& digestArithmetic, const string& signerCertStore) {
	PCCERT_CONTEXT pSignerCertContext = NULL;
	PBYTE pbKeyUsage = NULL;
	TCHAR pszNameString[256];
	CRYPT_SIGN_MESSAGE_PARA SignPara;
	DWORD dwKeySpec = CERT_NCRYPT_KEY_SPEC, cbKeyUsage = 1, indexCert = 0;
	HCERTSTORE hSignerCertStore = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	string tmpSign = digestArithmetic;
	// Open a system certificate store.
	if (string::npos != signerCertStore.find(",")) {
		hSignerCertStore = this->openMyAndAddressbookStore();
	}
	else hSignerCertStore = CertOpenSystemStore(NULL, signerCertStore.c_str());
	if (NULL == hSignerCertStore) { return FALSE; }
	//---------------------------------------------------------------
	// Get the certificate for the signer.
	while (pSignerCertContext = CertEnumCertificatesInStore(hSignerCertStore, pSignerCertContext)) {
		if (indexCert++ == signerCertIndex) {
			if (!(CryptAcquireCertificatePrivateKey(pSignerCertContext, CRYPT_ACQUIRE_ONLY_NCRYPT_KEY_FLAG, NULL, &hKey, &dwKeySpec, NULL))) { return FALSE; }
			else {
				if (NULL == hKey) { return FALSE; }
				if (TRUE == isKeySpec) {
					pbKeyUsage = (PBYTE)malloc(cbKeyUsage);
					if (CertGetIntendedKeyUsage(MY_TYPE, pSignerCertContext->pCertInfo, pbKeyUsage, cbKeyUsage)) {
						unsigned char flag = pbKeyUsage[0];
						if (1 != int(flag) >> 7) continue;
					}
				}
				break;
			}
			break;
		}
	}
	//---------------------------------------------------------------
	// Initialize variables and data structures
	// for the call to CryptSignMessage.
	SignPara.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
	SignPara.dwMsgEncodingType = MY_TYPE;
	SignPara.pSigningCert = pSignerCertContext;
	SignPara.HashAlgorithm.pszObjId = (LPSTR)tmpSign.c_str();
	SignPara.HashAlgorithm.Parameters.cbData = 0;
	SignPara.pvHashAuxInfo = NULL;
	SignPara.cMsgCert = 1;
	SignPara.rgpMsgCert = &pSignerCertContext;
	SignPara.cMsgCrl = 0;
	SignPara.rgpMsgCrl = NULL;
	SignPara.cAuthAttr = 0;
	SignPara.rgAuthAttr = NULL;
	SignPara.cUnauthAttr = 0;
	SignPara.rgUnauthAttr = NULL;
	SignPara.dwFlags = 0;
	SignPara.dwInnerContentType = 0;
	if (!CryptSignMessage(&SignPara, isDetached, 1, &pbToBeSigned, const_cast<DWORD*>(&cbToBeSigned), NULL, &cbSignedBlob)) { return FALSE; }
	if (!(pbSignedBlob = (unsigned char*)malloc(cbSignedBlob))) { return FALSE; }
	if (!CryptSignMessage(&SignPara, isDetached, 1, &pbToBeSigned, const_cast<DWORD*>(&cbToBeSigned), pbSignedBlob, &cbSignedBlob)) { return FALSE; }
	return TRUE;
}

/*
IWSAGetVersion(SucceedFunction)
3.2.1.2参数
参数名称	类  型	参数描述
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
*/
string& IWSAgentIS::IWSAGetVersionIS() {
	return version;
}

/*
IWSABase64Encode(PlainText, SucceedFunction)
3.2.4.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.4.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
TextData	字符串	编码后的数据
3.2.4.4功能说明
对输入原文进行Base64编码。
errorCode为0 ，表示成功。非0情况用户可以对照GetLastError()得到错误信息。
*/

void IWSAgentIS::IWSABase64EncodeIS(DWORD* errorCode, string& textData, const BYTE* plainText, const DWORD& plainTextSize) {
	LPSTR pszString = NULL;
	DWORD pcchString = 0;
	if (CryptBinaryToString(plainText, plainTextSize, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, nullptr, &pcchString)) {
		pszString = (LPSTR)malloc(pcchString);
		if (CryptBinaryToString(plainText, plainTextSize, CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF, pszString, &pcchString)) {
			textData = string(pszString, pcchString);
			*errorCode = 0;
			// Clean Up
			free(pszString);
		}
	}
	else {
		*errorCode = GetLastError();
		textData = "";
	}
}

/*
IWSABase64Decode (PlainText,SucceedFunction)
3.2.5.2参数
参数名称	类  型	参数描述
PlainText	字符串	Base64编码后的原文
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.5.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
TextData	字符串	解码后的数据
对输入的数据进行Base64格式的解码。
errorCode为0 ，表示成功。非0情况用户可以对照GetLastError()得到错误信息。
*/
void IWSAgentIS::IWSABase64DecodeIS(DWORD* errorCode, BYTE*& textData, DWORD& cbBinary, const string& plainText) {
	if (CryptStringToBinary(plainText.c_str(), plainText.size(), CRYPT_STRING_BASE64, NULL, &cbBinary, NULL, NULL)) {
		textData = (PBYTE)malloc(cbBinary);
		if (CryptStringToBinary(plainText.c_str(), plainText.size(), CRYPT_STRING_BASE64, textData, &cbBinary, NULL, NULL)) {
			*errorCode = 0;
		}
	}
	else {
		*errorCode = GetLastError();
		textData = nullptr;
		cbBinary = 0;
	}
}

/*
3.2.7（方法）Detached方式签名
3.2.7.1名称
IWSADetachedSign(PortGrade, PlainText, CertIndex, DigestArithmetic, SucceedFunction)
3.2.7.2参数
参数名称	类  型	参数描述
PortGrade	整数	1为一代签名接口，2为 二代签名接口
PlainText	字符串	原文
CertIndex	整数	证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
DigestArithmetic	字符串	摘要算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.7.3回调函数参数
参数名称	类 型	返回值说明
errorCode	整数	错误码
signedData	字符串	BASE64编码后的签名
3.2.7.4功能说明
对输入的原文做detached方式签名，再做Base64编码，并返回编码结果。
errorCode为0 ，表示成功。非0情况用户可以对照GetLastError()得到错误信息。
摘要算法的值分别为：” SHA1”    ” MD5”    ” SHA256”
*/

void IWSAgentIS::IWSADetachedSignIS(DWORD*& errorCode, string& signedData, const string& portGrade, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& certIndex, const BOOL isKeySpec, const string& digestArithmetic, const string& signerCertStore) {
	DWORD cbSignedBlob = 0;
	PBYTE pbSignedBlob = nullptr;
	if (SignIS(plainText, plainTextSize, pbSignedBlob, cbSignedBlob, certIndex, isKeySpec, TRUE, digestArithmetic, signerCertStore)) {
		IWSABase64EncodeIS(errorCode, signedData, pbSignedBlob, cbSignedBlob);
	}
}

/*
3.2.10（方法）Detached方式验签名
3.2.10.1名称
IWSADetachedVerify(PortGrade, signedMsg, PlainText, SucceedFunction)
3.2.10.2参数
参数名称	类  型	参数描述
PortGrade	整数	1为一代验签接口，2为 二代验签接口
signedMsg	字符串	Base64编码的签名
PlainText	字符串	原文
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.10.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	错误码
3.2.10.4功能说明
对输入的签名先作Base64解码，然后解开哈希值与原文产生的哈希值比对检查，如果一致，表明成功，然后开始校验证书的签名、有效期、证书链的有效性以及CRL。如果上述操作都成功，则errorCode为0，否则设置errorCode为对应的错误信息。
用户可以通过查看errorCode错误码表，对照错误码表得到详细错误信息。
*/

void IWSAgentIS::IWSADetachedVerifyIS(DWORD*& errorCode, const string& portGrade, const string& signedData, const BYTE*& plainText, const DWORD& plainTextSize) {
	PBYTE textData = NULL, pbDecoded = NULL;
	PCCERT_CONTEXT pSignerCertContext = NULL;
	PCERT_INFO pSignerCertInfo = NULL;
	DWORD* plainTextSizeNonConst = NULL, cbSignerCertInfo = 0, cbBinary = 0, indexCert = 0;
	HCERTSTORE hSignerCertStore = NULL;
	CRYPT_VERIFY_MESSAGE_PARA verifyPara;
	HCRYPTMSG hMsg = NULL;

	// Initialize the VerifyParams data structure.
	ZeroMemory(&verifyPara, sizeof(verifyPara));
	verifyPara.cbSize = sizeof(CRYPT_VERIFY_MESSAGE_PARA);
	verifyPara.dwMsgAndCertEncodingType = MY_TYPE;
	verifyPara.hCryptProv = NULL;
	verifyPara.pfnGetSignerCertificate = NULL;
	verifyPara.pvGetArg = NULL;

	IWSABase64DecodeIS(errorCode, pbDecoded, cbBinary, signedData);
	if (*errorCode) return;

	if (!(hMsg = CryptMsgOpenToDecode(MY_TYPE, 0, 0, NULL, NULL, NULL))) { *errorCode = GetLastError(); return; }

	if (!CryptMsgUpdate(hMsg, pbDecoded, cbBinary, TRUE)) { *errorCode = GetLastError(); return; }

	if (!CryptMsgGetParam(hMsg, CMSG_SIGNER_CERT_INFO_PARAM, 0, NULL, &cbSignerCertInfo)) { *errorCode = GetLastError(); return; }

	if (!(pSignerCertInfo = (PCERT_INFO)malloc(cbSignerCertInfo))) { *errorCode = GetLastError(); return; }

	if (!(CryptMsgGetParam(hMsg, CMSG_SIGNER_CERT_INFO_PARAM, 0, pSignerCertInfo, &cbSignerCertInfo))) { *errorCode = GetLastError(); return; }

	if (!(hSignerCertStore = CertOpenStore(CERT_STORE_PROV_MSG, MY_TYPE, NULL, 0, hMsg))) { *errorCode = GetLastError(); return; }

	// Find the signer's certificate in the store.

	if (!(pSignerCertContext = CertGetSubjectCertificateFromStore(hSignerCertStore, MY_TYPE, pSignerCertInfo))) { *errorCode = GetLastError(); return; }

	if (!(plainTextSizeNonConst = const_cast<DWORD*> (&plainTextSize))) { *errorCode = GetLastError(); return; }

	//dwSignerIndex not considered
	if (!CryptVerifyDetachedMessageSignature(&verifyPara, 0, pbDecoded, cbBinary, 1, &plainText, plainTextSizeNonConst, &pSignerCertContext)) { *errorCode = GetLastError(); return; }
	*errorCode = 0;

	free(pSignerCertInfo);
	if (pSignerCertContext) CertFreeCertificateContext(pSignerCertContext);
	if (hMsg) CryptMsgClose(hMsg);
	if (hSignerCertStore) CertCloseStore(hSignerCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
}


/*
3.2.19（方法）制作数字信封
3.2.19.1名称
IWSAEncryptedEnvelop(PlainText, CertIndex, SymmetryArithmetic, SucceedFunction)
3.2.19.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
CertIndex	整数	证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
SymmetryArithmetic	字符串	对称算法
SucceedFunction	回调函数名字	函数成功后的回调函数(同步方式忽略此参数)

3.2.19.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
envelopedMsg	字符串	返回数字信封的Base64编码结果

3.2.19.4功能说明
对输入的原文做数字信封，再做Base64编码，返回编码结果。
暂不支持国密证书。
errorCode为0 ，表示成功。非0情况用户可以对照GetLastError()得到错误信息。
对称算法的值分别为： “3DES” “RC2” “DES”
RC4 Not supported
*/
void IWSAgentIS::IWSAEncryptedEnvelopIS(DWORD*& errorCode, string& envelopedMsg, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& recipientCertIndex, const string& symmetryArithmetic) {
	PCCERT_CONTEXT pRecipientCertContext = NULL;          // receiver's certificate
	PCERT_INFO RecipCertArray[1];
	PBYTE pbEncodedBlob = NULL;
	LPCSTR recipientCertStore = this->defaultEncryptCertStore.c_str();
	DWORD cbEncodedBlob = 0, HashAlgSize = 0, indexCert = 0;
	CRYPT_ALGORITHM_IDENTIFIER ContentEncryptAlgorithm;
	CMSG_ENVELOPED_ENCODE_INFO EnvelopedEncodeInfo;
	HCRYPTMSG hMsg = NULL;
	CRYPT_ALGORITHM_IDENTIFIER HashAlgorithm;
	TCHAR pszNameString[256];
	HCERTSTORE hRecipientCertStore = NULL;

	if (!(hRecipientCertStore = this->openMyAndAddressbookStore())) { *errorCode = GetLastError(); return; }

	// Get a pointer to the recipient certificate.
	while (pRecipientCertContext = CertEnumCertificatesInStore(hRecipientCertStore, pRecipientCertContext)) {
		if (indexCert++ == recipientCertIndex) {
			break;
		}
	}
	if (!pRecipientCertContext) { *errorCode = GetLastError(); return; }

	// Initialize the first element of the array of CERT_INFOs. For NetSignCNGIS, there is only a single recipient.
	RecipCertArray[0] = pRecipientCertContext->pCertInfo;

	// Initialize the symmetric-encryption algorithm identifier structure.

	ZeroMemory(&ContentEncryptAlgorithm, sizeof(ContentEncryptAlgorithm));               // initialize to zero
	 // Initialize the necessary members. This particular OID does not need any parameters. Some OIDs, however, will require that the other members be initialized.
	ContentEncryptAlgorithm.pszObjId = const_cast<LPSTR>(symmetryArithmetic.c_str());
	ContentEncryptAlgorithm.Parameters.cbData = 0; // default even for DES

	// Initialize the CMSG_ENVELOPED_ENCODE_INFO structure.
	ZeroMemory(&EnvelopedEncodeInfo, sizeof(CMSG_ENVELOPED_ENCODE_INFO));
	EnvelopedEncodeInfo.cbSize = sizeof(CMSG_ENVELOPED_ENCODE_INFO);
	EnvelopedEncodeInfo.hCryptProv = 0;
	EnvelopedEncodeInfo.ContentEncryptionAlgorithm = ContentEncryptAlgorithm;
	EnvelopedEncodeInfo.pvEncryptionAuxInfo = NULL;
	EnvelopedEncodeInfo.cRecipients = 1;
	EnvelopedEncodeInfo.rgpRecipients = RecipCertArray;

	// Get the size of the encoded message BLOB.
	if (!(cbEncodedBlob = CryptMsgCalculateEncodedLength(MY_TYPE, 0, CMSG_ENVELOPED, &EnvelopedEncodeInfo, NULL, plainTextSize))) { *errorCode = GetLastError(); return; }          // size of content

	// Allocate memory for the encoded BLOB.
	if (!(pbEncodedBlob = (PBYTE)malloc(cbEncodedBlob))) { *errorCode = GetLastError(); return; }

	// Open a message to encode.
	if (!(hMsg = CryptMsgOpenToEncode(MY_TYPE, 0, CMSG_ENVELOPED, &EnvelopedEncodeInfo, NULL, NULL))) { *errorCode = GetLastError(); return; }

	// Update the message with the data.
	if (!(CryptMsgUpdate(hMsg, plainText, plainTextSize, TRUE))) { *errorCode = GetLastError(); return; }

	// Get the resulting message.
	if (CryptMsgGetParam(hMsg, CMSG_CONTENT_PARAM, 0, pbEncodedBlob, &cbEncodedBlob))
	{
		*errorCode = 0;
		IWSABase64EncodeIS(errorCode, envelopedMsg, pbEncodedBlob, cbEncodedBlob);
	}
	else { *errorCode = GetLastError(); return; }

	// Clean up.
	free(pbEncodedBlob);
	if (pRecipientCertContext) CertFreeCertificateContext(pRecipientCertContext);
	if (hRecipientCertStore) CertCloseStore(hRecipientCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hMsg) CryptMsgClose(hMsg);
}

/*
3.2.21方法）解数字信封
3.2.21.1名称
IWSADecryptEnvelop(envelopedMsg,SucceedFunction)
3.2.21.2参数
参数名称	类  型	参数描述
envelopedMsg	字符串	Base64编码的数字信封
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.21.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
PlainText	字符串	原文
certDN	字符串	加密证书DN
3.2.21.4功能说明
对输入的数字信封先作Base64解码，然后解密该数字信封。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。

deprecated
*/
void IWSAgentIS::IWSADecryptEnvelopIS(DWORD*& errorCode, string& plainText, string& certDN, const string& envelopedMsg) {
	PBYTE pbDecodedBlob = NULL, pbBase64Decoded = NULL, pbDecryptedBlob = NULL, pbBuffer = NULL, pbKeyBlob = NULL, pbSignature = NULL;
	PCERT_INFO pRecipientCertInfo = NULL;
	PCCERT_CONTEXT pRecipientCertContext = NULL;
	LPCSTR recipientCertStore = this->defaultEncryptCertStore.c_str();
	TCHAR pszNameString[256];
	NCRYPT_PROV_HANDLE hProvider = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	HCERTSTORE hRecipientCertStore = NULL;           // certificate store handle
	DWORD cbRecipientCertInfo = 0, cbDecodedBlob = 0, cbBase64Decoded = 0, * msgType = NULL, msgTypeSize = sizeof(DWORD), cbDecryptedBlob = 0, keyUsage = NCRYPT_ALLOW_DECRYPT_FLAG, dwBufferLen = 0, cbKeyBlob = 0, cbSignature = 0;
	HCRYPTMSG hMsg = NULL;
	CMSG_CTRL_DECRYPT_PARA decryptPara;

	ifstream privateKey(PRIVATE_KEY_RSA_3);
	string privateKeyPEM((std::istreambuf_iterator<char>(privateKey)), std::istreambuf_iterator<char>());

	IWSABase64DecodeIS(errorCode, pbBase64Decoded, cbBase64Decoded, envelopedMsg);
	if (*errorCode) { return; }
	// Open a PKCS7 message to decode
	if (!(hMsg = CryptMsgOpenToDecode(MY_TYPE, 0, 0, NULL, NULL, NULL))) { *errorCode = GetLastError(); return; }
	// Update the PKCS7 message with the data.
	if (!CryptMsgUpdate(hMsg, pbBase64Decoded, cbBase64Decoded, TRUE)) { *errorCode = GetLastError(); return; }
	// Get the resulting message (envelope decoded) 
	if (!CryptMsgGetParam(hMsg, CMSG_TYPE_PARAM, 0, NULL, &msgTypeSize)) { *errorCode = GetLastError(); return; }
	if (!(msgType = (DWORD*)malloc(msgTypeSize))) { *errorCode = GetLastError(); return; }
	if (CryptMsgGetParam(hMsg, CMSG_TYPE_PARAM, 0, msgType, &msgTypeSize)) {
		if (CMSG_ENVELOPED == *msgType) {
			if (CryptMsgGetParam(hMsg, CMSG_INNER_CONTENT_TYPE_PARAM, 0, NULL, &cbDecodedBlob)) { pbDecodedBlob = (PBYTE)malloc(cbDecodedBlob);}
			else { *errorCode = GetLastError(); return; }
			if (CryptMsgGetParam(hMsg, CMSG_INNER_CONTENT_TYPE_PARAM, 0, pbDecodedBlob, &cbDecodedBlob)) {
				if (!CryptStringToBinary(privateKeyPEM.c_str(), privateKeyPEM.size(), CRYPT_STRING_BASE64HEADER, NULL, &dwBufferLen, NULL, NULL)) { *errorCode = GetLastError(); return; }
				if (!(pbBuffer = (PBYTE)malloc(dwBufferLen))) { *errorCode = GetLastError(); return; }
				if (!CryptStringToBinary(privateKeyPEM.c_str(), privateKeyPEM.size(), CRYPT_STRING_BASE64HEADER, pbBuffer, &dwBufferLen, NULL, NULL)) { *errorCode = GetLastError(); return; }
				
				if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbBuffer, dwBufferLen, 0, NULL, NULL, &cbKeyBlob)) { *errorCode = GetLastError(); return; }
				if (!(pbKeyBlob = (PBYTE)malloc(cbKeyBlob))) { *errorCode = GetLastError(); return; }
				if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbBuffer, dwBufferLen, 0, NULL, pbKeyBlob, &cbKeyBlob)) { *errorCode = GetLastError(); return; }
				// BCRYPT_RSAKEY_BLOB *blob = (BCRYPT_RSAKEY_BLOB*)pbKeyBlob;
				// Generate private key handle
				if (ERROR_SUCCESS == NCryptOpenStorageProvider(&hProvider, MS_KEY_STORAGE_PROVIDER, 0)) {
					if (ERROR_SUCCESS != NCryptImportKey(hProvider, NULL, BCRYPT_RSAPRIVATE_BLOB, NULL, &hKey, pbKeyBlob, cbKeyBlob, NCRYPT_DO_NOT_FINALIZE_FLAG | NCRYPT_OVERWRITE_KEY_FLAG)) { *errorCode = GetLastError(); return; }
					if (ERROR_SUCCESS != NCryptSetProperty(hKey, NCRYPT_KEY_USAGE_PROPERTY, (PBYTE)&keyUsage, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) { *errorCode = GetLastError(); return; }
					if (ERROR_SUCCESS != NCryptFinalizeKey(hKey, 0)) { *errorCode = GetLastError(); return; }
				}

				ZeroMemory(&decryptPara, sizeof(decryptPara));
				decryptPara.hNCryptKey = hKey;
				decryptPara.dwRecipientIndex = 0;
				decryptPara.dwKeySpec = 0;
				decryptPara.cbSize = sizeof(decryptPara);

				if (CryptMsgControl(hMsg, CMSG_CRYPT_RELEASE_CONTEXT_FLAG, CMSG_CTRL_DECRYPT, &decryptPara)) {
					if (CryptMsgGetParam(hMsg, CMSG_CONTENT_PARAM, 0, NULL, &cbDecryptedBlob)) {
						pbDecryptedBlob = (PBYTE)malloc(cbDecryptedBlob);
					}
					else { *errorCode = GetLastError(); return; }
					if (CryptMsgGetParam(hMsg, CMSG_CONTENT_PARAM, 0, pbDecryptedBlob, &cbDecryptedBlob)) {
						plainText = string((char*)pbDecryptedBlob, cbDecryptedBlob);
					}
					else { *errorCode = GetLastError(); return; }
					if (!CryptMsgGetParam(hMsg, CMSG_RECIPIENT_INFO_PARAM, 0, NULL, &cbRecipientCertInfo)) { *errorCode = GetLastError(); return; }
					// Allocate memory.
					if (!(pRecipientCertInfo = (PCERT_INFO)malloc(cbRecipientCertInfo))) { *errorCode = GetLastError(); return; }
					// Get the message certificate information (CERT_INFO structure).
					if (!(CryptMsgGetParam(hMsg, CMSG_RECIPIENT_INFO_PARAM, 0, pRecipientCertInfo, &cbRecipientCertInfo))) { *errorCode = GetLastError(); return; }
					if (!(hRecipientCertStore = CertOpenSystemStore(NULL, recipientCertStore))) { *errorCode = GetLastError(); return; }
					if (pRecipientCertContext = CertGetSubjectCertificateFromStore(hRecipientCertStore, MY_TYPE, pRecipientCertInfo))   // pointer to retrieved CERT_CONTEXT
					{
						if (CertGetNameString(pRecipientCertContext, CERT_NAME_RDN_TYPE, 0, NULL, pszNameString, MAX_NAME) > 1)
						{
							certDN = pszNameString;
						}
						else { *errorCode = GetLastError(); return; }
					}
					else { *errorCode = GetLastError(); return; }
				}
				else { 
					*errorCode = GetLastError(); 
					cout << hex << *errorCode << endl;
					return; 
				}
			}
		}
	}
	else { *errorCode = GetLastError(); return; }

	// Clean up.
	free(pbDecodedBlob);
	free(pbBuffer);
	free(pbKeyBlob);
	free(pbDecryptedBlob);
	if (pRecipientCertContext) CertFreeCertificateContext(pRecipientCertContext);
	if (hRecipientCertStore) CertCloseStore(hRecipientCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hProvider) NCryptFreeObject(hProvider);
	if (hMsg) CryptMsgClose(hMsg);
	if (hKey) NCryptFreeObject(hKey);
}

/*
3.2.22（方法）封装带签名的数字信封
3.2.22.1名称
IWSAEncryptedSignEnvelop (PlainText,SignCertIndex,EncryptCertIndex,DigestArithmetic,SymmetryArithmetic,SucceedFunction)
3.2.22.2参数
参数名称	类  型	参数描述
PlainText	字符串	原文
SignCertIndex	整数	签名证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
EncryptCertIndex	整数	加密证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
DigestArithmetic	字符串	摘要算法
SymmetryArithmetic	字符串	对称算法
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.22.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
envelopedMsg	字符串	返回数字信封的Base64编码结果
3.2.22.4功能说明
对输入的原文信息先做Attach签名，然后在做数字信封。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。
*/
void IWSAgentIS::IWSAEncryptedSignEnvelopIS(DWORD*& errorCode, string& envelopedMsg, const BYTE*& plainText, const DWORD& plainTextSize, const DWORD& signerCertIndex, const DWORD& recipientCertIndex, const string& digestArithmetic, const string& symmetryArithmetic) {
	PBYTE pbSignedBlob = NULL;
	string signedData = "";
	DWORD cbSignedBlob = 0;
	string store = this->defaultSignCertStore + "," + this->defaultEncryptCertStore;
	if (SignIS(plainText, plainTextSize, pbSignedBlob, cbSignedBlob, signerCertIndex, FALSE, FALSE, digestArithmetic, store)) {
		const BYTE* tmp = pbSignedBlob;
		IWSAEncryptedEnvelopIS(errorCode, envelopedMsg, tmp, cbSignedBlob, recipientCertIndex, symmetryArithmetic);
	}
	else { *errorCode = GetLastError(); return; }
}

/*
3.2.24（方法）解带签名的数字信封
3.2.24.1名称
IWSADecryptSignEnvelop (envelopedMsg,SucceedFunction)
3.2.24.2参数
参数名称	类  型	参数描述
envelopedMsg	字符串	Base64编码的带签名的数字信封
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.24.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
PlainText	字符串	原文
SigncertDN	字符串	签名证书DN
EncryptcertDN	字符串	加密证书DN
3.2.24.4功能说明
对输入的带签名的数字信封先作Base64解码，然后解密该数字信封并验证签名是否正确。
errorCode为0 ，表示成功。非0情况用户可以对照错误码表得到错误信息。

deprecated
*/
void IWSAgentIS::IWSADecryptSignEnvelopIS(DWORD*& errorCode, string& plainText, string& signerDefaultDN, string& recipientDefaultDN, const string& envelopedMsg) {
	PCCERT_CONTEXT pSignerCertContext = NULL;
	PCERT_INFO pSignerCertInfo = NULL;
	PBYTE pbDecoded = NULL, pbSignedBlob = NULL, pPlainText = NULL;
	DWORD cbDecoded = 0, cbSignedBlob = 0, cbSignerCertInfo = 0, plainTextSize = 0;
	string signedPlainText = "";
	HCRYPTMSG hMsg = NULL;
	HCERTSTORE hSignerCertStore = NULL;
	TCHAR pszNameString[256];
	IWSADecryptEnvelopIS(errorCode, signedPlainText, recipientDefaultDN, envelopedMsg); // decode inclu
	cbSignedBlob = signedPlainText.size();
	pbSignedBlob = const_cast<PBYTE>((unsigned char*)signedPlainText.c_str());
	//  Open a message for decoding.
	if (!(hMsg = CryptMsgOpenToDecode(MY_TYPE, 0, 0, NULL, NULL, NULL))) { *errorCode = GetLastError(); return; }
	//  Update the message with an encoded BLOB.
	if (!CryptMsgUpdate(hMsg, pbSignedBlob, cbSignedBlob, TRUE)) { *errorCode = GetLastError(); return; }
	if (!CryptMsgGetParam(hMsg, CMSG_SIGNER_CERT_INFO_PARAM, 0, NULL, &cbSignerCertInfo)) { *errorCode = GetLastError(); return; }
	// Allocate memory.
	if (!(pSignerCertInfo = (PCERT_INFO)malloc(cbSignerCertInfo))) { *errorCode = GetLastError(); return; }
	// Get the message certificate information (CERT_INFO structure).
	if (!(CryptMsgGetParam(hMsg, CMSG_SIGNER_CERT_INFO_PARAM, 0, pSignerCertInfo, &cbSignerCertInfo))) { *errorCode = GetLastError(); return; }
	// Open a certificate store in memory using CERT_STORE_PROV_MSG, which initializes it with the certificates from the message.
	if (!(hSignerCertStore = CertOpenStore(CERT_STORE_PROV_MSG, MY_TYPE, NULL, 0, hMsg))) { *errorCode = GetLastError(); return; }
	// Find the signer's certificate in the store.
	if (pSignerCertContext = CertGetSubjectCertificateFromStore(hSignerCertStore, MY_TYPE, pSignerCertInfo)) {
		if (CertGetNameString(pSignerCertContext, CERT_NAME_RDN_TYPE, 0, NULL, pszNameString, MAX_NAME) > 1)
		{
			signerDefaultDN = pszNameString;
		}
		else { *errorCode = GetLastError(); return; }
	}
	else { *errorCode = GetLastError(); return; }
	//  Get the number of bytes needed for a buffer to hold the decoded message.
	if (!CryptMsgGetParam(hMsg, CMSG_CONTENT_PARAM, 0, NULL, &plainTextSize)) { *errorCode = GetLastError(); return; }
	// Allocate memory.
	if (!(pPlainText = (BYTE*)malloc(plainTextSize))) { *errorCode = GetLastError(); return; }
	// Copy the content to the buffer.
	if (!CryptMsgGetParam(hMsg, CMSG_CONTENT_PARAM, 0, pPlainText, &plainTextSize)) { *errorCode = GetLastError(); return; }
	plainText = string((char*)pPlainText, plainTextSize);
	*errorCode = 0;
	if (pSignerCertInfo) free(pSignerCertInfo);
	if (pPlainText) free(pPlainText);
	if (pSignerCertContext) CertFreeCertificateContext(pSignerCertContext);
	if (hMsg) CryptMsgClose(hMsg);
	if (hSignerCertStore) CertCloseStore(hSignerCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
}

// Get list of the store param
string IWSAgentIS::IWSAGetAllCertsListInfoIS(const string& certStoreSM2, const string& certStoreRSA, const DWORD& keySpec) {
	PCCERT_CONTEXT pCertContext = NULL;
	PBYTE pbDecoded = NULL, pbKeyUsage = NULL;
	LPSTR pIssuer = NULL, pSubject = NULL;
	void* pvData = NULL;
	DWORD cbData = 0, cbDecoded = 0, cIssuer = 0, cSubject = 0, cbKeyUsage = 2, dwPropId = 0, certStoreFlag = -1, loopCounts = 1;
	string certDN, issuerDN, certStore, keyUsage, certType, object_arr_toJS, certJson;
	mpz_t certSN, result;
	SYSTEMTIME validBegin, validEnd, * pst = new SYSTEMTIME();
	vector<string> certsList_vector, certStores;
	HCERTSTORE hCertStore = NULL, hCertStoreTmp = NULL;
	CertificateIS cert;

	mpz_init(result);
	mpz_init(certSN);

	if ("" != certStoreSM2) certStore = certStoreSM2;
	if ("" != certStoreRSA) {
		certType = "RSA";
	}
	if (("" == certStoreSM2) && ("" == certStoreRSA)) return "[]";

	if (getIsStore() && certStoreRSA == cachedCertsType && !cachedCerts.empty()) {
		for (CertificateIS cert : cachedCerts) {
			if (2 == keySpec && "signature" != cert.KeyUsage) continue; // filtre
			cert.toJson(certJson);
			certsList_vector.push_back(certJson);
		}
	}
	else {
		tokenize(certStoreRSA, ',', certStores);
		for (string s : certStores) {
			hCertStore = CertOpenSystemStore(NULL, s.c_str());
			certStore = s;
			while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
				mpz_import(result, 1, -1, pCertContext->pCertInfo->SerialNumber.cbData, 0, 0, pCertContext->pCertInfo->SerialNumber.pbData);
				char* tmp = mpz_get_str(NULL, 16, result);
				mpz_set(certSN, result);
				if (cIssuer = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, NULL, 0)) {
					pIssuer = (LPSTR)malloc(cIssuer);
					if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, pIssuer, cIssuer)) {
						issuerDN = string(pIssuer, cIssuer - 1);
					}
				}
				if (FileTimeToSystemTime(&pCertContext->pCertInfo->NotBefore, pst)) { validBegin = *pst; }
				if (FileTimeToSystemTime(&pCertContext->pCertInfo->NotAfter, pst)) { validEnd = *pst; }
				if (cSubject = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Subject, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, NULL, 0)) {
					pSubject = (LPSTR)malloc(cSubject);
					if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Subject, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, pSubject, cSubject)) {
						certDN = string(pSubject, cSubject - 1); // exclure '\0'
					}
				}
				pbKeyUsage = (PBYTE)malloc(cbKeyUsage);
				CertGetIntendedKeyUsage(MY_TYPE, pCertContext->pCertInfo, pbKeyUsage, cbKeyUsage);
				for (int i = 0; i < cbKeyUsage; ++i) {
					unsigned char flag = pbKeyUsage[i];
					if (int(flag) >> 7 == 1) keyUsage = "signature";
					if (int(flag) >> 4 == 1) keyUsage = "encryption";
					if (int(flag) >> 5 == 1) keyUsage = "envelope";
				}
				cert = CertificateIS(certDN, issuerDN, certSN, validBegin, validEnd, certStore, keyUsage, certType);
				cert.toJson(certJson);
				if (0 == keySpec) {
					certsList_vector.push_back(certJson);
				}
				else if (2 == keySpec) {
					if (cert.KeyUsage == "signature") {
						certsList_vector.push_back(certJson);
					}
				}
				if (getIsStore()) {
					cachedCerts.push_back(cert); // cache all
					cachedCertsType = certStoreRSA;
				}
				keyUsage = "";
			}
		}
	}
	object_arr_toJS = "[";
	for (string s : certsList_vector) {
		object_arr_toJS += s;
		object_arr_toJS += ",";
	}
	if (certsList_vector.size() > 0) object_arr_toJS.replace(object_arr_toJS.end() - 1, object_arr_toJS.end(), "]");
	else object_arr_toJS += "]";

	// Clean up.
	delete pst;
	if (pIssuer) free(pIssuer);
	if (pSubject) free(pSubject);
	if (pbKeyUsage) free(pbKeyUsage);
	mpz_clear(certSN);
	mpz_clear(result);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hCertStore) CertCloseStore(hCertStore, 0);
	if (hCertStoreTmp) CertCloseStore(hCertStoreTmp, 0);
	return object_arr_toJS;
}

/*
3.2.48（方法）返回证书列表中指定证书的公钥
3.2.48.1名称
IWSAGetCertPublicKeyInfoForIndex (CertIndex,SucceedFunction)
3.2.48.2参数
参数名称	类  型	参数描述
CertIndex	整数	证书序号 ，从 0 开始，序号基于获取证书列表函数构建的证书列表，例如:IWSAGetCertsListInfo等
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.2.48.3回调函数参数
参数名称	类  型	返回值说明
CertPublicKeyInfoData	字符串	证书公钥信息

3.2.48.4功能说明
获取NetSignCNG证书列表中指定证书的公钥信息,十六进制显示。
*/
string IWSAgentIS::IWSAGetCertPublicKeyInfoForIndexIS(const DWORD& certIndex) {
	PCCERT_CONTEXT pCertContext = NULL;
	HCERTSTORE hCertStore = NULL;
	DWORD indexCert = 0;
	string publicKeyInfo;
	// Open a system certificate store.
	if (!(hCertStore = this->openMyAndAddressbookStore())) { return ""; }
	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
		if (indexCert++ == certIndex) {
			ByteToStr(pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData, pCertContext->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData, publicKeyInfo);
			break;
		}
	}
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	return publicKeyInfo;
}

/*
3.4.1（方法RSA）枚举出系统中的提供者，返回提供者数组
3.4.1.1名称
IWSA_rsa_csp_listProvider (SucceedFunction)
3.4.1.2参数
参数名称	类  型	参数描述
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.1.3回调函数参数
参数名称	类  型	返回值说明
result	数组	包含提供者

3.4.1.4功能说明
枚举出系统中的提供者，返回提供者数组。
*/
string IWSAgentIS::IWSA_rsa_csp_listProviderIS() {
	PCRYPT_PROVIDERS pBuffer = NULL;
	vector<string> providers;
	string str = "[";
	NTSTATUS status;
	ULONG cbBuffer = 0;
	/*
	Get the providers, letting the BCryptEnumRegisteredProviders function allocate the memory.
	*/
	status = BCryptEnumRegisteredProviders(&cbBuffer, &pBuffer);
	if (NT_SUCCESS(status))
	{
		if (pBuffer != NULL)
		{
			// Enumerate the providers.
			for (ULONG i = 0; i < pBuffer->cProviders; i++)
			{
				wstring ws(pBuffer->rgpszProviders[i]);
				string p = string(ws.begin(), ws.end());
				providers.push_back(p);
			}
		}
	}
	if (NULL != pBuffer)
	{
		/*
		Free the memory allocated by the BCryptEnumRegisteredProviders function.
		*/
		BCryptFreeBuffer(pBuffer);
	}
	for (string s : providers) {
		str += "{\"Provider\":\"" + s;
		str += "\"},";
	}
	if (providers.size() > 0) str.replace(str.end() - 1, str.end(), "]");
	else str += "]";
	return str;
}

/*
3.4.4（方法RSA）高级接口 产生P10
3.4.4.1名称
 IWSA_rsa_csp_AdvgenContainerP10 (KeySize,DN,bDoubleCert,SucceedFunction)
3.4.4.2参数
参数名称	类  型	参数描述
KeySize	String	密钥长度”512”，”1024”， “2048”，”4096”
DN	String	主题DN，空值DN请赋值””空串
bDoubleCert	String	是否为双证，”true”/”false”
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.4.4.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
P10Value	string	P10 结构值
EncKeyPair	string	加密公钥
3.4.4.4功能说明
产生新容器，在容器内生成密钥对，再用公钥生成P10包。根据传入的参考号和授权码生成安全码。
返回Base64编码的P10 包，加密公钥和安全码。

// bDoubleCert unused, 调用两次函数
*/
void IWSAgentIS::IWSA_rsa_csp_AdvgenContainerP10IS(DWORD*& errorCode, string& p10Value, string& encKeyPair, const string& keySize, const string& dn, const BOOL bDoubleCert) {
	PCCERT_CONTEXT pCert = NULL;
	IX500DistinguishedName* subject = NULL;
	IX509CertificateRequestPkcs10* pPkcs10 = NULL;
	IX509PrivateKey* pKey = NULL;
	IX509PublicKey* pPublicKey = NULL;
	BOOL fCoInit = FALSE;
	BSTR strP10Value = NULL, strPubKey = NULL, strTemplateName = NULL;
	DWORD loopCount = 1;

	wstring wdn = wstring(dn.begin(), dn.end());
	if (bDoubleCert) ++loopCount;
	if (KEY_SIZE_512 != keySize && KEY_SIZE_1024 != keySize && KEY_SIZE_2048 != keySize && KEY_SIZE_4096 != keySize) { *errorCode = E_INVALID_KEYSIZE; return; }
	// CoInitializeEx
	if (S_OK != CoInitializeEx(NULL, COINIT_MULTITHREADED)) { *errorCode = GetLastError(); return; }
	fCoInit = TRUE;

	while (loopCount > 0) {
		--loopCount;
		/* Get the public key Create IX509PrivateKey */
		if (S_OK != CoCreateInstance(__uuidof(CX509PrivateKey), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX509PrivateKey), (void**)&pKey)) { *errorCode = GetLastError(); return; }
		pKey->put_Length(atol(keySize.c_str()));
		// Create the key
		if (S_OK != pKey->Create()) { *errorCode = GetLastError(); return; }
		// Export the public key
		if (S_OK != pKey->ExportPublicKey(&pPublicKey)) { *errorCode = GetLastError(); return; }
		// Create IX509CertificateRequestPkcs10
		if (S_OK != CoCreateInstance(__uuidof(CX509CertificateRequestPkcs10), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX509CertificateRequestPkcs10), (void**)&pPkcs10)) { *errorCode = GetLastError(); return; }
		// Allocate BSTR for template name
		strTemplateName = SysAllocString(wdn.c_str());
		if (NULL == strTemplateName) { *errorCode = E_OUTOFMEMORY; return; }
		// Initialize IX509CertificateRequestPkcs10 from public key
		if (S_OK != pPkcs10->InitializeFromPublicKey(ContextUser, pPublicKey, NULL)) { *errorCode = GetLastError(); return; } // BUG can't set subject name
		// Create CN
		if (S_OK != CoCreateInstance(__uuidof(CX500DistinguishedName), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX500DistinguishedName), (void**)&subject)) { *errorCode = GetLastError(); return; }
		else {
			subject->Encode(strTemplateName, XCN_CERT_NAME_STR_NONE);
			pPkcs10->put_Subject(subject);
		}
		if (S_OK != pPkcs10->Encode()) { *errorCode = GetLastError(); return; }
		if (S_OK != pPkcs10->get_RawDataToBeSigned((EncodingType)(XCN_CRYPT_STRING_BASE64 | XCN_CRYPT_STRING_NOCRLF), &strP10Value)) { *errorCode = GetLastError(); return; }
		if (S_OK != pPublicKey->get_EncodedKey((EncodingType)(XCN_CRYPT_STRING_BASE64 | XCN_CRYPT_STRING_NOCRLF), &strPubKey)) { *errorCode = GetLastError(); return; }
		if (bDoubleCert && 1 == loopCount || !bDoubleCert) {
			wstring ws1(strP10Value, SysStringLen(strP10Value));
			p10Value = string(ws1.begin(), ws1.end());
		}
		if (bDoubleCert) {
			wstring ws2(strPubKey, SysStringLen(strPubKey));
			encKeyPair = string(ws2.begin(), ws2.end());
		}
	}

	*errorCode = 0;
	if (pCert) CertFreeCertificateContext(pCert);
	if (fCoInit) CoUninitialize();
}

/*
3.4.5（方法RSA）高级接口 导入签名证书和加密证书
3.4.5.1名称
IWSA_rsa_csp_AdvImportSignEncCert (CertType,SignCert,EncCert,EncPrikey,UKEK, SucceedFunction)
3.4.5.2参数
参数名称	类  型	参数描述
CertType	String 	证书类型， "X509" or "P7"
SignCert	String	P7包，封装证书链，Base64编码。可以只有X509用户证书或X509根证书加X509用户证书或X509根证书加多张X509中级证书加X509用户证书
或者 X509证书
EncCert	String	P7包，封装证书链，Base64编码。可以只有X509用户证书或X509根证书加X509用户证书或X509根证书加多张X509中级证书加X509用户证书
或者 X509证书
EncPrikey	String	加密私钥，Base64编码
UKEK	String	对称密钥包，Base64编码
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.4.5.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.5.4功能说明
导入签名和加密证书。
*/
void IWSAgentIS::IWSA_rsa_csp_AdvImportSignEncX509CertIS(DWORD*& errorCode, const string& cspName, const string& signCert, const string& encCert, const string& encPriKey, const string& ukek) {
	PBYTE pbCertDecoded = NULL, pbKeyBlob = NULL, pbBlob = NULL, pbPriKeyEncoded = NULL, pbCertEncoded = NULL;
	PCCERT_CONTEXT pCertContext = NULL;
	BCRYPT_RSAKEY_BLOB* pRsaKeyBlob = NULL;
	PUCHAR pbKeyObject = NULL;
	DWORD cbCertDecoded, keyUsage = NCRYPT_ALLOW_DECRYPT_FLAG, cbKeyBlob, cbBlob, cbPriKeyEncoded, cbCertEncoded, type = CERT_X500_NAME_STR, nameBufSize;
	HCERTSTORE hRootStore = NULL, hCAStore = NULL, hSignCertStore = NULL, hProvPKCS7Store = NULL, hEncryptCertStore = NULL, hCertStore = NULL;
	CRYPT_INTEGER_BLOB pkcs7Blob;
	ULONG cbKeyObject, cbData;
	NCRYPT_PROV_HANDLE hProvider = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	BCRYPT_ALG_HANDLE hAesAlg = NULL, hUkekKey = NULL;
	vector<PCCERT_CONTEXT> pCertContexts;
	TCHAR pszNameString[256], pszIssuerNameString[256];
	string container = "";
	CRYPT_KEY_PROV_INFO cryptKeyProvInfo;

	ZeroMemory(&cryptKeyProvInfo, sizeof(cryptKeyProvInfo));
	IWSABase64DecodeIS(errorCode, pbCertEncoded, cbCertEncoded, signCert);
	if (*errorCode) { return; }
	pCertContext = CertCreateCertificateContext(MY_TYPE, pbCertEncoded, cbCertEncoded);
	if (NULL == pCertContext) { *errorCode = GetLastError(); return; }
	// Add cert to store
	hSignCertStore = CertOpenSystemStore(NULL, this->defaultSignCertStore.c_str());
	if (NULL == hSignCertStore) { *errorCode = E_FAIL_OPENSTORE; return; }
	if (!CertAddCertificateContextToStore(hSignCertStore, pCertContext, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { *errorCode = GetLastError(); return; }
	// Decode x509Cert and encPriKey to bytes
	IWSABase64DecodeIS(errorCode, pbCertEncoded, cbCertEncoded, encCert);
	IWSABase64DecodeIS(errorCode, pbPriKeyEncoded, cbPriKeyEncoded, encPriKey);
	// Create cert
	pCertContext = CertCreateCertificateContext(MY_TYPE, pbCertEncoded, cbCertEncoded);
	if (NULL == pCertContext) { *errorCode = GetLastError(); return; }
	if (!CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, 0, &type, pszNameString, MAX_NAME)) { *errorCode = GetLastError(); return; }
	container += string(pszNameString);
	if (!CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszNameString, MAX_NAME)) { *errorCode = GetLastError(); return; }
	container += " | " + string(pszNameString);
	wstring wcontainer = wstring(container.begin(), container.end());
	cryptKeyProvInfo.pwszContainerName = const_cast<LPWSTR>(wcontainer.c_str());
	cryptKeyProvInfo.pwszProvName = const_cast<LPWSTR>(MS_KEY_STORAGE_PROVIDER);
	cryptKeyProvInfo.dwProvType = 0;
	cryptKeyProvInfo.dwKeySpec = 0;
	cryptKeyProvInfo.dwFlags = 0;

	// Decode from certificate to key bytes
	if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbPriKeyEncoded, cbPriKeyEncoded, 0, NULL, NULL, &cbKeyBlob)) { *errorCode = GetLastError(); return; }
	pRsaKeyBlob = (BCRYPT_RSAKEY_BLOB*)LocalAlloc(0, cbKeyBlob);
	if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbPriKeyEncoded, cbPriKeyEncoded, 0, NULL, pRsaKeyBlob, &cbKeyBlob)) { *errorCode = GetLastError(); return; }
	// Generate private key handle
	if (ERROR_SUCCESS == NCryptOpenStorageProvider(&hProvider, MS_KEY_STORAGE_PROVIDER, 0)) {
		if (ERROR_SUCCESS != NCryptImportKey(hProvider, NULL, BCRYPT_RSAPRIVATE_BLOB, NULL, &hKey, (PBYTE)pRsaKeyBlob, cbKeyBlob, NCRYPT_DO_NOT_FINALIZE_FLAG | NCRYPT_OVERWRITE_KEY_FLAG)) { *errorCode = GetLastError(); return; }
		if (ERROR_SUCCESS != NCryptSetProperty(hKey, NCRYPT_KEY_USAGE_PROPERTY, (PBYTE)&keyUsage, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) { *errorCode = GetLastError(); return; }
		if (ERROR_SUCCESS != NCryptFinalizeKey(hKey, 0)) { *errorCode = GetLastError(); return; }
	}

	// Update the private key of the certificate and set the conteneur
	if (!CertSetCertificateContextProperty(pCertContext, CERT_NCRYPT_KEY_HANDLE_PROP_ID, 0, &hKey)) { *errorCode = GetLastError(); return; }
	if (!CertSetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, 0, &cryptKeyProvInfo)) { *errorCode = GetLastError(); return; }

	// Add cert to store
	hEncryptCertStore = CertOpenSystemStore(NULL, defaultDecryptCertStore.c_str());
	if (NULL != hEncryptCertStore) { CertAddCertificateContextToStore(hEncryptCertStore, pCertContext, CERT_STORE_ADD_REPLACE_EXISTING, NULL); }

	IWSABase64DecodeIS(errorCode, pbBlob, cbBlob, ukek);
	// Open an algorithm handle.
	if (STATUS_SUCCESS != BCryptOpenAlgorithmProvider(&hAesAlg, BCRYPT_AES_ALGORITHM, NULL, 0)) { *errorCode = GetLastError(); return; }
	// Calculate the size of the buffer to hold the KeyObject.
	if (STATUS_SUCCESS != BCryptGetProperty(hAesAlg, BCRYPT_OBJECT_LENGTH, (PBYTE)&cbKeyObject, sizeof(DWORD), &cbData, 0)) { *errorCode = GetLastError(); return; }
	// Allocate the key object on the heap.
	pbKeyObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbKeyObject);
	if (NULL == pbKeyObject) { *errorCode = GetLastError(); return; }
	if (STATUS_SUCCESS != BCryptImportKey(hAesAlg, NULL, BCRYPT_OPAQUE_KEY_BLOB, &hUkekKey, pbKeyObject, cbKeyObject, pbBlob, cbBlob, 0)) { *errorCode = GetLastError(); return; }
	this->setSymmetryKey(hUkekKey);

	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hKey) NCryptFreeObject(hKey);
	if (hProvider) NCryptFreeObject(hProvider);
	if (hSignCertStore) CertCloseStore(hSignCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hEncryptCertStore) CertCloseStore(hEncryptCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
}

void IWSAgentIS::IWSA_rsa_csp_AdvImportSignEncP7CertIS(DWORD*& errorCode, const string& cspName, const string& signCert, const string& encCert, const string& encPriKey, const string& ukek) {
	PBYTE pbCertDecoded = NULL, pbKeyBlob = NULL, pbBlob = NULL, pbPriKeyEncoded = NULL, pbCertEncoded = NULL;
	PCCERT_CONTEXT pCertContext = NULL;
	BCRYPT_RSAKEY_BLOB* pRsaKeyBlob = NULL;
	PUCHAR pbKeyObject = NULL;
	DWORD cbCertDecoded, keyUsage = NCRYPT_ALLOW_DECRYPT_FLAG, cbKeyBlob, cbBlob, cbPriKeyEncoded, cbCertEncoded, type = CERT_X500_NAME_STR, nameBufSize;
	HCERTSTORE hRootStore = NULL, hCAStore = NULL, hSignCertStore = NULL, hProvPKCS7Store = NULL, hEncryptCertStore = NULL, hCertStore = NULL;
	CRYPT_INTEGER_BLOB pkcs7Blob;
	ULONG cbKeyObject, cbData;
	NCRYPT_PROV_HANDLE hProvider = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	BCRYPT_ALG_HANDLE hAesAlg = NULL, hUkekKey = NULL;
	vector<PCCERT_CONTEXT> pCertContexts;
	TCHAR pszNameString[256], pszIssuerNameString[256];

	hSignCertStore = CertOpenSystemStore(NULL, this->defaultSignCertStore.c_str());
	hEncryptCertStore = CertOpenSystemStore(NULL, this->defaultDecryptCertStore.c_str());
	hRootStore = CertOpenSystemStore(NULL, ROOT);
	hCAStore = CertOpenSystemStore(NULL, CA);
	if (NULL == hSignCertStore || NULL == hRootStore || NULL == hCAStore) { *errorCode = E_FAIL_OPENSTORE; return; }
	// Decode p7Cert to bytes
	IWSABase64DecodeIS(errorCode, pbCertDecoded, cbCertDecoded, signCert);
	if (*errorCode) { return; }
	// Create certs
	// 1.Fabriquer un magasin de certificats PKCS7
	pkcs7Blob.cbData = cbCertDecoded;
	pkcs7Blob.pbData = pbCertDecoded;
	hProvPKCS7Store = CertOpenStore(CERT_STORE_PROV_PKCS7, MY_TYPE, NULL, 0, &pkcs7Blob);
	if (NULL == hProvPKCS7Store) { *errorCode = E_FAIL_OPENSTORE; return; }
	// 2.Ajouter les certificats dans son magasin, les certificats sont différentiés par leurs issuers.
	while (pCertContext = CertEnumCertificatesInStore(hProvPKCS7Store, pCertContext)) {
		pCertContexts.push_back(pCertContext);
	}
	for (PCCERT_CONTEXT pc : pCertContexts) {
		BOOL flag = FALSE;
		if (CertGetNameString(pc, CERT_NAME_RDN_TYPE, 0, &type, pszNameString, MAX_NAME)) {
			if (CertGetNameString(pc, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszIssuerNameString, MAX_NAME)) {
				if (string(pszNameString) == string(pszIssuerNameString)) {
					if (!CertAddCertificateContextToStore(hRootStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { *errorCode = GetLastError(); return; }
					continue;
				}
			}
		}
		for (PCCERT_CONTEXT pc1 : pCertContexts) {
			if (CertGetNameString(pc1, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszIssuerNameString, MAX_NAME)) {
				if (string(pszNameString) == string(pszIssuerNameString)) { // Si pc n'est pas root et si pc est l'issuer du pc1 => pc est un ca.
					if (!CertAddCertificateContextToStore(hCAStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { *errorCode = GetLastError(); return; }
					flag = TRUE;
					break;
				}
			}
		}
		if (!flag) {
			// pc n'est pas l'issuer du tout le pc1 => pc est un x509 final
			if (!CertAddCertificateContextToStore(hSignCertStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { *errorCode = GetLastError(); return; }
		}
	}
	pCertContext = NULL;
	pCertContexts.clear();
	// Decode p7Cert and encPriKey to bytes
	IWSABase64DecodeIS(errorCode, pbCertDecoded, cbCertDecoded, encCert);
	IWSABase64DecodeIS(errorCode, pbPriKeyEncoded, cbPriKeyEncoded, encPriKey);
	// Decode from certificate to key bytes
	if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbPriKeyEncoded, cbPriKeyEncoded, 0, NULL, NULL, &cbKeyBlob)) { *errorCode = GetLastError(); return; }
	pRsaKeyBlob = (BCRYPT_RSAKEY_BLOB*)LocalAlloc(0, cbKeyBlob);
	if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbPriKeyEncoded, cbPriKeyEncoded, 0, NULL, pRsaKeyBlob, &cbKeyBlob)) { *errorCode = GetLastError(); return; }
	// Generate private key handle
	if (ERROR_SUCCESS == NCryptOpenStorageProvider(&hProvider, MS_KEY_STORAGE_PROVIDER, 0)) {
		if (ERROR_SUCCESS != NCryptImportKey(hProvider, NULL, BCRYPT_RSAPRIVATE_BLOB, NULL, &hKey, (PBYTE)pRsaKeyBlob, cbKeyBlob, NCRYPT_DO_NOT_FINALIZE_FLAG | NCRYPT_OVERWRITE_KEY_FLAG)) { *errorCode = GetLastError(); return; }
		if (ERROR_SUCCESS != NCryptSetProperty(hKey, NCRYPT_KEY_USAGE_PROPERTY, (PBYTE)&keyUsage, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) { *errorCode = GetLastError(); return; }
		if (ERROR_SUCCESS != NCryptFinalizeKey(hKey, 0)) { *errorCode = GetLastError(); return; }
	}
	// Create certs
	// 1.Fabriquer un magasin de certificats PKCS7
	pkcs7Blob.cbData = cbCertDecoded;
	pkcs7Blob.pbData = pbCertDecoded;
	hProvPKCS7Store = CertOpenStore(CERT_STORE_PROV_PKCS7, MY_TYPE, NULL, 0, &pkcs7Blob);
	// 2.Ajouter les certificats dans son magasin, les certificats sont différentiés par leurs issuers.
	while (pCertContext = CertEnumCertificatesInStore(hProvPKCS7Store, pCertContext)) {
		pCertContexts.push_back(pCertContext);
	}
	for (PCCERT_CONTEXT pc : pCertContexts) {
		BOOL flag = FALSE;
		if (CertGetNameString(pc, CERT_NAME_RDN_TYPE, 0, &type, pszNameString, MAX_NAME)) {
			if (CertGetNameString(pc, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszIssuerNameString, MAX_NAME)) {
				if (string(pszNameString) == string(pszIssuerNameString)) {
					CertAddCertificateContextToStore(hRootStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL); // Try to add root cert to ROOT store, not fail when root cert exists
					continue;
				}
			}
		}
		for (PCCERT_CONTEXT pc1 : pCertContexts) {
			if (CertGetNameString(pc1, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszIssuerNameString, MAX_NAME)) {
				if (string(pszNameString) == string(pszIssuerNameString)) { // Si pc n'est pas root et si pc est l'issuer du pc1 => pc est un ca.
					CertAddCertificateContextToStore(hCAStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL); // Try to add root cert to CA store, not fail when ca cert exists
					flag = TRUE;
					break;
				}
			}
		}
		if (!flag) {
			// pc n'est pas l'issuer du tout le pc1 => pc est un x509 final
			if (!CertSetCertificateContextProperty(pc, CERT_NCRYPT_KEY_HANDLE_PROP_ID, NULL, &hKey)) { *errorCode = GetLastError(); return; }
			if (!CertAddCertificateContextToStore(hEncryptCertStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { *errorCode = GetLastError(); return; }
		}
	}

	IWSABase64DecodeIS(errorCode, pbBlob, cbBlob, ukek);
	// Open an algorithm handle.
	if (STATUS_SUCCESS != BCryptOpenAlgorithmProvider(&hAesAlg, BCRYPT_AES_ALGORITHM, NULL, 0)) { *errorCode = GetLastError(); return; }
	// Calculate the size of the buffer to hold the KeyObject.
	if (STATUS_SUCCESS != BCryptGetProperty(hAesAlg, BCRYPT_OBJECT_LENGTH, (PBYTE)&cbKeyObject, sizeof(DWORD), &cbData, 0)) { *errorCode = GetLastError(); return; }
	// Allocate the key object on the heap.
	pbKeyObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbKeyObject);
	if (NULL == pbKeyObject) { *errorCode = GetLastError(); return; }
	if (STATUS_SUCCESS != BCryptImportKey(hAesAlg, NULL, BCRYPT_OPAQUE_KEY_BLOB, &hUkekKey, pbKeyObject, cbKeyObject, pbBlob, cbBlob, 0)) { *errorCode = GetLastError(); return; }
	this->setSymmetryKey(hUkekKey);

	if (hKey) NCryptFreeObject(hKey);
	if (hProvider) NCryptFreeObject(hProvider);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hSignCertStore) CertCloseStore(hSignCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hRootStore) CertCloseStore(hRootStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hCAStore) CertCloseStore(hCAStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hProvPKCS7Store) CertCloseStore(hProvPKCS7Store, CERT_CLOSE_STORE_CHECK_FLAG);
}

/*
3.4.2（方法RSA）获取系统中的RSA证书个数
3.4.2.1名称
IWSA_rsa_csp_getCountOfCert (SucceedFunction)
3.4.2.2参数
参数名称	类  型	参数描述
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.2.3回调函数参数
参数名称	类  型	返回值说明
CertCount	整数	证书个数

3.4.2.4功能说明
获取系统中的RSA证书个数。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_getCountOfCertIS() {
	PCCERT_CONTEXT pCertContext = NULL;
	HCERTSTORE hCertStore = NULL;
	DWORD indexCert = 0;
	hCertStore = this->openMyAndAddressbookStore();
	if (NULL == hCertStore) { return E_FAIL_OPENSTORE; }
	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
		++indexCert;
	}
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	return indexCert;
}

/*
3.4.3（方法RSA）获取指定索引的证书信息，返回数组
3.4.3.1名称
IWSA_rsa_csp_getCertInfo (CertIndex,SucceedFunction)
3.4.3.2参数
参数名称	类  型	参数描述
CertIndex	string	从零开始
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.3.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
Provider	string	提供者
Container	string	容器名
DN	string	证书主题
Issuer	string	证书颁发者
StartDate	string	证书起始时间
EndDate	string	证书结束时间
CertSN	string	证书序列号 格式："02X02X…"
CertPurpose	string	证书用途
3.4.3.4功能说明
获取指定索引的证书信息，返回数组。
*/
string IWSAgentIS::IWSA_rsa_csp_getCertInfoIS(const DWORD& certIndex) {
	PBYTE pbDecoded = NULL, pbKeyUsage = NULL, pbName = NULL;
	LPSTR pIssuer = NULL, pSubject = NULL;  // variable to hold a pointer to the decoded buffer
	LPWSTR pStoreName = NULL;
	PCCERT_CONTEXT pCertContext = NULL;
	PCRYPT_KEY_PROV_INFO pKeyProvInfo = NULL;
	string certDN, certDNSimple, issuerDN, certStore, keyUsage, certType, object_arr_toJS, signature_algorithm, certJson, publicKeyInfo, container, provider = string(this->csp.begin(),this->csp.end()), validBegin_str, validEnd_str, serialnumber;
	ostringstream validBegin_strStream, validEnd_strStream;
	mpz_t certSN, result;
	SYSTEMTIME validBegin, validEnd, *pst = new SYSTEMTIME();
	DWORD cbData, cbDecoded, cIssuer, cSubject, cSubjectSimple, cbKeyUsage = 1, dwProviderCount, bStoreName, certStoreFlag = -1, indexCert = 0, dwPropId = 0, cKeyProvInfo, cbName, pcbNameResult, cbProvData, errorCode = 0, chKeyHandle;
	CertificateIS cert;
	HCERTSTORE hCertStore = NULL, hCertStoreTmp = NULL;
	NTSTATUS status = 0;
	ULONG flags = 0;
	UCHAR c_seed[128] = { 0 };
	vector<string> storeTypes;
	
	mpz_init(certSN);
	mpz_init(result);

	// Open a collection certificate store.
	if (!(hCertStore = CertOpenStore(CERT_STORE_PROV_COLLECTION, MY_TYPE, NULL, 0, NULL))) { return "[]"; }
	for (DWORD d : systemStoreLocation) {
		for (string name : storeNames) {
			wstring ws = wstring(name.begin(), name.end());
			if (hCertStoreTmp = CertOpenStore(CERT_STORE_PROV_SYSTEM, MY_TYPE, NULL, d | CERT_STORE_READONLY_FLAG, ws.c_str())) {
				if (CertAddStoreToCollection(hCertStore, hCertStoreTmp, CERT_PHYSICAL_STORE_ADD_ENABLE_FLAG, 0)) {
					while (pCertContext = CertEnumCertificatesInStore(hCertStoreTmp, pCertContext)) {
						storeTypes.push_back(name);
					}
				}
			}
		}
	}
	
	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
		if (indexCert++ == certIndex) {
			certStore = storeTypes.at(indexCert - 1);
			break;
		}
	}

	if (NULL == pCertContext) { return "";  }

	if (cSubject = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Subject, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, NULL, 0)) {
		pSubject = (LPSTR)malloc(cSubject);
		if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Subject, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, pSubject, cSubject)) {
			certDN = string(pSubject, cSubject - 1); // exclure '\0'
		}
	}

	mpz_import(result, 1, -1, pCertContext->pCertInfo->SerialNumber.cbData, 0, 0, pCertContext->pCertInfo->SerialNumber.pbData);
	char* tmp = mpz_get_str(NULL, 16, result);
	mpz_set(certSN, result);

	if (cIssuer = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, NULL, 0)) {
		pIssuer = (LPSTR)malloc(cIssuer);
		if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, pIssuer, cIssuer)) {
			issuerDN = string(pIssuer, cIssuer - 1);
		}
	}

	if (FileTimeToSystemTime(&pCertContext->pCertInfo->NotBefore, pst)) { validBegin = *pst; }
	if (FileTimeToSystemTime(&pCertContext->pCertInfo->NotAfter, pst)) { validEnd = *pst; }

	if (pbKeyUsage = (PBYTE)malloc(cbKeyUsage)) {
		if (CertGetIntendedKeyUsage(MY_TYPE, pCertContext->pCertInfo, pbKeyUsage, cbKeyUsage)) {
			for (int i = 0; i < cbKeyUsage; ++i) {
				unsigned char flag = pbKeyUsage[i];
				if (1 == int(flag) >> 7 ) keyUsage = "signature";
				if (1 == int(flag) >> 4 ) keyUsage = "encryption";
				if (1 == int(flag) >> 5 ) keyUsage = "envelope";
			}
		}
	}

	if (string(szOID_RSA_RSA) == string(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId)) { certType = "RSA"; }

	cert = CertificateIS(certDN, issuerDN, certSN, validBegin, validEnd, certStore, keyUsage, certType);
	cert.toJson(certJson);

	// seul moyen marche
	if (CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, NULL, &cKeyProvInfo))
	{
		if (pKeyProvInfo = (PCRYPT_KEY_PROV_INFO)malloc(cKeyProvInfo)) {
			CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, pKeyProvInfo, &cKeyProvInfo);
			wstring ws1(pKeyProvInfo->pwszContainerName);
			wstring ws2(pKeyProvInfo->pwszProvName);
			container = string(ws1.begin(), ws1.end());
			provider = string(ws2.begin(), ws2.end());
			HCRYPTPROV_OR_NCRYPT_KEY_HANDLE hCryptProvOrNCryptKey;
			DWORD dwKeySpec;
			if (CryptAcquireCertificatePrivateKey(pCertContext, 0, 0, &hCryptProvOrNCryptKey, &dwKeySpec, FALSE)) {
				// deprecated only way to not cng provider
				BYTE* pbData;
				DWORD pdwDataLen;
				if (CryptGetProvParam(hCryptProvOrNCryptKey, PP_UNIQUE_CONTAINER, NULL, &pdwDataLen, 0)) {
					if (pbData = (BYTE*)malloc(pdwDataLen)) {
						if (CryptGetProvParam(hCryptProvOrNCryptKey, PP_UNIQUE_CONTAINER, pbData, &pdwDataLen, 0)) {
							container = string((char*)pbData, pdwDataLen - 1);
						}
					}
				}
			}
		}
	}

	validBegin_strStream << validBegin.wDayOfWeek << " " << validBegin.wDay << " " << validBegin.wMonth << " " << validBegin.wYear << " " << validBegin.wHour << ":" << validBegin.wMinute << ":" << validBegin.wSecond;
	validEnd_strStream << validEnd.wDayOfWeek << " " << validEnd.wDay << " " << validEnd.wMonth << " " << validEnd.wYear << " " << validEnd.wHour << ":" << validEnd.wMinute << ":" << validEnd.wSecond;
	validBegin_str = validBegin_strStream.str();
	validEnd_str = validEnd_strStream.str();
	serialnumber = tmp;
	
	certJson.replace(certJson.find_first_of("{"), 1, "").replace(certJson.find_last_of("}"), 1, "");
	certJson = "[{\"errorCode\":\"" + to_string(errorCode) + "\",\"Provider\":\"" + provider + "\",\"Container\":\"" + container + "\",\"DN\":\"" + certDN + "\",\"Issuer\":\"" + issuerDN + "\",\"StartDate\":\"" + validBegin_str + "\",\"EndDate\":\"" + validEnd_str + "\",\"CertSN\":\"" + serialnumber + "\",\"CertPurpose\":\"" + keyUsage + "\"}]";
	
	if (pSubject) free(pSubject);
	if (pbKeyUsage) free(pbKeyUsage);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hCertStoreTmp) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	return certJson;
}

/*
3.4.9（方法RSA）删除一个容器，容器中的签名证书和加密证书同时删除，如果有的话
3.4.9.1名称
IWSA_rsa_csp_delContainer (CspName,Container,SucceedFunction)
3.4.9.2参数
参数名称	类  型	参数描述
CspName	string	CSP名称
Container	string	容器名称
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.9.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.9.4功能说明
删除一个容器，容器中的签名证书和加密证书同时删除，如果有的话。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_delContainerIS(const string& csp, const string& container) {
	PCCERT_CONTEXT pCertContext = NULL;
	PCRYPT_KEY_PROV_INFO pCryptKeyProvInfo = NULL;
	HCERTSTORE hCertStore = NULL;
	DWORD dwCryptKeyProvInfo = 0, errorCode = 0;

	wstring wcontainer = wstring(container.begin(), container.end());
	hCertStore = this->openMyAndAddressbookStore();
	if (NULL == hCertStore) { return E_FAIL_OPENSTORE; }
	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
		if (CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, NULL, &dwCryptKeyProvInfo)) {
			pCryptKeyProvInfo = (PCRYPT_KEY_PROV_INFO)malloc(dwCryptKeyProvInfo);
			CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, pCryptKeyProvInfo, &dwCryptKeyProvInfo);
			if (wcontainer == wstring(pCryptKeyProvInfo->pwszContainerName)) {
				break;
			}
		}
	}
	if (NULL != pCertContext) {
		if (MS_KEY_STORAGE_PROVIDER_STR == csp || MS_SMART_CARD_KEY_STORAGE_PROVIDER_STR == csp || MS_PLATFORM_CRYPTO_PROVIDER_STR == csp || MS_PRIMITIVE_PROVIDER_STR == csp || AMD_PROVIDER == csp) {
			this->IWSA_rsa_csp_deleteContainerIS(csp, container);
			if (!CertDeleteCertificateFromStore(pCertContext)) { return GetLastError(); }
		}
	}
	if (pCryptKeyProvInfo) free(pCryptKeyProvInfo);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	return 0;
}

/*
3.4.10（方法RSA）创建新容器
3.4.10.1名称
IWSA_rsa_csp_genContainer (SucceedFunction)
3.4.10.2参数
参数名称	类  型	参数描述
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.10.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
Container	string	容器的名称
3.4.10.4功能说明
返回创建的容器名称。

not used
容器与密钥绑定?
*/
void IWSAgentIS::IWSA_rsa_csp_genContainerIS(DWORD*& errorCode, string& container) { // pas complet
	container = this->getTmpContainerName();
	*errorCode = 0;
}

/*
3.4.8（方法RSA）创建指定名称的容器
3.4.8.1名称
IWSA_rsa_csp_createContainer (Container,SucceedFunction)
3.4.8.2参数
参数名称	类  型	参数描述
Container	string	容器的名称
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.8.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.8.4功能说明
创建指定名称的容器。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_createContainerIS(const string& csp, const string& container) {
	PBYTE pbBuffer = NULL, pbKeyBlob = NULL, pbBlob = NULL;
	NCRYPT_PROV_HANDLE hProvider = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	DWORD dwBufferLen = 0, cbKeyBlob = 0, cbBlob = 0;
	ifstream privateKey(PRIVATE_KEY_RSA_DEFAULT);
	string privateKeyPEM((std::istreambuf_iterator<char>(privateKey)), std::istreambuf_iterator<char>());
	wstring wcontainer = wstring(container.begin(), container.end());
	this->setTmpContainerName(container);
	this->csp = wstring(csp.begin(), csp.end());
	if (!CryptStringToBinary(privateKeyPEM.c_str(), privateKeyPEM.size(), CRYPT_STRING_BASE64HEADER, NULL, &dwBufferLen, NULL, NULL)) { return GetLastError(); }
	pbBuffer = (PBYTE)malloc(dwBufferLen);
	if (!CryptStringToBinary(privateKeyPEM.c_str(), privateKeyPEM.size(), CRYPT_STRING_BASE64HEADER, pbBuffer, &dwBufferLen, NULL, NULL)) { return GetLastError();  }
	if (!CryptDecodeObjectEx(MY_TYPE, PKCS_RSA_PRIVATE_KEY, pbBuffer, dwBufferLen, 0, NULL, NULL, &cbKeyBlob)) { return GetLastError(); }
	pbKeyBlob = (PBYTE)malloc(cbKeyBlob);
	if (!CryptDecodeObjectEx(MY_TYPE, PKCS_RSA_PRIVATE_KEY, pbBuffer, dwBufferLen, 0, NULL, pbKeyBlob, &cbKeyBlob)) { return GetLastError(); }
	if (ERROR_SUCCESS == NCryptOpenStorageProvider(&hProvider, this->csp.c_str(), 0)) {
		if (ERROR_SUCCESS == NCryptCreatePersistedKey(hProvider, &hKey, NCRYPT_RSA_ALGORITHM, wcontainer.c_str(), AT_SIGNATURE, NCRYPT_OVERWRITE_KEY_FLAG)) {
			if (ERROR_SUCCESS != NCryptSetProperty(hKey, BCRYPT_PRIVATE_KEY_BLOB, pbKeyBlob, cbKeyBlob, 0)) { return GetLastError(); }
			if (ERROR_SUCCESS != NCryptFinalizeKey(hKey, 0)) { return GetLastError(); }
		}
	}
	if (pbBuffer) free(pbBuffer);
	if (pbKeyBlob) free(pbKeyBlob);
	if (hKey) NCryptFreeObject(hKey);
	if (hProvider) NCryptFreeObject(hProvider);
	return 0;
}

/*
3.4.7（方法RSA）删除指定名称的容器，不删除与此容器关联的证书
3.4.7.1名称
IWSA_rsa_csp_deleteContainer (Container,SucceedFunction)
3.4.7.2参数
参数名称	类  型	参数描述
Container	string	容器的名称
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.7.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.7.4功能说明
删除指定名称的容器，不删除与此容器关联的证书。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_deleteContainerIS(const string& csp, const string& container) { // pas complet tous providers
	NCRYPT_PROV_HANDLE hProvider = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	wstring wcontainer = wstring(container.begin(), container.end());
	this->csp = wstring(csp.begin(), csp.end());
	this->setTmpContainerName(container);
	if (ERROR_SUCCESS != NCryptOpenStorageProvider(&hProvider, NULL, 0)) { return GetLastError(); }
	if (ERROR_SUCCESS == NCryptOpenKey(hProvider, &hKey, wcontainer.c_str(), 0, 0)) {
		if (ERROR_SUCCESS != NCryptDeleteKey(hKey, 0)) { return GetLastError(); }
	}
	else { return GetLastError(); }	
	if (hKey) NCryptFreeObject(hKey);
	if (hProvider) NCryptFreeObject(hProvider);
	return 0;
}

/*
3.4.11方法RSA）生成P10包，新容器
3.4.11.1名称
IWSA_rsa_csp_genContainerP10 (bSign,KeySize,DN,DigestOID,PubKeyAlgOID,SignAlgOID,bExport,bProtect,SucceedFunction)
3.4.11.2参数

参数名称	类  型	参数描述
bSign	string	签名密钥或加密密钥标识,”true”/”false”
KeySize	string	密钥长度512，1024， 2048，4096
DN	string	主题DN，空值DN请赋值””空串
DigestOID	string	摘要算法OID，使用默认值请赋值””空串
PubKeyAlgOID	string	公钥算法OID，，使用默认值请赋值””空串
SignAlgOID	string	签名算法OID，，使用默认值请赋值””空串
bExport	string	私钥是否可导出,”true”/”false”
bProtect	string	是否启用增强密钥保护,”true”/”false”
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.11.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
Container	String	容器名称
P10	String	P10 结构值
3.4.11.4功能说明
产生新容器，在容器内生成密钥对，再用公钥生成P10包，返回容器名和Base64编码的P10 包数组。

bSign not infected

*/
string IWSAgentIS::IWSA_rsa_csp_genContainerP10IS(DWORD*& errorCode, string& container, string& p10, BOOL bSign, const string& keySize, const string& dn, const string& digestOID, const string& pubKeyAlgOID, const string& signAlgOID, BOOL bExport, BOOL bProtect) {
	IX509CertificateRequestPkcs10 *pPkcs10 = NULL;
	IX509PrivateKey *pKey = NULL;
	IX509PublicKey *pPublicKey = NULL;
	IX500DistinguishedName *subject = NULL;
	IObjectId *objId = NULL, *objSignId = NULL, *objEncId = NULL;
	BSTR strTemplateName = NULL, strP10Value = NULL, containerNamePrefix = NULL, digestAlgo = NULL, pubKeyAlgo = NULL, signAlgo = NULL;
	HRESULT hr = S_OK;
	bool fCoInit = false;
	BYTE    buffer[16];
	DWORD   bufferSize;
	string  retValue, sz;
	wstring signOid, pubOid, dOid, wdn = wstring(dn.begin(), dn.end());

	if (KEY_SIZE_512 != keySize && KEY_SIZE_1024 != keySize && KEY_SIZE_2048 != keySize && KEY_SIZE_4096 != keySize) { *errorCode = E_INVALID_KEYSIZE; return ""; }
	// CoInitializeEx
	if (S_OK != CoInitializeEx(NULL, COINIT_MULTITHREADED)) { *errorCode = GetLastError(); return ""; }
	fCoInit = true;
	// Create IX509PrivateKey
	if (S_OK != CoCreateInstance(__uuidof(CX509PrivateKey), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX509PrivateKey), (void**)&pKey)) { *errorCode = GetLastError(); return ""; }
	pKey->put_Length(atol(keySize.c_str()));
	if (TRUE == bExport) { pKey->put_ExportPolicy(XCN_NCRYPT_ALLOW_EXPORT_FLAG); }
	else { pKey->put_ExportPolicy(XCN_NCRYPT_ALLOW_EXPORT_NONE); }
	if (TRUE == bProtect) { pKey->put_KeyProtection(XCN_NCRYPT_UI_FORCE_HIGH_PROTECTION_FLAG); }
	else { pKey->put_KeyProtection(XCN_NCRYPT_UI_NO_PROTECTION_FLAG); }
	// handle pubkey algorithm
	if (TRUE == bSign) {
		if (S_OK != CoCreateInstance(__uuidof(CObjectId), NULL, CLSCTX_INPROC_SERVER, __uuidof(IObjectId), (void**)&objSignId)) { *errorCode = GetLastError(); return ""; }
		if (RSASHA1 == signAlgOID) {
			signOid = L"1.2.840.113549.1.1.5"; // rsa + sha1
		}
		else if (RSAMD5 == signAlgOID) {
			signOid = L"1.2.840.113549.1.1.4"; // rsa + md5
		}
		else {
			signOid = L"1.2.840.113549.1.1.11"; // rsa + sha256
		}
		signAlgo = SysAllocStringLen(signOid.data(), signOid.size());
		objSignId->InitializeFromValue(signAlgo);
		pKey->put_Algorithm(objSignId);
	}
	else {
		if (S_OK != CoCreateInstance(__uuidof(CObjectId), NULL, CLSCTX_INPROC_SERVER, __uuidof(IObjectId), (void**)&objEncId)) { *errorCode = GetLastError(); return ""; }
		if (RSA == pubKeyAlgOID) {
			pubOid = L"1.2.840.113549.1.1.1";
		}
		else {
			pubOid = L"1.2.840.113549.1.1.1";
		}
		pubKeyAlgo = SysAllocStringLen(pubOid.data(), pubOid.size());
		objEncId->InitializeFromValue(pubKeyAlgo);
		pKey->put_Algorithm(objEncId);
	}
	// Create the key
	if (S_OK != pKey->Create()) { *errorCode = GetLastError(); return ""; }
	// Export the public key
	if (S_OK != pKey->ExportPublicKey(&pPublicKey)) { *errorCode = GetLastError(); return ""; }
	// Create IX509CertificateRequestPkcs10
	if (S_OK != CoCreateInstance(__uuidof(CX509CertificateRequestPkcs10), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX509CertificateRequestPkcs10), (void**)&pPkcs10)) { *errorCode = GetLastError(); return ""; }
	// Allocate BSTR for template name
	strTemplateName = SysAllocString(wdn.c_str());
	if (NULL == strTemplateName) { hr = E_OUTOFMEMORY; }
	// Initialize IX509CertificateRequestPkcs10 from public key
	if (S_OK != pPkcs10->InitializeFromPublicKey(ContextUser, pPublicKey, NULL)) { *errorCode = GetLastError(); return ""; } // BUG cant set subject name
	// Create CN
	if (S_OK != CoCreateInstance(__uuidof(CX500DistinguishedName), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX500DistinguishedName), (void**)&subject)) { *errorCode = GetLastError(); return ""; }
	else {
		subject->Encode(strTemplateName, XCN_CERT_NAME_STR_NONE);
		pPkcs10->put_Subject(subject);
	}
	if (S_OK != CoCreateInstance(__uuidof(CObjectId), NULL, CLSCTX_INPROC_SERVER, __uuidof(IObjectId), (void**)&objId)) { *errorCode = GetLastError(); return ""; }
	// handle hash algorithm
	if (MD5 == digestOID) {
		dOid = L"1.2.840.113549.2.5";
		digestAlgo = SysAllocStringLen(dOid.data(), dOid.size());
	}
	else if (SHA1 == digestOID) {
		dOid = L"1.3.14.3.2.26";
		digestAlgo = SysAllocStringLen(dOid.data(), dOid.size());
	}
	else { // sha256
		dOid = L"2.16.840.1.101.3.4.2.1";
		digestAlgo = SysAllocStringLen(dOid.data(), dOid.size());
	}
	objId->InitializeFromValue(digestAlgo);
	pPkcs10->put_HashAlgorithm(objId);
	if (S_OK != pPkcs10->Encode()) { *errorCode = GetLastError(); return ""; }
	pPkcs10->get_RawDataToBeSigned((EncodingType)(XCN_CRYPT_STRING_BASE64 | XCN_CRYPT_STRING_NOCRLF), &strP10Value);
	wstring ws1(strP10Value, SysStringLen(strP10Value));
	p10 = string(ws1.begin(), ws1.end());
	bufferSize = sizeof(buffer);
	memset(buffer, 0, bufferSize);
	if (STATUS_SUCCESS != BCryptGenRandom(NULL, buffer, bufferSize, BCRYPT_USE_SYSTEM_PREFERRED_RNG)) { *errorCode = GetLastError(); return ""; }
	pPkcs10->get_KeyContainerNamePrefix(&containerNamePrefix);
	wstring ws2(containerNamePrefix, SysStringLen(containerNamePrefix));
	container = string(ws2.begin(), ws2.end());
	ByteToStrRandom(bufferSize, buffer, sz);
	container += sz;
	retValue = "[{\"errorCode\":\"" + to_string(*errorCode) + "\", \"Container\":\"" + container + "\",\"P10\":\"" + p10 + "\"}]";
	if (fCoInit) CoUninitialize();
	*errorCode = 0;
	return retValue;
}

/*
3.4.12（方法RSA）生成P10包，指定容器
3.4.12.1名称
IWSA_rsa_csp_genP10 (Container,bSign,KeySize,DN,DigestOID,PubKeyAlgOID,SignAlgOID,bExport,bProtect,SucceedFunction)
3.4.12.2参数

参数名称	类  型	参数描述
Container	string	容器名称
bSign	string	签名密钥或加密密钥标识,”true”/”false”
KeySize	string	密钥长度512，1024， 2048，4096
DN	string	主题DN，空值DN请赋值””空串
DigestOID	string	摘要算法OID，使用默认值请赋值””空串
PubKeyAlgOID	string	公钥算法OID，，使用默认值请赋值””空串
SignAlgOID	string	签名算法OID，，使用默认值请赋值””空串
bExport	string	私钥是否可导出,”true”/”false”
bProtect	string	是否启用增强密钥保护,”true”/”false”
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.12.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
P10	String	P10 结构值

3.4.12.4功能说明
在指定已有容器里，生成密钥对，再用公钥生成P10包，返回Base64编码的P10 包。
*/
string IWSAgentIS::IWSA_rsa_csp_genP10IS(DWORD*& errorCode, string& p10, const string& container, BOOL bSign, const string& keySize, const string& dn, const string& digestOID, const string& pubKeyAlgOID, const string& signAlgOID, BOOL bExport, BOOL bProtect) {
	IX509CertificateRequestPkcs10* pPkcs10 = NULL;
	IX509PrivateKey* pKey = NULL;
	IX509PublicKey* pPublicKey = NULL;
	IX500DistinguishedName* subject = NULL;
	ICspInformation* iCspInfo = NULL;
	ICspInformations* iCspInfos = NULL;
	IObjectId* objId = NULL, * objSignId = NULL, * objEncId = NULL;
	HRESULT hr = S_OK;
	bool fCoInit = false;
	BSTR strTemplateName = NULL, strP10Value = NULL, containerNamePrefix = NULL, digestAlgo = NULL, pubKeyAlgo = NULL, signAlgo = NULL, cspInfo = NULL, cspInfos = NULL;
	wstring signOid, pubOid, dOid, wdn = wstring(dn.begin(), dn.end());

	if (KEY_SIZE_512 != keySize && KEY_SIZE_1024 != keySize && KEY_SIZE_2048 != keySize && KEY_SIZE_4096 != keySize) { *errorCode = GetLastError(); return ""; }
	// CoInitializeEx
	if (S_OK != CoInitializeEx(NULL, COINIT_MULTITHREADED)) { *errorCode = GetLastError(); return ""; }
	fCoInit = true;
	// Create IX509PrivateKey
	if (S_OK != CoCreateInstance(__uuidof(CX509PrivateKey), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX509PrivateKey), (void**)&pKey)) { *errorCode = GetLastError(); return ""; }
	pKey->put_Length(atol(keySize.c_str()));
	// Ajouter les attributes de l'export et du protect
	if (TRUE == bExport) { pKey->put_ExportPolicy(XCN_NCRYPT_ALLOW_EXPORT_FLAG); }
	else { pKey->put_ExportPolicy(XCN_NCRYPT_ALLOW_EXPORT_NONE); }
	if (TRUE == bProtect) { pKey->put_KeyProtection(XCN_NCRYPT_UI_FORCE_HIGH_PROTECTION_FLAG); }
	else { pKey->put_KeyProtection(XCN_NCRYPT_UI_NO_PROTECTION_FLAG); }
	// handle pubkey algorithm
	if (TRUE == bSign) {
		if (S_OK != CoCreateInstance(__uuidof(CObjectId), NULL, CLSCTX_INPROC_SERVER, __uuidof(IObjectId), (void**)&objSignId)) { *errorCode = GetLastError(); return ""; }
		if (RSASHA1 == signAlgOID) {
			signOid = L"1.2.840.113549.1.1.5"; // rsa + sha1
		}
		else if (RSAMD5 == signAlgOID) {
			signOid = L"1.2.840.113549.1.1.4"; // rsa + md5
		}
		else {
			signOid = L"1.2.840.113549.1.1.11"; // rsa + sha256
		}
		signAlgo = SysAllocStringLen(signOid.data(), signOid.size());
		objSignId->InitializeFromValue(signAlgo);
		pKey->put_Algorithm(objSignId);
	}
	else {
		if (S_OK != CoCreateInstance(__uuidof(CObjectId), NULL, CLSCTX_INPROC_SERVER, __uuidof(IObjectId), (void**)&objEncId)) { *errorCode = GetLastError(); return ""; }
		if (RSA == pubKeyAlgOID) {
			pubOid = L"1.2.840.113549.1.1.1";
		}
		else {
			pubOid = L"1.2.840.113549.1.1.1";
		}
		pubKeyAlgo = SysAllocStringLen(pubOid.data(), pubOid.size());
		objEncId->InitializeFromValue(pubKeyAlgo);
		pKey->put_Algorithm(objEncId);
	}
	// Create the key
	if (S_OK != pKey->Create()) { *errorCode = GetLastError(); return ""; }
	// Export the public key
	if (S_OK != pKey->ExportPublicKey(&pPublicKey)) { *errorCode = GetLastError(); return ""; }
	// Create IX509CertificateRequestPkcs10
	if (S_OK != CoCreateInstance(__uuidof(CX509CertificateRequestPkcs10), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX509CertificateRequestPkcs10), (void**)&pPkcs10)) { *errorCode = GetLastError(); return ""; }
	// Allocate BSTR for template name
	strTemplateName = SysAllocString(wdn.c_str());
	if (NULL == strTemplateName) { *errorCode = E_OUTOFMEMORY; return ""; }
	// Initialize IX509CertificateRequestPkcs10 from public key
	
	// Create CN
	if (S_OK != CoCreateInstance(__uuidof(CX500DistinguishedName), NULL, CLSCTX_INPROC_SERVER, __uuidof(IX500DistinguishedName), (void**)&subject)) { *errorCode = GetLastError(); return ""; }
	else {
		subject->Encode(strTemplateName, XCN_CERT_NAME_STR_NONE);
		pPkcs10->put_Subject(subject);
	}
	if (S_OK != CoCreateInstance(__uuidof(CObjectId), NULL, CLSCTX_INPROC_SERVER, __uuidof(IObjectId), (void**)&objId)) { *errorCode = GetLastError(); return ""; }
	// handle hash algorithm
	if (MD5 == digestOID) {
		dOid = L"1.2.840.113549.2.5";
		digestAlgo = SysAllocStringLen(dOid.data(), dOid.size());
	}
	else if (SHA1 == digestOID) {
		dOid = L"1.3.14.3.2.26";
		digestAlgo = SysAllocStringLen(dOid.data(), dOid.size());
	}
	else { // sha256
		dOid = L"2.16.840.1.101.3.4.2.1";
		digestAlgo = SysAllocStringLen(dOid.data(), dOid.size());
	}
	objId->InitializeFromValue(digestAlgo);
	pPkcs10->put_HashAlgorithm(objId);
	wstring ws(container.begin(), container.end());
	containerNamePrefix = SysAllocStringLen(ws.data(), ws.size());
	pPkcs10->put_KeyContainerNamePrefix(containerNamePrefix);

	// Create IX509CertificateRequestPkcs10
	// if (S_OK != CoCreateInstance(__uuidof(CCspInformation), NULL, CLSCTX_INPROC_SERVER, __uuidof(ICspInformation), (void**)&iCspInfo)) { *errorCode = GetLastError(); return ""; }
	// if (S_OK != CoCreateInstance(__uuidof(CCspInformations), NULL, CLSCTX_INPROC_SERVER, __uuidof(ICspInformations), (void**)&iCspInfos)) { *errorCode = GetLastError(); return ""; }
	// cspInfo = SysAllocStringLen(this->csp.data(), this->csp.size());
	// CComBSTR _proividerName(MS_KEY_STORAGE_PROVIDER);
	// if (S_OK != iCspInfo->InitializeFromName(_proividerName)) { *errorCode = GetLastError(); return ""; }
	// if (S_OK != iCspInfos->Add(iCspInfo)) { *errorCode = GetLastError(); return ""; }
	// if (S_OK != pPkcs10->put_CspInformations(iCspInfos)) { *errorCode = GetLastError(); return ""; }
	//if (S_OK != pPkcs10->put_SmimeCapabilities(VARIANT_FALSE)) { *errorCode = GetLastError(); return ""; }
	if (S_OK != pPkcs10->InitializeFromPublicKey(ContextUser, pPublicKey, NULL)) { *errorCode = GetLastError(); return ""; } // BUG cant set subject name initialize after puts
	if (S_OK != pPkcs10->Encode()) { *errorCode = GetLastError(); return ""; }
	if (S_OK != pPkcs10->get_RawDataToBeSigned((EncodingType)(XCN_CRYPT_STRING_BASE64 | XCN_CRYPT_STRING_NOCRLF), &strP10Value)) { *errorCode = GetLastError(); return ""; }
	wstring ws1(strP10Value, SysStringLen(strP10Value));
	p10 = string(ws1.begin(), ws1.end());
	if (fCoInit) CoUninitialize();
	*errorCode = 0;
	return p10;
}

/*
3.4.13（方法RSA）生成加密密钥对
3.4.13.1名称
IWSA_rsa_csp_genEncKeyPair (SucceedFunction)
3.4.13.2参数
参数名称	类  型	参数描述
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.13.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
Data	string	加密密钥公钥Base64编码返回

3.4.13.4功能说明
生成加密密钥对，用以解密下载加密证书时的对称密钥；把加密密钥公钥Base64编码返回。
*/
void IWSAgentIS::IWSA_rsa_csp_genEncKeyPairIS(DWORD*& errorCode, string& data) {
	BCRYPT_KEY_HANDLE keyHandle = NULL;
	BCRYPT_ALG_HANDLE rsaAlgHandle = NULL;
	ULONG  cbResult = 0;
	PUCHAR pbOutput;
	if (STATUS_SUCCESS != BCryptOpenAlgorithmProvider(&rsaAlgHandle, BCRYPT_RSA_ALGORITHM, NULL, 0)) { *errorCode = GetLastError(); return; }
	if (STATUS_SUCCESS != BCryptGenerateKeyPair(rsaAlgHandle, &keyHandle, DEFAULT_KEY_SIZE_RSA_ULONG, 0)) { *errorCode = GetLastError(); return; }
	if (STATUS_SUCCESS != BCryptFinalizeKeyPair(keyHandle, 0)) { *errorCode = GetLastError(); return; }
	if (STATUS_SUCCESS != BCryptExportKey(keyHandle, NULL, BCRYPT_RSAPUBLIC_BLOB, NULL, 0, &cbResult, 0)) { *errorCode = GetLastError(); return; }
	pbOutput = (PBYTE)malloc(cbResult);
	if (NULL == pbOutput) { *errorCode = GetLastError(); return; } 
	if (STATUS_SUCCESS != BCryptExportKey(keyHandle, NULL, BCRYPT_RSAPUBLIC_BLOB, pbOutput, cbResult, &cbResult, 0)) { *errorCode = GetLastError(); return; }
	this->setEncKeyPair(keyHandle);
	IWSABase64EncodeIS(errorCode, data, pbOutput, cbResult);
	*errorCode = 0;
	if(pbOutput) free(pbOutput);
}

/*
3.4.14（方法RSA）删除加密密钥对
3.4.14.1名称
IWSA_rsa_csp_delEncKeyPair(SucceedFunction)
3.4.14.2参数
参数名称	类  型	参数描述
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.14.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.14.4功能说明
删除加密密钥对。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_delEncKeyPairIS() {
	return  BCryptDestroyKey(this->getEncKeyPair());
}

/*
3.4.15（方法RSA）导入用户证书的验签名证书链到存储区
3.4.15.1名称
IWSA_rsa_csp_importX509CertToStore (X509Cert,bRoot,SucceedFunction)
3.4.15.2参数
参数名称	类  型	参数描述
X509Cert	String	X509证书，Base64编码
bRoot	String	是否是根证书,”true”/”false”
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.15.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.15.4功能说明
用于导入用户证书的验签名证书链到存储区。
如果bRoot等于TRUE是根证书导入ROOT存储区，否则导入CA中级存储区。
*/

DWORD IWSAgentIS::IWSA_rsa_csp_importX509CertToStoreIS(const string& x509Cert, BOOL bRoot) {
	PBYTE pbCertEncoded = NULL;
	DWORD cbCertEncoded, errorCode;
	HCERTSTORE hStore = NULL;
	if (TRUE == bRoot) hStore = CertOpenSystemStore(NULL, ROOT);
	else hStore = CertOpenSystemStore(NULL, CA);
	IWSABase64DecodeIS(&errorCode, pbCertEncoded, cbCertEncoded, x509Cert);
	if (errorCode) return errorCode;
	if (!CertAddEncodedCertificateToStore(hStore, MY_TYPE, pbCertEncoded, cbCertEncoded, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { return GetLastError(); }
	return 0;
}

/*
3.4.16（方法RSA）删除存储区里的证书
3.4.16.1名称
IWSA_rsa_csp_delX509CertInStore (Issuer,Serial, SucceedFunction)
3.4.16.2参数
参数名称	类  型	参数描述
Issuer	string	证书颁发者，不区分大小写，不计空格
Serial	string	证书序列号，不区分大小写
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.16.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.16.4功能说明
删除存储区里的证书。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_delX509CertInStoreIS(const string& issuerDN, const string& certSN) {
	PCCERT_CONTEXT pCertContext = NULL;
	LPSTR pIssuer = NULL, pSubject = NULL, pSubjectSimple = NULL;
	string certDNSimple, issuerDN_t;
	HCERTSTORE hCertStore = NULL, hCertStoreTmp = NULL;
	DWORD cIssuer, cSubject, cSubjectSimple;
	mpz_t result;

	mpz_init(result);
	if (!(hCertStore = this->openAllStore())) { return GetLastError(); }
	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
		BOOL flag = FALSE;
		if (cIssuer = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, NULL, 0)) {
			pIssuer = (LPSTR)malloc(cIssuer);
			if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, pIssuer, cIssuer)) {
				issuerDN_t = string(pIssuer, cIssuer - 1);
				if (issuerDN == issuerDN_t) { // issuerDN_t
					flag = TRUE;
				}
			}
		}
		if (!flag && (cSubjectSimple = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_SIMPLE_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, NULL, 0))) {
			pSubjectSimple = (LPSTR)malloc(cSubjectSimple);
			if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_SIMPLE_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, pSubjectSimple, cSubjectSimple)) {
				certDNSimple = string(pSubjectSimple, cSubjectSimple - 1); // exclure '\0'
				if (issuerDN == certDNSimple) { // issuerDN_t
					flag = TRUE;
				}
			}
		}
		if (!flag && (cIssuer = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG | CERT_NAME_STR_REVERSE_FLAG, NULL, 0))) {
			pIssuer = (LPSTR)malloc(cIssuer);
			if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG | CERT_NAME_STR_REVERSE_FLAG, pIssuer, cIssuer)) {
				issuerDN_t = string(pIssuer, cIssuer - 1);
				if (issuerDN == issuerDN_t) { // issuerDN_t
					flag = TRUE;
				}
			}
		}
		if (!flag) continue;
		mpz_import(result, 1, -1, pCertContext->pCertInfo->SerialNumber.cbData, 0, 0, pCertContext->pCertInfo->SerialNumber.pbData);
		char* tmp = mpz_get_str(NULL, 16, result);
		if (atol(certSN.c_str()) != atol(tmp)) continue;
		break;
	}
	if (!CertDeleteCertificateFromStore(pCertContext)) { return GetLastError(); }
	
	if (pIssuer) free(pIssuer);
	if (pSubjectSimple) free(pSubjectSimple);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hCertStoreTmp) CertCloseStore(hCertStoreTmp, CERT_CLOSE_STORE_CHECK_FLAG);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	return 0;
}

/*
3.4.17（方法RSA）导入签名证书X509
3.4.17.1名称
IWSA_rsa_csp_importSignX509Cert (Container,X509Cert,SucceedFunction)
3.4.17.2参数
参数名称	类  型	参数描述
Container	string	容器名，支持””：使用产生P10时的容器
X509Cert	string	X509证书，Base64编码
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.17.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.17.4功能说明
把签名X509证书导入到存储区与签名密钥关联。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_importSignX509CertIS(const string& container, const string& x509Cert) {
	PBYTE pbCertEncoded = NULL;
	PCCERT_CONTEXT pCertContext = NULL;
	DWORD cbCertEncoded, errorCode;
	NCRYPT_PROV_HANDLE hProvider = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	HCERTSTORE hCertStore = NULL;
	CRYPT_KEY_PROV_INFO cryptKeyProvInfo;

	wstring wcontainer = wstring(container.begin(), container.end());
	ZeroMemory(&cryptKeyProvInfo, sizeof(cryptKeyProvInfo));
	cryptKeyProvInfo.pwszContainerName = const_cast<LPWSTR>(wcontainer.c_str());
	cryptKeyProvInfo.pwszProvName = const_cast<LPWSTR>(MS_KEY_STORAGE_PROVIDER);
	cryptKeyProvInfo.dwProvType = 0;
	cryptKeyProvInfo.dwKeySpec = 0;
	cryptKeyProvInfo.dwFlags = 0;

	IWSABase64DecodeIS(&errorCode, pbCertEncoded, cbCertEncoded, x509Cert);

	pCertContext = CertCreateCertificateContext(MY_TYPE, pbCertEncoded, cbCertEncoded);
	if (NULL == pCertContext) { return E_FAIL_CERTCONTEXT; }

	if (!CertSetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, 0, &cryptKeyProvInfo)) { return GetLastError(); }

	// Add cert to store
	hCertStore = CertOpenSystemStore(NULL, (this->defaultSignCertStore).c_str());
	if (NULL != hCertStore) { if (!CertAddCertificateContextToStore(hCertStore, pCertContext, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { return GetLastError(); } }

	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hKey) NCryptFreeObject(hKey);
	if (hProvider) NCryptFreeObject(hProvider);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	return errorCode;
}

/*
3.4.18（方法RSA）导入加密证书X509
3.4.18.1名称
IWSA_rsa_csp_importEncX509Cert (Container,X509Cert,EncPrikey,UKEK,bExport,bProtect,SucceedFunction)
3.4.18.2参数
参数名称	类  型	参数描述
Container	string	容器名，支持””：使用产生P10时的容器，未产生p10时，新建容器
X509Cert	string	X509证书，Base64编码
EncPrikey	string	加密私钥，Base64编码
UKEK	string	对称密钥包，Base64编码
bExport	string	私钥是否可导出,”true”/”false”
bProtect	string	是否启用增强密钥保护,”true”/”false” not implemented
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.18.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.18.4功能说明
把加密X509证书导入到存储区与加密密钥关联。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_importEncX509CertIS(const string& container, const string& x509Cert, const string& encPriKey, const string& ukek, BOOL bExport, BOOL bProtect) {
	// Declarer et initialiser les variables
	PUCHAR pbKeyObject = NULL;
	PBYTE pbCertEncoded = NULL, pbKeyBlob = NULL, pbBlob = NULL, pbPriKeyEncoded = NULL;
	PCCERT_CONTEXT pCertContext = NULL;
	BCRYPT_RSAKEY_BLOB *pRsaKeyBlob = NULL;
	DWORD cbCertEncoded, errorCode, cbKeyBlob, cbBlob, cbPriKeyEncoded, exportPolicy = NCRYPT_ALLOW_EXPORT_FLAG | NCRYPT_ALLOW_PLAINTEXT_EXPORT_FLAG, keyUsage = NCRYPT_ALLOW_DECRYPT_FLAG, nameBufSize;
	ULONG cbKeyObject, cbData;
	NCRYPT_PROV_HANDLE hProvider = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	BCRYPT_ALG_HANDLE hAesAlg = NULL, hUkekKey = NULL;
	HCERTSTORE hCertStore = NULL;
	CRYPT_KEY_PROV_INFO cryptKeyProvInfo;
	wstring pinProtectDefault = L"123456";

	// Examiner les entrées
	wstring wcontainer = wstring(container.begin(), container.end());

	// Initialiser 
	ZeroMemory(&cryptKeyProvInfo, sizeof(cryptKeyProvInfo));
	cryptKeyProvInfo.pwszContainerName = const_cast<LPWSTR>(wcontainer.c_str());
	cryptKeyProvInfo.pwszProvName = const_cast<LPWSTR>(this->csp.c_str());
	cryptKeyProvInfo.dwProvType = 0;
	cryptKeyProvInfo.dwKeySpec = 0;
	cryptKeyProvInfo.dwFlags = 0;

	BCryptBuffer nameBuf;
	nameBuf.BufferType = NCRYPTBUFFER_PKCS_KEY_NAME; // ULONG = 4B
	nameBuf.pvBuffer = (PVOID)wcontainer.c_str();
	if (wcontainer.size() < sizeof(ULONG)) {
		nameBufSize = 3 * sizeof(ULONG);
	}
	else if (wcontainer.size() + 1 < 2 * sizeof(ULONG)) {
		nameBufSize = 3 * (wcontainer.size() + 1);
	}
	else {
		nameBufSize = (wcontainer.size() + 1) * 2;
	}
	nameBuf.cbBuffer = nameBufSize;

	NCryptBufferDesc parList;
	parList.ulVersion = NCRYPTBUFFER_VERSION;
	parList.cBuffers = 1;
	parList.pBuffers = &nameBuf;

	// Decode x509Cert and encPriKey to bytes
	IWSABase64DecodeIS(&errorCode, pbCertEncoded, cbCertEncoded, x509Cert);
	IWSABase64DecodeIS(&errorCode, pbPriKeyEncoded, cbPriKeyEncoded, encPriKey);
	// Create cert
	pCertContext = CertCreateCertificateContext(MY_TYPE, pbCertEncoded, cbCertEncoded);
	if (NULL == pCertContext) { return E_FAIL_CERTCONTEXT; }
	// Decode from certificate to key bytes
	if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbPriKeyEncoded, cbPriKeyEncoded, 0, NULL, NULL, &cbKeyBlob)) { return GetLastError(); }
	pRsaKeyBlob = (BCRYPT_RSAKEY_BLOB*)malloc(cbKeyBlob);
	if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbPriKeyEncoded, cbPriKeyEncoded, 0, NULL, pRsaKeyBlob, &cbKeyBlob)) { return GetLastError(); }
	// Generate private key handle
	if (ERROR_SUCCESS == NCryptOpenStorageProvider(&hProvider, MS_KEY_STORAGE_PROVIDER, 0)) {
		if (ERROR_SUCCESS != NCryptImportKey(hProvider, NULL, BCRYPT_RSAPRIVATE_BLOB, &parList, &hKey, (PBYTE)pRsaKeyBlob, cbKeyBlob, NCRYPT_DO_NOT_FINALIZE_FLAG | NCRYPT_OVERWRITE_KEY_FLAG)) { return GetLastError(); }
		if (ERROR_SUCCESS != NCryptSetProperty(hKey, NCRYPT_KEY_USAGE_PROPERTY, (PBYTE)&keyUsage, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) { return GetLastError(); }
		if (TRUE == bExport) { // Par default false, only need to be set when it's allowed to be exported
			if (ERROR_SUCCESS != NCryptSetProperty(hKey, NCRYPT_EXPORT_POLICY_PROPERTY, (PBYTE)&exportPolicy, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) { return GetLastError(); }
		}
		if (ERROR_SUCCESS != NCryptFinalizeKey(hKey, 0)) { return GetLastError(); }
	}
	// Update the private key of the certificate and set the conteneur
	if (!CertSetCertificateContextProperty(pCertContext, CERT_NCRYPT_KEY_HANDLE_PROP_ID, NULL, &hKey)) { return GetLastError(); }
	if (!CertSetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, 0, &cryptKeyProvInfo)) { return GetLastError(); }
	// Add cert to store
	hCertStore = CertOpenSystemStore(NULL, defaultEncryptCertStore.c_str());
	if (NULL != hCertStore) { CertAddCertificateContextToStore(hCertStore, pCertContext, CERT_STORE_ADD_REPLACE_EXISTING, NULL); }

	IWSABase64DecodeIS(&errorCode, pbBlob, cbBlob, ukek);
	// Open an algorithm handle.
	if (STATUS_SUCCESS != BCryptOpenAlgorithmProvider(&hAesAlg, BCRYPT_AES_ALGORITHM, NULL, 0)) { return GetLastError(); }
	// Calculate the size of the buffer to hold the KeyObject.
	if (STATUS_SUCCESS != BCryptGetProperty(hAesAlg, BCRYPT_OBJECT_LENGTH, (PBYTE)&cbKeyObject, sizeof(DWORD), &cbData, 0)) { return GetLastError(); }
	// Allocate the key object on the heap.
	if (!(pbKeyObject = (PBYTE)malloc(cbKeyObject))) { return GetLastError(); }
	if (STATUS_SUCCESS != BCryptImportKey(hAesAlg, NULL, BCRYPT_OPAQUE_KEY_BLOB, &hUkekKey, pbKeyObject, cbKeyObject, pbBlob, cbBlob, 0)) { return GetLastError(); }
	this->setSymmetryKey(hUkekKey);

	if (pRsaKeyBlob) free(pRsaKeyBlob);
	if (pbKeyObject) free(pbKeyObject);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hKey) NCryptFreeObject(hKey);
	if (hProvider) NCryptFreeObject(hProvider);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	return errorCode;
}

/*
3.4.19（方法RSA）把签名证书P7包导入到存储区与签名密钥关联
3.4.19.1名称
IWSA_rsa_csp_importSignP7Cert (Container,P7Cert,SucceedFunction)
3.4.19.2参数
参数名称	类  型	参数描述
Container	String	容器名，支持””：使用产生P10时的容器
P7Cert	String	P7包，封装证书链。可以只有X509用户证书或 X509根证书加X509用户证书 或 X509根证书加多张X509中级证书加X509用户证书 注意：对于没有根证书的情况，第一张CA证书会被误添加到ROOT区域。
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)

3.4.19.3功能说明
将根证书和中级证书分别导入ROOT存储区和CA存储区；把签名X509证书导入到存储区与签名密钥关联。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_importSignP7CertIS(const string& container, const string& p7Cert) {
	PBYTE pbCertDecoded = NULL;
	PCCERT_CONTEXT pCertContext = NULL;
	CRYPT_KEY_PROV_INFO* pCryptKeyProvInfo = NULL;
	DWORD cbCertDecoded, errorCode, dwCryptKeyProvInfo, type = CERT_X500_NAME_STR;
	HCERTSTORE hRootStore = NULL, hCAStore = NULL, hSignCertStore = NULL, hProvPKCS7Store = NULL;
	CRYPT_INTEGER_BLOB pkcs7Blob;
	vector<PCCERT_CONTEXT> pCertContexts;
	TCHAR pszNameString[256], pszIssuerNameString[256];

	wstring wcontainer = wstring(container.begin(), container.end());
	hSignCertStore = CertOpenSystemStore(NULL, this->defaultSignCertStore.c_str());
	hRootStore = CertOpenSystemStore(NULL, ROOT);
	hCAStore = CertOpenSystemStore(NULL, CA);
	if (NULL == hSignCertStore || NULL == hRootStore || NULL == hCAStore) { return E_FAIL_CERTCONTEXT; }

	// Decode p7Cert to bytes
	IWSABase64DecodeIS(&errorCode, pbCertDecoded, cbCertDecoded, p7Cert);
	// Create certs
	// 1.Fabriquer un magasin de certificats PKCS7
	pkcs7Blob.cbData = cbCertDecoded;
	pkcs7Blob.pbData = pbCertDecoded;
	hProvPKCS7Store = CertOpenStore(CERT_STORE_PROV_PKCS7, MY_TYPE, NULL, 0, &pkcs7Blob);
	// 2.Ajouter les certificats dans son magasin, les certificats sont différentiés par leurs issuers.
	while (pCertContext = CertEnumCertificatesInStore(hProvPKCS7Store, pCertContext)) {
		pCertContexts.push_back(pCertContext);
	}
	for (PCCERT_CONTEXT pc : pCertContexts) {
		BOOL flag = FALSE;
		if (CertGetNameString(pc, CERT_NAME_RDN_TYPE, 0, &type, pszNameString, MAX_NAME)) {
			if (CertGetNameString(pc, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszIssuerNameString, MAX_NAME)) {
				if (string(pszNameString) == string(pszIssuerNameString)) {
					CertAddCertificateContextToStore(hRootStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL);
					continue;
				}
			}
		}
		for (PCCERT_CONTEXT pc1 : pCertContexts) {
			if (CertGetNameString(pc1, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszIssuerNameString, MAX_NAME)) {
				if (string(pszNameString) == string(pszIssuerNameString)) { // Si pc n'est pas root et si pc est l'issuer du pc1 => pc est un ca.
					CertAddCertificateContextToStore(hCAStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL);
					flag = TRUE;
					break;
				}
			}
		}
		if (!flag) {
			// pc n'est pas l'issuer du tout le pc1 => pc est un x509 final
			if (CertGetCertificateContextProperty(pc, CERT_KEY_PROV_INFO_PROP_ID, NULL, &dwCryptKeyProvInfo)) {
				pCryptKeyProvInfo = (PCRYPT_KEY_PROV_INFO)malloc(dwCryptKeyProvInfo);
				CertGetCertificateContextProperty(pc, CERT_KEY_PROV_INFO_PROP_ID, pCryptKeyProvInfo, &dwCryptKeyProvInfo);
			}
			if (NULL == pCryptKeyProvInfo) { // Pas de container en stockage All the elements have to be initialized.
				pCryptKeyProvInfo = (PCRYPT_KEY_PROV_INFO)malloc(sizeof(CRYPT_KEY_PROV_INFO));
				pCryptKeyProvInfo->pwszContainerName = const_cast<LPWSTR>(wcontainer.c_str());
				pCryptKeyProvInfo->pwszProvName = const_cast<LPWSTR>(MS_KEY_STORAGE_PROVIDER);
				pCryptKeyProvInfo->dwProvType = 0;
				pCryptKeyProvInfo->dwKeySpec = 0;
				pCryptKeyProvInfo->dwFlags = 0;
				pCryptKeyProvInfo->cProvParam = 0;
				pCryptKeyProvInfo->rgProvParam = NULL;
				if (!CertSetCertificateContextProperty(pc, CERT_KEY_PROV_INFO_PROP_ID, 0, pCryptKeyProvInfo)) { return GetLastError(); } 
			}
			if (!CertAddCertificateContextToStore(hSignCertStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { return GetLastError(); }
		}
	}

	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hSignCertStore) CertCloseStore(hSignCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hRootStore) CertCloseStore(hRootStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hCAStore) CertCloseStore(hCAStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hProvPKCS7Store) CertCloseStore(hProvPKCS7Store, CERT_CLOSE_STORE_CHECK_FLAG);
	return 0;
}

/*
3.4.20（方法RSA）把加密证书P7包导入到存储区与加密密钥关联
3.4.20.1名称
IWSA_rsa_csp_importEncP7Cert (Container,P7Cert,EncPrikey,UKEK,bExport,bProtect,SucceedFunction)
3.4.20.2参数
参数名称	类  型	参数描述
Container	string	容器名，支持””：使用产生P10时的容器，未产生p10时，新建容器
P7Cert	string	P7包，封装证书链。可以只有X509用户证书或X509根证书加X509用户证书或X509根证书加多张X509中级证书加X509用户证书
EncPrikey	string	加密私钥，Base64编码
UKEK	string	对称密钥包，Base64编码
bExport	string	私钥是否可导出,”true”/”false”
bProtect	string	是否启用增强密钥保护,”true”/”false”
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.20.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码

3.4.20.4功能说明
将根证书和中级证书分别导入ROOT存储区和CA存储区；把加密X509证书导入到存储区与加密密钥关联。
*/
DWORD IWSAgentIS::IWSA_rsa_csp_importEncP7CertIS(const string& container, const string& p7Cert, const string& encPriKey, const string& ukek, BOOL bExport, BOOL bProtect) {
	// Declarer et initialiser les variables
	PUCHAR pbKeyObject = NULL;
	PBYTE pbCertDecoded = NULL, pbKeyBlob = NULL, pbBlob = NULL, pbPriKeyEncoded = NULL;
	PCCERT_CONTEXT pCertContext = NULL;
	BCRYPT_RSAKEY_BLOB* pRsaKeyBlob = NULL;
	DWORD cbCertDecoded, errorCode, cbKeyBlob, cbBlob, cbPriKeyEncoded, exportPolicy = NCRYPT_ALLOW_EXPORT_FLAG | NCRYPT_ALLOW_PLAINTEXT_EXPORT_FLAG, keyUsage = NCRYPT_ALLOW_DECRYPT_FLAG, certNumber = 0, certNumberIndex = 0, dwCryptKeyProvInfo, nameBufSize, type = CERT_X500_NAME_STR;
	ULONG cbKeyObject, cbData;
	NCRYPT_PROV_HANDLE hProvider = NULL;
	NCRYPT_KEY_HANDLE hKey = NULL;
	BCRYPT_ALG_HANDLE hAesAlg = NULL, hUkekKey = NULL;
	HCERTSTORE hEncryptCertStore = NULL, hProvPKCS7Store = NULL, hRootStore = NULL, hCAStore = NULL;
	CRYPT_KEY_PROV_INFO cryptKeyProvInfo;
	CRYPT_INTEGER_BLOB pkcs7Blob;
	vector<PCCERT_CONTEXT> pCertContexts;
	TCHAR pszNameString[256], pszIssuerNameString[256];
	wstring pinProtectDefault = L"123456";

	// Examiner les entrées
	wstring wcontainer = wstring(container.begin(), container.end());

	// Initialiser 
	hEncryptCertStore = CertOpenSystemStore(NULL, this->defaultEncryptCertStore.c_str()); // avec clé privée decrypt store
	hRootStore = CertOpenSystemStore(NULL, ROOT);
	hCAStore = CertOpenSystemStore(NULL, CA);
	if (NULL == hEncryptCertStore || NULL == hRootStore || NULL == hCAStore) { return E_FAIL_OPENSTORE; }

	ZeroMemory(&cryptKeyProvInfo, sizeof(cryptKeyProvInfo));
	cryptKeyProvInfo.pwszContainerName = const_cast<LPWSTR>(wcontainer.c_str());
	cryptKeyProvInfo.pwszProvName = const_cast<LPWSTR>(this->csp.c_str());
	cryptKeyProvInfo.dwProvType = 0;
	cryptKeyProvInfo.dwKeySpec = 0;
	cryptKeyProvInfo.dwFlags = 0;
	cryptKeyProvInfo.cProvParam = 0;
	cryptKeyProvInfo.rgProvParam = NULL;

	BCryptBuffer nameBuf;
	nameBuf.BufferType = NCRYPTBUFFER_PKCS_KEY_NAME; // ULONG = 4B
	nameBuf.pvBuffer = (PVOID)wcontainer.c_str();
	if (wcontainer.size() < sizeof(ULONG)) {
		nameBufSize = 3 * sizeof(ULONG);
	}
	else if (wcontainer.size() + 1 < 2 * sizeof(ULONG)) {
		nameBufSize = 3 * (wcontainer.size() + 1);
	}
	else {
		nameBufSize = (wcontainer.size() + 1) * 2;
	}
	nameBuf.cbBuffer = nameBufSize;

	NCryptBufferDesc parList;
	parList.ulVersion = NCRYPTBUFFER_VERSION;
	parList.cBuffers = 1;
	parList.pBuffers = &nameBuf;

	// Decode p7Cert and encPriKey to bytes
	IWSABase64DecodeIS(&errorCode, pbCertDecoded, cbCertDecoded, p7Cert);
	IWSABase64DecodeIS(&errorCode, pbPriKeyEncoded, cbPriKeyEncoded, encPriKey);
	// Decode from certificate to key bytes
	if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbPriKeyEncoded, cbPriKeyEncoded, 0, NULL, NULL, &cbKeyBlob)) { return GetLastError(); }
	pRsaKeyBlob = (BCRYPT_RSAKEY_BLOB*)malloc(cbKeyBlob);
	if (!CryptDecodeObjectEx(MY_TYPE, CNG_RSA_PRIVATE_KEY_BLOB, pbPriKeyEncoded, cbPriKeyEncoded, 0, NULL, pRsaKeyBlob, &cbKeyBlob)) { return GetLastError(); }
	// Generate private key handle
	if (ERROR_SUCCESS == NCryptOpenStorageProvider(&hProvider, MS_KEY_STORAGE_PROVIDER, 0)) {
		if (ERROR_SUCCESS != NCryptImportKey(hProvider, NULL, BCRYPT_RSAPRIVATE_BLOB, &parList, &hKey, (PBYTE)pRsaKeyBlob, cbKeyBlob, NCRYPT_DO_NOT_FINALIZE_FLAG | NCRYPT_OVERWRITE_KEY_FLAG)) { return GetLastError(); }
		if (ERROR_SUCCESS != NCryptSetProperty(hKey, NCRYPT_KEY_USAGE_PROPERTY, (PBYTE)&keyUsage, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) { return GetLastError(); }
		if (TRUE == bExport) { // Par default false, only need to be set when it's allowed to be exported
			if (ERROR_SUCCESS != NCryptSetProperty(hKey, NCRYPT_EXPORT_POLICY_PROPERTY, (PBYTE)&exportPolicy, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) { return GetLastError(); }
		}
		if (ERROR_SUCCESS != NCryptFinalizeKey(hKey, 0)) { return GetLastError(); }
	}
	// Create certs
	// 1.Fabriquer un magasin de certificats PKCS7
	pkcs7Blob.cbData = cbCertDecoded;
	pkcs7Blob.pbData = pbCertDecoded;
	hProvPKCS7Store = CertOpenStore(CERT_STORE_PROV_PKCS7, MY_TYPE, NULL, 0, &pkcs7Blob);
	if (NULL == hProvPKCS7Store) { return E_FAIL_OPENSTORE; }
	// 2.Ajouter les certificats dans son magasin, les certificats sont différentiés par leurs issuers.
	while (pCertContext = CertEnumCertificatesInStore(hProvPKCS7Store, pCertContext)) {
		pCertContexts.push_back(pCertContext);
	}
	for (PCCERT_CONTEXT pc : pCertContexts) {
		BOOL flag = FALSE;
		if (CertGetNameString(pc, CERT_NAME_RDN_TYPE, 0, &type, pszNameString, MAX_NAME)) {
			if (CertGetNameString(pc, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszIssuerNameString, MAX_NAME)) {
				if (string(pszNameString) == string(pszIssuerNameString)) {
					CertAddCertificateContextToStore(hRootStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL);
					continue;
				}
			}
		}
		for (PCCERT_CONTEXT pc1 : pCertContexts) {
			if (CertGetNameString(pc1, CERT_NAME_RDN_TYPE, CERT_NAME_ISSUER_FLAG, &type, pszIssuerNameString, MAX_NAME)) {
				if (string(pszNameString) == string(pszIssuerNameString)) { // Si pc n'est pas root et si pc est l'issuer du pc1 => pc est un ca.
					CertAddCertificateContextToStore(hCAStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL);
					flag = TRUE;
					break;
				}
			}
		}
		if (!flag) {
			// pc n'est pas l'issuer du tout le pc1 => pc est un x509 final
			if (!CertSetCertificateContextProperty(pc, CERT_KEY_PROV_INFO_PROP_ID, 0, &cryptKeyProvInfo)) { return GetLastError(); }
			if (!CertSetCertificateContextProperty(pc, CERT_NCRYPT_KEY_HANDLE_PROP_ID, NULL, &hKey)) { return GetLastError(); }
			if (!CertAddCertificateContextToStore(hEncryptCertStore, pc, CERT_STORE_ADD_REPLACE_EXISTING, NULL)) { return GetLastError(); }
		}
	}

	IWSABase64DecodeIS(&errorCode, pbBlob, cbBlob, ukek);
	// Open an algorithm handle.
	if (STATUS_SUCCESS != BCryptOpenAlgorithmProvider(&hAesAlg, BCRYPT_AES_ALGORITHM, NULL, 0)) { return GetLastError(); }
	// Calculate the size of the buffer to hold the KeyObject.
	if (STATUS_SUCCESS != BCryptGetProperty(hAesAlg, BCRYPT_OBJECT_LENGTH, (PBYTE)&cbKeyObject, sizeof(DWORD), &cbData, 0)) { return GetLastError(); }
	// Allocate the key object on the heap.
	pbKeyObject = (PBYTE)malloc(cbKeyObject);
	if (NULL == pbKeyObject) { return GetLastError(); }
	if (STATUS_SUCCESS != BCryptImportKey(hAesAlg, NULL, BCRYPT_OPAQUE_KEY_BLOB, &hUkekKey, pbKeyObject, cbKeyObject, pbBlob, cbBlob, 0)) { return GetLastError(); }
	this->setSymmetryKey(hUkekKey);

	if (pRsaKeyBlob) free(pRsaKeyBlob);
	if (pbKeyObject) free(pbKeyObject);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hKey) NCryptFreeObject(hKey);
	if (hProvider) NCryptFreeObject(hProvider);
	if (hEncryptCertStore) CertCloseStore(hEncryptCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hRootStore) CertCloseStore(hRootStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hCAStore) CertCloseStore(hCAStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hProvPKCS7Store) CertCloseStore(hProvPKCS7Store, CERT_CLOSE_STORE_CHECK_FLAG);
	return errorCode;
}

/*
3.4.21（方法RSA）导出指定容器中的签名证书
3.4.21.1名称
IWSA_rsa_csp_exportSignX509Cert (Container,SucceedFunction)
3.4.21.2参数
参数名称	类  型	参数描述
Container	string	容器名称
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.21.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
Data	string	签名证书，Base64编码

3.4.21.4功能说明
导出指定容器中的签名证书，Base64编码返回。
*/
void IWSAgentIS::IWSA_rsa_csp_exportSignX509CertIS(DWORD*& errorCode, string& data, const string& container) {
	HCERTSTORE hCertStore = NULL;
	PCCERT_CONTEXT pCertContext = NULL, pPreCertContext = NULL;
	PCRYPT_KEY_PROV_INFO pCryptKeyProvInfo = NULL;
	DWORD cbData;

	hCertStore = CertOpenSystemStore(NULL, this->defaultSignCertStore.c_str());
	if (NULL == hCertStore) { *errorCode = E_FAIL_OPENSTORE; return; }

	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pPreCertContext)) {
		CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, NULL, &cbData);
		pCryptKeyProvInfo = (PCRYPT_KEY_PROV_INFO)malloc(cbData);
		CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, pCryptKeyProvInfo, &cbData);
		wstring ws = wstring(container.begin(), container.end());
		if (ws == wstring(pCryptKeyProvInfo->pwszContainerName)) {
			break;
		}
		pPreCertContext = pCertContext;
	}
	// Retourner le certificat sous forme de Base64
	if (NULL != pCertContext) IWSABase64EncodeIS(errorCode, data, pCertContext->pbCertEncoded, pCertContext->cbCertEncoded);
	else { *errorCode = E_FAIL_CERTCONTEXT; return; }
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (pPreCertContext) CertFreeCertificateContext(pCertContext);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	*errorCode = 0;
}

void IWSAgentIS::IWSA_rsa_csp_exportEncX509CertIS(DWORD*& errorCode, string& data, const string& container) {
	PCCERT_CONTEXT pCertContext = NULL;
	PCRYPT_KEY_PROV_INFO pCryptKeyProvInfo = NULL;
	DWORD cbData = 0;
	HCERTSTORE hCertStore = NULL;
	hCertStore = this->openAllStore(); // container
	if (NULL == hCertStore) { *errorCode = E_FAIL_OPENSTORE; return; }
	// Chercher le conteneur dans tout le certificat
	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
		if (CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, NULL, &cbData)) {
			pCryptKeyProvInfo = (PCRYPT_KEY_PROV_INFO)malloc(cbData);
			CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, pCryptKeyProvInfo, &cbData);
			wstring ws = wstring(container.begin(), container.end());
			if (ws == wstring(pCryptKeyProvInfo->pwszContainerName)) {
				break;
			}
		}
	}
	// Retourner le certificat sous forme de Base64
	if (NULL != pCertContext) IWSABase64EncodeIS(errorCode, data, pCertContext->pbCertEncoded, pCertContext->cbCertEncoded);
	else { *errorCode = E_FAIL_CERTCONTEXT; return; }
	if (pCryptKeyProvInfo) free(pCryptKeyProvInfo);
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	*errorCode = 0;
}

/*
3.4.23（方法RSA）导出证书容器的pfx文件数据
3.4.23.1名称
IWSA_rsa_csp_exportPfxCert (Issuer,Serial,Password,bSaveAs,SucceedFunction)
3.4.23.2参数
参数名称	类  型	参数描述
Issuer	string	证书颁发者，不区分大小写，不计空格
Serial	string	证书序列号，不区分大小写
Password	string	设置pfx文件密码
bSaveAs	string	是否另存本地文件,”true”/”false”
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.23.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
Data	string	Base64编码

3.4.23.4功能说明
根据证书颁发者和证书序列号查找匹配的存储区证书，导出证书容器的pfx文件数据并且Base64编码返回。
*/
void IWSAgentIS::IWSA_rsa_csp_exportPfxCertIS(DWORD*& errorCode, string& data, const string& issuerDN, const string& certSN, const string& password, BOOL bSaveAs) {
	PCCERT_CONTEXT pCertContext = NULL, pPreCertContext = NULL, pCertContextPfx = NULL;
	LPSTR pIssuer = NULL, pSubject = NULL, pSubjectSimple = NULL;
	CRYPT_KEY_PROV_INFO cryptKeyProvInfo;
	string certDNSimple, issuerDN_x500;
	HCERTSTORE hCertStore, hCertStorePfx;
	DWORD cIssuer, cSubject, cSubjectSimple, cbData, dwKeySpec = CERT_NCRYPT_KEY_SPEC;
	mpz_t result;
	CRYPT_DATA_BLOB pfxBlob;
	NCRYPT_KEY_HANDLE hKey;
	TCHAR pszNameString[256];
	ofstream ofile(this->pfxFile);

	mpz_init(result);
	wstring wpassword = wstring(password.begin(), password.end());
	hCertStore = this->openAllStore();
	if (NULL == hCertStore) { *errorCode = E_FAIL_OPENSTORE; return; }
	while (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext)) {
		BOOL flag = FALSE;
		if (cIssuer = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, NULL, 0)) {
			pIssuer = (LPSTR)malloc(cIssuer);
			if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, pIssuer, cIssuer)) {
				issuerDN_x500 = string(pIssuer, cIssuer - 1);
				if (issuerDN == issuerDN_x500) {
					flag = TRUE;
				}
			}
		}
		if (!flag && (cIssuer = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG | CERT_NAME_STR_REVERSE_FLAG, NULL, 0))) {
			pIssuer = (LPSTR)malloc(cIssuer);
			if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_X500_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG | CERT_NAME_STR_REVERSE_FLAG, pIssuer, cIssuer)) {
				issuerDN_x500 = string(pIssuer, cIssuer - 1);
				if (issuerDN == issuerDN_x500) {
					flag = TRUE;
				}
			}
		}
		if (!flag && (cSubjectSimple = CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_SIMPLE_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, NULL, 0))) {
			pSubjectSimple = (LPSTR)malloc(cSubjectSimple);
			if (CertNameToStr(MY_TYPE, &pCertContext->pCertInfo->Issuer, CERT_SIMPLE_NAME_STR | CERT_NAME_STR_NO_QUOTING_FLAG, pSubjectSimple, cSubjectSimple)) {
				certDNSimple = string(pSubjectSimple, cSubjectSimple - 1); // exclure '\0'
				if (issuerDN == certDNSimple) {
					flag = TRUE;
				}
			}
		}
		if (!flag && (CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, pszNameString, MAX_NAME))) {
			if (issuerDN == string(pszNameString)) {
				flag = TRUE;
			}
		}
		if (!flag && (CertGetNameString(pCertContext, CERT_NAME_FRIENDLY_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, pszNameString, MAX_NAME))) {
			if (issuerDN == string(pszNameString)) {
				flag = TRUE;
			}
		}
		if (!flag) continue;
		mpz_import(result, 1, -1, pCertContext->pCertInfo->SerialNumber.cbData, 0, 0, pCertContext->pCertInfo->SerialNumber.pbData);
		char* tmp = mpz_get_str(NULL, 16, result);
		if (atol(certSN.c_str()) != atol(tmp)) continue;
		break;
	}
	if (NULL == pCertContext) { *errorCode = E_FAIL_CERTCONTEXT; return; }
	if (!CryptAcquireCertificatePrivateKey(pCertContext, CRYPT_ACQUIRE_ONLY_NCRYPT_KEY_FLAG, NULL, &hKey, &dwKeySpec, NULL)) { *errorCode = GetLastError(); return;  }
	hCertStorePfx = CertOpenStore(CERT_STORE_PROV_MEMORY, 0, NULL, CERT_STORE_CREATE_NEW_FLAG, NULL);
	if (NULL == hCertStorePfx) { *errorCode = E_FAIL_CERTCONTEXT; return; }
	pCertContextPfx = CertCreateCertificateContext(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, pCertContext->pbCertEncoded, pCertContext->cbCertEncoded);
	if (NULL == pCertContextPfx) { *errorCode = E_FAIL_CERTCONTEXT; return; }
	if (!CertSetCertificateContextProperty(pCertContextPfx, CERT_KEY_PROV_HANDLE_PROP_ID, 0, &hKey)) { *errorCode = GetLastError(); return; }
	if (!CertAddCertificateContextToStore(hCertStorePfx, pCertContextPfx, CERT_STORE_ADD_ALWAYS, NULL)) { *errorCode = GetLastError(); return; }
	pfxBlob.pbData = NULL;
	if (!PFXExportCertStoreEx(hCertStorePfx, &pfxBlob, wpassword.c_str(), NULL, EXPORT_PRIVATE_KEYS)) { *errorCode = GetLastError(); return; }
	pfxBlob.pbData = (PBYTE)malloc(pfxBlob.cbData);
	if (!PFXExportCertStoreEx(hCertStorePfx, &pfxBlob, wpassword.c_str(), NULL, EXPORT_PRIVATE_KEYS)) { *errorCode = GetLastError(); return; }
	IWSABase64EncodeIS(errorCode, data, pfxBlob.pbData, pfxBlob.cbData);

	if (bSaveAs) {
		ofile << data;
	}
	if (pSubjectSimple) free(pSubjectSimple);
	if (pIssuer) free(pIssuer);
	if (pfxBlob.pbData) free(pfxBlob.pbData);
	SecureZeroMemory((PVOID)wpassword.c_str(), wpassword.size());
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (pCertContextPfx) CertFreeCertificateContext(pCertContextPfx);
	if (pPreCertContext) CertFreeCertificateContext(pPreCertContext);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hCertStorePfx) CertCloseStore(hCertStorePfx, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hKey) NCryptFreeObject(hKey);
}

/*
3.4.24（方法RSA）导出指定容器里的签名或加密密钥，生成pfx文件数据
3.4.24.1名称
IWSA_rsa_csp_exportContainerPfxCert (Container,bSignCert,Password,bSaveAs ,SucceedFunction)
3.4.24.2参数
参数名称	类  型	参数描述
Container	string	容器名称
bSignCert	string	“true”:签名密钥，”false”:加密密钥
Password	string	设置pfx文件密码
bSaveAs	string	是否另存本地文件,”true”/”false”
SucceedFunction	回调函数名称	函数成功后的回调函数(同步方式忽略此参数)
3.4.24.3回调函数参数
参数名称	类  型	返回值说明
errorCode	整数	errorCode 错误码
Data	string	Base64编码

3.4.24.4功能说明
导出指定容器里的签名或加密密钥，生成pfx文件数据并且Base64编码返回。
*/
void IWSAgentIS::IWSA_rsa_csp_exportContainerPfxCertIS(DWORD*& errorCode, string& data, const string& container, BOOL bSignCert, const string& password, BOOL bSaveAs) {
	PCCERT_CONTEXT pCertContext = NULL, pPreCertContext = NULL, pCertContextPfx = NULL;
	PCRYPT_KEY_PROV_INFO pCryptKeyProvInfo;
	HCERTSTORE hCertStore = NULL, hCertStorePfx = NULL;
	DWORD  cbData, dwKeySpec = CERT_NCRYPT_KEY_SPEC;
	CRYPT_DATA_BLOB pfxBlob;
	NCRYPT_KEY_HANDLE hKey = NULL;
	NCRYPT_PROV_HANDLE hProvider = NULL;
	ofstream ofile(this->pfxFile);
	wstring wpassword = wstring(password.begin(), password.end());
	wstring wcontainer = wstring(container.begin(), container.end());

	if (ERROR_SUCCESS != NCryptOpenStorageProvider(&hProvider, NULL, 0)) { *errorCode = GetLastError(); return; }
	if (ERROR_SUCCESS != NCryptOpenKey(hProvider, &hKey, wcontainer.c_str(), 0, 0)) { *errorCode = GetLastError(); return; }

	if (bSignCert) hCertStore = CertOpenSystemStore(NULL, this->defaultSignCertStore.c_str());
	else hCertStore = CertOpenSystemStore(NULL, this->defaultEncryptCertStore.c_str());

	while (pCertContext = CertFindCertificateInStore(hCertStore, MY_TYPE, NULL, CERT_FIND_HAS_PRIVATE_KEY, NULL, pPreCertContext)) {
		CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, NULL, &cbData);
		pCryptKeyProvInfo = (PCRYPT_KEY_PROV_INFO)malloc(cbData);
		CertGetCertificateContextProperty(pCertContext, CERT_KEY_PROV_INFO_PROP_ID, pCryptKeyProvInfo, &cbData);
		wstring ws = wstring(container.begin(), container.end());
		if (wstring(pCryptKeyProvInfo->pwszContainerName) == ws) {
			break;
		}
		pPreCertContext = pCertContext;
	}
	if (NULL == pCertContext) { *errorCode = GetLastError(); return; }
	if (!CryptAcquireCertificatePrivateKey(pCertContext, CRYPT_ACQUIRE_ONLY_NCRYPT_KEY_FLAG, NULL, &hKey, &dwKeySpec, NULL)) { *errorCode = GetLastError(); return; }
	if (!(hCertStorePfx = CertOpenStore(CERT_STORE_PROV_MEMORY, 0, NULL, CERT_STORE_CREATE_NEW_FLAG, NULL))) { *errorCode = GetLastError(); return; }
	if (!(pCertContextPfx = CertCreateCertificateContext(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, pCertContext->pbCertEncoded, pCertContext->cbCertEncoded))) { *errorCode = GetLastError(); return; }
	if (!CertSetCertificateContextProperty(pCertContextPfx, CERT_KEY_PROV_HANDLE_PROP_ID, 0, &hKey)) { *errorCode = GetLastError(); return; }
	if (!CertAddCertificateContextToStore(hCertStorePfx, pCertContextPfx, CERT_STORE_ADD_ALWAYS, NULL)) { *errorCode = GetLastError(); return; }
	pfxBlob.pbData = NULL;
	if (!PFXExportCertStoreEx(hCertStorePfx, &pfxBlob, wpassword.c_str(), NULL, EXPORT_PRIVATE_KEYS)) { *errorCode = GetLastError(); return; }
	pfxBlob.pbData = (PBYTE)malloc(pfxBlob.cbData);
	if (!PFXExportCertStoreEx(hCertStorePfx, &pfxBlob, wpassword.c_str(), NULL, EXPORT_PRIVATE_KEYS)) { *errorCode = GetLastError(); return; }
	IWSABase64EncodeIS(errorCode, data, pfxBlob.pbData, pfxBlob.cbData);

	if (bSaveAs) {
		ofile << data;
	}

	if (pfxBlob.pbData) free(pfxBlob.pbData);
	SecureZeroMemory((PVOID)wpassword.c_str(), wpassword.size());
	if (pCertContext) CertFreeCertificateContext(pCertContext);
	if (pCertContextPfx) CertFreeCertificateContext(pCertContextPfx);
	if (pPreCertContext) CertFreeCertificateContext(pPreCertContext);
	if (hCertStore) CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hCertStorePfx) CertCloseStore(hCertStorePfx, CERT_CLOSE_STORE_CHECK_FLAG);
	if (hKey) NCryptFreeObject(hKey);
}


string IWSAgentIS::IWSA_rsa_csp_GenUKEK() {
	BCRYPT_ALG_HANDLE h3desAlg = NULL; // bug aes
	BCRYPT_KEY_HANDLE hKey = NULL;
	NTSTATUS status = STATUS_UNSUCCESSFUL;
	DWORD cbCipherText = 0, cbPlainText = 0, cbData = 0, cbKeyObject = 0, cbBlockLen = 0, cbBlob = 0;
	PBYTE pbCipherText = NULL, pbPlainText = NULL, pbKeyObject = NULL, pbIV = NULL, pbBlob = NULL;

	const BYTE rgbPlaintext[] =
	{
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
		0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F
	};

	static const BYTE rgbIV[] =
	{
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
		0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F
	};

	static const BYTE rgbAES128Key[] =
	{
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
		0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F
	};

	// Open an algorithm handle.
	if (!NT_SUCCESS(status = BCryptOpenAlgorithmProvider(&h3desAlg, BCRYPT_AES_ALGORITHM, NULL, 0))) { wprintf(L"**** Error 0x%x returned by BCryptOpenAlgorithmProvider\n", status); }
	// Calculate the size of the buffer to hold the KeyObject.
	if (!NT_SUCCESS(status = BCryptGetProperty(h3desAlg, BCRYPT_OBJECT_LENGTH, (PBYTE)&cbKeyObject, sizeof(DWORD), &cbData, 0))) { wprintf(L"**** Error 0x%x returned by BCryptGetProperty\n", status); }
	// Allocate the key object on the heap.
	pbKeyObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbKeyObject);
	if (NULL == pbKeyObject) { wprintf(L"**** memory allocation failed\n"); }
	// Calculate the block length for the IV.
	if (!NT_SUCCESS(status = BCryptGetProperty(h3desAlg, BCRYPT_BLOCK_LENGTH, (PBYTE)&cbBlockLen, sizeof(DWORD), &cbData, 0))) { wprintf(L"**** Error 0x%x returned by BCryptGetProperty\n", status); }
	// Determine whether the cbBlockLen is not longer than the IV length.
	if (cbBlockLen > sizeof(rgbIV)) { wprintf(L"**** block length is longer than the provided IV length\n"); }
	// Allocate a buffer for the IV. The buffer is consumed during the encrypt/decrypt process.
	pbIV = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbBlockLen);
	if (NULL == pbIV) { wprintf(L"**** memory allocation failed\n"); }
	memcpy(pbIV, rgbIV, cbBlockLen);
	if (!NT_SUCCESS(status = BCryptSetProperty(h3desAlg, BCRYPT_CHAINING_MODE, (PBYTE)BCRYPT_CHAIN_MODE_CBC, sizeof(BCRYPT_CHAIN_MODE_CBC), 0))) { wprintf(L"**** Error 0x%x returned by BCryptSetProperty\n", status); }
	// Generate the key from supplied input key bytes.
	if (!NT_SUCCESS(status = BCryptGenerateSymmetricKey(h3desAlg, &hKey, pbKeyObject, cbKeyObject, (PBYTE)rgbAES128Key, sizeof(rgbAES128Key), 0))) { wprintf(L"**** Error 0x%x returned by BCryptGenerateSymmetricKey\n", status); }
	// Save another copy of the key for later.
	if (!NT_SUCCESS(status = BCryptExportKey(hKey, NULL, BCRYPT_OPAQUE_KEY_BLOB, NULL, 0, &cbBlob, 0))) { wprintf(L"**** Error 0x%x returned by BCryptExportKey\n", status); }
	// Allocate the buffer to hold the BLOB.
	pbBlob = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbBlob);
	if (NULL == pbBlob) { wprintf(L"**** memory allocation failed\n"); }
	if (!NT_SUCCESS(status = BCryptExportKey(hKey, NULL, BCRYPT_OPAQUE_KEY_BLOB, pbBlob, cbBlob, &cbBlob, 0))) { wprintf(L"**** Error 0x%x returned by BCryptExportKey\n", status); }
	DWORD errorCode;
	string textData;
	IWSABase64EncodeIS(&errorCode, textData, pbBlob, cbBlob);
	return textData;
}

string IWSAgentIS::readCrttoBase64(const string& file) {
	ifstream iFile(file);
	string iContent((std::istreambuf_iterator<char>(iFile)), std::istreambuf_iterator<char>());
	DWORD dwBufferLen, cbKeyBlob, errorCode;
	PBYTE pbBuffer, pbKeyBlob;
	PCERT_INFO pCertInfo;
	string ret;

	if (!CryptStringToBinary(iContent.c_str(), iContent.size(), CRYPT_STRING_BASE64HEADER, NULL, &dwBufferLen, NULL, NULL)) { printf("Failed to convert BASE64 crt. Error 0x%.8X\n", GetLastError()); MyCertHandleError("[x] Error."); }
	pbBuffer = (PBYTE)LocalAlloc(0, dwBufferLen);
	if (!CryptStringToBinary(iContent.c_str(), iContent.size(), CRYPT_STRING_BASE64HEADER, pbBuffer, &dwBufferLen, NULL, NULL)) { printf("Failed to convert BASE64 crt. Error 0x%.8X\n", GetLastError()); MyCertHandleError("[x] Error."); }
	IWSABase64EncodeIS(&errorCode, ret, pbBuffer, dwBufferLen);
	return ret;
}

string IWSAgentIS::readPriKeytoBase64(const string& file) {
	ifstream iFile(file);
	string iContent((std::istreambuf_iterator<char>(iFile)), std::istreambuf_iterator<char>());
	DWORD dwBufferLen, cbKeyBlob, errorCode;
	PBYTE pbBuffer, pbKeyBlob;
	PCERT_INFO pCertInfo;
	string ret;

	if (!CryptStringToBinary(iContent.c_str(), iContent.size(), CRYPT_STRING_BASE64HEADER, NULL, &dwBufferLen, NULL, NULL)) { printf("Failed to convert BASE64 crt. Error 0x%.8X\n", GetLastError()); MyCertHandleError("[x] Error."); }
	pbBuffer = (PBYTE)LocalAlloc(0, dwBufferLen);
	if (!CryptStringToBinary(iContent.c_str(), iContent.size(), CRYPT_STRING_BASE64HEADER, pbBuffer, &dwBufferLen, NULL, NULL)) { printf("Failed to convert BASE64 crt. Error 0x%.8X\n", GetLastError()); MyCertHandleError("[x] Error."); }
	IWSABase64EncodeIS(&errorCode, ret, pbBuffer, dwBufferLen);
	return ret;
}

/*
Rassembler tous stores dans un handle et le retourner.
*/
HCERTSTORE& IWSAgentIS::openAllStore() {
	HCERTSTORE hCertStore = NULL, hCertStoreTmp = NULL;
	if (!(hCertStore = CertOpenStore(CERT_STORE_PROV_COLLECTION, MY_TYPE, NULL, 0, NULL))) { return hCertStore; }
	for (DWORD d : systemStoreLocation) {
		for (string s : storeNames) {
			wstring ws = wstring(s.begin(),s.end());
			hCertStoreTmp = CertOpenStore(CERT_STORE_PROV_SYSTEM, MY_TYPE, NULL, d, ws.c_str());
			CertAddStoreToCollection(hCertStore, hCertStoreTmp, CERT_PHYSICAL_STORE_ADD_ENABLE_FLAG, 0);
		}
	}
	if (hCertStoreTmp) CertCloseStore(hCertStoreTmp, CERT_CLOSE_STORE_CHECK_FLAG);
	return hCertStore;
}

/*
Rassembler MY and ADDRESSBOOK dans un handle et le retourner.
*/
HCERTSTORE& IWSAgentIS::openMyAndAddressbookStore() {
	HCERTSTORE hCertStore = NULL, hCertStoreTmp = NULL;
	vector<string> storeMyAndAddressbook;
	storeMyAndAddressbook.push_back("MY");
	storeMyAndAddressbook.push_back("ADDRESSBOOK");
	if (hCertStore = CertOpenStore(CERT_STORE_PROV_COLLECTION, MY_TYPE, NULL, 0, NULL)) { fprintf(stderr, "The collection store has been opened. \n"); }
	else { MyCertHandleError("The store was not opened."); }
	for (string s : storeMyAndAddressbook) {
		hCertStoreTmp = CertOpenSystemStore(NULL, s.c_str());
		CertAddStoreToCollection(hCertStore, hCertStoreTmp, CERT_PHYSICAL_STORE_ADD_ENABLE_FLAG, 0);
	}
	if (hCertStoreTmp) CertCloseStore(hCertStoreTmp, CERT_CLOSE_STORE_CHECK_FLAG);
	return hCertStore;
}