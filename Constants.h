#pragma once
#define CERT_STORE_SM2 "CertStoreSM2"
#define CERT_STORE_RSA "CertStoreRSA"
#define KEY_SPEC "Keyspec"
#define EMPTY_PARAM ""
#define EMPTY_JS_ARRAY "[]"
#define BANNER "=================="
#define VERSION "NetSignCNG  1.0.16.1 Build 20161010"
#define VERSION1 "NetSignCNG  1.0.16.1.0.1 Revised 20201001"
#define VERSION0 "NetSignCNG  1.0.16.1.0.1 Revised 20201019"

#define SHA1 "SHA1"
#define MD5 "MD5"
#define SHA256 "SHA256"

#define PORTGRADE1 "PortGrade1"
#define PORTGRADE2 "PortGrade2"

#define MY "MY"
#define ROOT "ROOT"
#define CA "CA"
#define ADDRESSBOOK "ADDRESSBOOK"

#define Root "Root"
#define Trust "Trust"
#define UserDS "UserDS"

// unuserd
#define ServiceNameMY 	"ServiceName\MY"
#define ServiceNameRoot 	"ServiceName\Root"
#define ServiceNameTrust 	"ServiceName\Trust"
#define ServiceNameCA 	"ServiceName\CA"

#define useridMY "userid\MY"
#define useridRoot "userid\Root"
#define useridTrust "userid\Trust"
#define useridCA "userid\CA"


#define DEFAULTSTORE "MY"
#define CUSTOMALLSTORE "CUSTOMEALLSTORE"

#define SignAndEncrypt "SignAndEncrypt"
#define Encrypt "Encrypt"
#define Sign "Sign"


#define RC2	szOID_RSA_RC2CBC    
#define RC4 szOID_RSA_RC4       
#define DES szOID_OIWSEC_desCBC
#define TDES szOID_RSA_DES_EDE3_CBC 

#define CUSTOM_KEY_NAME L"DecryptKey"

#define DEFAULT_PFX_FILE "default_pfx.pfx"
#define PRIVATE_KEY_RSA_1 "privateKey.pem"
#define PRIVATE_KEY_RSA_2 "sign.pem"
#define PRIVATE_KEY_RSA_3 "encrypt.pem"
#define PRIVATE_KEY_RSA_4 "cencrypt.pem"
#define PRIVATE_KEY_RSA_DEFAULT "encrypt.pem"

#define ERROR_CODE_SUCCESS 0

#define KEY_SIZE_512 "512"
#define KEY_SIZE_1024 "1024"
#define KEY_SIZE_2048 "2048"
#define KEY_SIZE_4096 "4096"
#define DEFAULT_KEY_SIZE_RSA KEY_SIZE_2048
#define DEFAULT_KEY_SIZE_RSA_ULONG 2048


#define MS_KEY_STORAGE_PROVIDER_STR "Microsoft Software Key Storage Provider"
#define MS_SMART_CARD_KEY_STORAGE_PROVIDER_STR "Microsoft Smart Card Key Storage Provider"
#define MS_PLATFORM_CRYPTO_PROVIDER_STR "Microsoft Platform Crypto Provider"
#define MS_PRIMITIVE_PROVIDER_STR "Microsoft Primitive Provider"
#define AMD_PROVIDER "AMD PROVIDER"

#define RSA "RSA"
#define RSAMD5 "RSAMD5"
#define RSASHA1 "RSASHA1"
#define RSASHA256 "RSASHA256"

#define CERT_TYPE_X509 "X509"
#define CERT_TYPE_P7 "P7"

#define E_INVALID_KEYSIZE -1
#define E_FAIL_OPENSTORE -2
#define E_FAIL_CERTCONTEXT -3