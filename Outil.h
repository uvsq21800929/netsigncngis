#pragma once
#include<iostream>
#include<string>
#include<vector>
using namespace std;
void tokenize(const string& str, const char delim, vector<string>& out);
void ByteToStr(const DWORD cb, const void* pv, string& sz);
void ByteToStrRandom(const DWORD cb, const void* pv, string& sz);
void vectorToStr(string& str, const vector<string> vec);
unsigned char FromHex(unsigned char x);
string UrlDecode(const string& str);
