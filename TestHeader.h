#pragma once
void testIWSAGetAllCertsListInfoISNULL();
//void testIWSAGetAllCertsListInfoDetailISMY0();
void testIWSAGetAllCertsListInfoISMY0();
void testIWSAGetAllCertsListInfoISMY2();
void testIWSAGetAllCertsListInfoISCA0();
void testIWSAGetAllCertsListInfoISCA2();
void testIWSAGetAllCertsListInfoISROOT0();
void testIWSAGetAllCertsListInfoISROOT2();
void testIWSAGetAllCertsListInfoISTRUST0();
void testIWSAGetAllCertsListInfoISTRUST2();
void testIWSAGetAllCertsListInfoISADDRESSBOOK0();
void testIWSAGetAllCertsListInfoISADDRESSBOOK2();

void testSignAndEncryptIS();
void testEncryptIS();

void testBase64EncodeIS();
void testBase64DecodeIS();

void testSignIS();
void testSignByIndexIS();

void testDetachedSignIS();
void testDetachedSignDefaultDNIS();
void testDetachedVerifyIS();

void testAttachedSignIS();
void testAttachedSignDefaultDNIS();
void testAttachedVerifyIS();

void testRawSignIS();
void testRawSignFilterCNGIS();
void testRawSignDefaultDNCNGIS();
void testRawVerifyIS();
void testRawVerifyDefaultDNIS();

void testEncryptedEnvelopIS();
void testEncryptedEnvelopDESIVIS();
void testEncryptedEnvelopDefaultDNIS();
void testDecryptEnvelopIS();
void testEncryptedSignEnvelopIS(); 
void testEncryptedSignEnvelopDefaultDNIS();
void testDecryptSignEnvelopIS();

void testAddFormIS();
void testAddFormsIS();
void testAddFileIS();
void testAddFilesIS();
void testClearFormListIS();
void testClearFileListIS();
void testGetAlreadyFormListIS();
void testGetAlreadyFileListIS();