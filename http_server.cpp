﻿#include "http_server.h" // premier http_server.h
#include "IWSAgentIS.h" // apres http_server.h
#include "urilite.h"
#include <utility>
#include <iostream>
#include <sstream>
#include <string>
#include <map>


using namespace urilite;


IWSAgentIS HttpServer::agent = IWSAgentIS();

void HttpServer::Init(const std::string& port)
{
	m_port = port;
	s_server_option.enable_directory_listing = "yes";
	s_server_option.document_root = s_web_dir.c_str();

	// ÆäËûhttpÉèÖÃ

	// ¿ªÆô CORS£¬±¾ÏîÖ»Õë¶ÔÖ÷Ò³¼ÓÔØÓÐÐ§
	// s_server_option.extra_headers = "Access-Control-Allow-Origin: *";
}

bool HttpServer::Start()
{
	mg_mgr_init(&m_mgr, NULL);
	mg_connection* connection = mg_bind(&m_mgr, m_port.c_str(), HttpServer::OnHttpWebsocketEvent);
	if (connection == NULL)
		return false;
	// for both http and websocket
	mg_set_protocol_http_websocket(connection);

	printf("starting http server at port: %s\n", m_port.c_str());
	// loop
	while (true)
		mg_mgr_poll(&m_mgr, 500); // ms

	return true;
}

void HttpServer::OnHttpWebsocketEvent(mg_connection* connection, int event_type, void* event_data)
{
	// Çø·ÖhttpºÍwebsocket
	if (event_type == MG_EV_HTTP_REQUEST)
	{
		http_message* http_req = (http_message*)event_data;
		HandleHttpEvent(connection, http_req);
	}
	else if (event_type == MG_EV_WEBSOCKET_HANDSHAKE_DONE ||
		event_type == MG_EV_WEBSOCKET_FRAME ||
		event_type == MG_EV_CLOSE)
	{
		websocket_message* ws_message = (struct websocket_message*)event_data;
		HandleWebsocketMessage(connection, event_type, ws_message);
	}
}

// ---- simple http ---- //
static bool route_check(http_message* http_msg, const char* route_prefix)
{
	if (mg_vcmp(&http_msg->uri, route_prefix) == 0) {
		return true;
	}
	else
		return false;



	// TODO: »¹¿ÉÒÔÅÐ¶Ï GET, POST, PUT, DELTEµÈ·½·¨
	//mg_vcmp(&http_msg->method, "GET");
	//mg_vcmp(&http_msg->method, "POST");
	//mg_vcmp(&http_msg->method, "PUT");
	//mg_vcmp(&http_msg->method, "DELETE");
}

void HttpServer::AddHandler(const std::string& url, ReqHandler req_handler)
{
	if (s_handler_map.find(url) != s_handler_map.end())
		return;

	s_handler_map.insert(std::make_pair(url, req_handler));
}

void HttpServer::RemoveHandler(const std::string& url)
{
	auto it = s_handler_map.find(url);
	if (it != s_handler_map.end())
		s_handler_map.erase(it);
}

void HttpServer::SendHttpRsp(mg_connection* connection, std::string rsp)
{
	/* CORS
	mg_printf(connection, "%s", "HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");

	mg_printf_http_chunk(connection, "%s", rsp.c_str());

	mg_send_http_chunk(connection, "", 0);
	*/
	// --- ¿ªÆôCORS
	/**/
	mg_printf(connection, "HTTP/1.1 200 OK\r\n"
		"Content-Type: text/html\n"
		"Cache-Control: no-cache\n"
		"Content-Length: %d\n"
		"Access-Control-Allow-Origin: *\n\n"
		"%s\n", rsp.length(), rsp.c_str());
}

void HttpServer::HandleHttpEvent(mg_connection* connection, http_message* http_req)
{
	std::string req_str = std::string(http_req->message.p, http_req->message.len);
	printf("got request: %s\n", req_str.c_str());

	std::string url = std::string(http_req->uri.p, http_req->uri.len);
	std::string body = std::string(http_req->body.p, http_req->body.len);
	auto it = s_handler_map.find(url);
	if (it != s_handler_map.end())
	{
		ReqHandler handle_func = it->second;
		handle_func(url, body, connection, &HttpServer::SendHttpRsp);
	}


	if (route_check(http_req, "/")) // index page
		mg_serve_http(connection, http_req, s_server_option);
	else if (route_check(http_req, "/api/hello"))
	{
		SendHttpRsp(connection, "welcome to httpserver");
	}
	else if (route_check(http_req, "/api/sum"))
	{

		char n1[100], n2[100];
		double result;

		/* Get form variables */
		mg_get_http_var(&http_req->body, "n1", n1, sizeof(n1));
		mg_get_http_var(&http_req->body, "n2", n2, sizeof(n2));

		/* Compute the result and send it back as a JSON object */
		result = strtod(n1, NULL) + strtod(n2, NULL);
		SendHttpRsp(connection, std::to_string(result));
	}
	else if (route_check(http_req, "/NSBase64Encode")) {
		DWORD* errorCode = new DWORD();
		string textData;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		int pos = postData.find_first_of("=");
		string key = postData.substr(0, pos);//key=value pos=3
		string value = postData.substr(pos + 1, postData.size() - pos - 1);//9
		HttpServer::agent.IWSABase64EncodeIS(errorCode, textData, (PBYTE)value.c_str(), value.size());
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"TextData\":\"" + textData + "\",\"errorCode\":\"" + strErrorCode + "\"}]");
	}
	else if (route_check(http_req, "/NSBase64Decode")) {
		DWORD* errorCode = new DWORD();
		BYTE* textData;
		DWORD textDataLen;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len))); // il est conseillé de decoder deux fois au fait que le plaintext est encodé deux fois.
		vector<string> param_vector;
		int pos = postData.find_first_of("=");
		string key = postData.substr(0, pos);//key=value pos=3
		string value = postData.substr(pos + 1, postData.size() - pos - 1);//9
		HttpServer::agent.IWSABase64DecodeIS(errorCode, textData, textDataLen, value);
		string TextData = string((char*)textData, textDataLen);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"TextData\":\"" + TextData + "\",\"errorCode\":\"" + strErrorCode + "\"}]");
	}
	else if (route_check(http_req, "/NSDetachedSign")) {
		DWORD* errorCode = new DWORD();
		string signedData;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		const BYTE* pPlainText = (PBYTE)param_map.at("PlainText").c_str();
		HttpServer::agent.IWSADetachedSignIS(errorCode, signedData, param_map.at("PortGrade"), pPlainText, param_map.at("PlainText").size(), atol(param_map.at("CertIndex").c_str()), FALSE, param_map.at("DigestArithmetic"), HttpServer::agent.defaultSignCertStore);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"signedData\":\"" + signedData + "\"}]");
	}
	
	else if (route_check(http_req, "/NSDetachedVerify")) {
		DWORD* errorCode = new DWORD();
		string signedData;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len))); // Decoder deux fois pour le cas où l'encodage est fait deux fois
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		const BYTE* pPlainText = (PBYTE)param_map.at("PlainText").c_str();
		HttpServer::agent.IWSADetachedVerifyIS(errorCode, param_map.at("PortGrade"), param_map.at("signedMsg"), pPlainText, param_map.at("PlainText").size());
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\"}]");
	}
	else if (route_check(http_req, "/NSEncryptedEnvelop")) {
		DWORD* errorCode = new DWORD();
		string envelopedMsg, certDN;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		const BYTE* pPlainText = (BYTE*)param_map.at("PlainText").c_str();
		HttpServer::agent.IWSAEncryptedEnvelopIS(errorCode, envelopedMsg, pPlainText, param_map.at("PlainText").size(), atol(param_map.at("CertIndex").c_str()), param_map.at("SymmetryArithmetic"));
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"envelopedMsg\":\"" + envelopedMsg + "\"}]");
	}
	else if (route_check(http_req, "/NSDecryptEnvelop")) {
		DWORD* errorCode = new DWORD();
		string plainText, certDN;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSADecryptEnvelopIS(errorCode, plainText, certDN, param_map.at("envelopedMsg"));
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"PlainText\":\"" + plainText + "\",\"EncryptcertDN\":\"" + certDN + "\"}]");
	}	
	else if (route_check(http_req, "/NSEncryptedSignEnvelop")) {
		DWORD* errorCode = new DWORD();
		string envelopedMsg, certDN;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		const BYTE* pPlainText = (BYTE*)param_map.at("PlainText").c_str();
		HttpServer::agent.IWSAEncryptedSignEnvelopIS(errorCode, envelopedMsg, pPlainText, param_map.at("PlainText").size(), atol(param_map.at("SignCertIndex").c_str()), atol(param_map.at("EncryptCertIndex").c_str()), param_map.at("DigestArithmetic"), param_map.at("SymmetryArithmetic"));
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"envelopedMsg\":\"" + envelopedMsg + "\"}]");
	}
	else if (route_check(http_req, "/NSDecryptSignEnvelop")) {
		DWORD* errorCode = new DWORD();
		string plainText, signCertDN, encryptCertDN;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSADecryptSignEnvelopIS(errorCode, plainText, signCertDN, encryptCertDN, param_map.at("envelopedMsg"));
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"PlainText\":\"" + plainText + "\",\"SigncertDN\":\"" + signCertDN + "\",\"EncryptcertDN\":\"" + encryptCertDN + "\"}]");
	}
	else if (route_check(http_req, "/NSGetCertsListInfo")) {
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, HttpServer::agent.IWSAGetAllCertsListInfoIS("", param_map["CertStore"], atol(param_map["Keyspec"].c_str())));
	}
	else if (route_check(http_req, "/NSGetCertPublicKeyInfoForIndex")) {
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		;
		SendHttpRsp(connection, "[{\"CertPublicKeyInfoData\":\"" + HttpServer::agent.IWSAGetCertPublicKeyInfoForIndexIS(atol(param_map["CertIndex"].c_str())) + "\"}]");
	} 
	else if (route_check(http_req, "/NSGetRsaCspListProvider")) {
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}	
		SendHttpRsp(connection, HttpServer::agent.IWSA_rsa_csp_listProviderIS());
	}
	else if (route_check(http_req, "/NSAdvRsaCspGenContainerP10")) {
		DWORD* errorCode = new DWORD();
		BOOL b;
		string p10Value;
		string encKeyPair;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		if ("true" == param_map.at("bDoubleCert")) b = TRUE;
		else b = FALSE;
		HttpServer::agent.IWSA_rsa_csp_AdvgenContainerP10IS(errorCode, p10Value, encKeyPair, param_map.at("IKeySize"), param_map.at("DN"), b);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"P10Value\":\"" + p10Value + "\",\"EncKeyPair\":\"" + encKeyPair + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspImportSignEncX509Cert")) {
		// Test only:
		cout << "===============TEST UKEK=================" << endl;
		cout << HttpServer::agent.IWSA_rsa_csp_GenUKEK() << endl;
		cout << "=========================================" << endl;
		DWORD* errorCode = new DWORD();
		string p10Value;
		string encKeyPair;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSA_rsa_csp_AdvImportSignEncX509CertIS(errorCode, param_map.at("CspName"), param_map.at("SignCert_Base64"), param_map.at("EncCert_Base64"), param_map.at("EncPrikey_Base64"), param_map.at("UKEK_Base64"));
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspImportSignEncP7Cert")) {
		// Test only:
		cout << "===============TEST UKEK=================" << endl;
		cout << HttpServer::agent.IWSA_rsa_csp_GenUKEK() << endl;
		cout << "=========================================" << endl;
		DWORD* errorCode = new DWORD();
		string p10Value;
		string encKeyPair;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSA_rsa_csp_AdvImportSignEncP7CertIS(errorCode, param_map.at("CspName"), param_map.at("SignCert_Base64"), param_map.at("EncCert_Base64"), param_map.at("EncPrikey_Base64"), param_map.at("UKEK_Base64"));
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\"}]");
	}
	else if (route_check(http_req, "/NSGetRsaCspCountOfCert")) {
		SendHttpRsp(connection, "[{\"CertCount\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_getCountOfCertIS()) + "\"}]");
	}
	else if (route_check(http_req, "/NSGetRsaCspCertInfo")) {
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, HttpServer::agent.IWSA_rsa_csp_getCertInfoIS(atol(param_map.at("CertIndex").c_str())));
	}	
	else if (route_check(http_req, "/NSRsaCspDelContainer")) { // delete the cert
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_delContainerIS(param_map.at("CspNamedel"), param_map.at("Container"))) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspDeleteContainer")) { // not delete the cert, delete only the container
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_deleteContainerIS(param_map.at("CspName"), param_map.at("Container"))) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspCreateContainer")) {
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_createContainerIS(param_map.at("CspName"), param_map.at("Container"))) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspGenContainer")) {
		DWORD* errorCode = new DWORD();
		string container;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSA_rsa_csp_genContainerIS(errorCode, container);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"Container\":\"" + container +  "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspGenContainer")) {
		DWORD* errorCode = new DWORD();
		string container;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSA_rsa_csp_genContainerIS(errorCode, container);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();	
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"Container\":\"" + container + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspGenP10")) {
		DWORD* errorCode = new DWORD();
		string p10;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bSign, bExport, bProtect;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		if ("true" == param_map.at("bSign")) bSign = TRUE;
		else bSign = FALSE;
		if ("true" == param_map.at("bExport")) bExport = TRUE;
		else bExport = FALSE;
		if ("true" == param_map.at("bProtect")) bProtect = TRUE;
		else bProtect = FALSE;
		HttpServer::agent.IWSA_rsa_csp_genP10IS(errorCode, p10, param_map.at("Container"), bSign, param_map.at("KeySize"), param_map.at("DN"), param_map.at("DigestOID"), param_map.at("PubKeyAlgOID"), param_map.at("SignAlgOID"), bExport, bProtect);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"Data\":\"" + p10 + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspGenContainerP10")) {
		DWORD* errorCode = new DWORD();
		string p10, container;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bSign, bExport, bProtect;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		if ("true" == param_map.at("bSign")) bSign = TRUE;
		else bSign = FALSE;
		if ("true" == param_map.at("bExport")) bExport = TRUE;
		else bExport = FALSE;
		if ("true" == param_map.at("bProtect")) bProtect = TRUE;
		else bProtect = FALSE;
		HttpServer::agent.IWSA_rsa_csp_genContainerP10IS(errorCode, container, p10, bSign, param_map.at("KeySize"), param_map.at("DN"), param_map.at("DigestOID"), param_map.at("PubKeyAlgOID"), param_map.at("SignAlgOID"), bExport, bProtect);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(*errorCode) + "\", \"Container\":\"" + container + "\",\"P10\":\"" + p10 + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspGenEncKeyPair")) { // CspName unused
		DWORD* errorCode = new DWORD();
		string data;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSA_rsa_csp_genEncKeyPairIS(errorCode, data);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"Data\":\"" + data + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspDelEncKeyPair")) { // CspName unused
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_delEncKeyPairIS()) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspImportX509CertToStore")) { // CspName unused
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bRoot;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		if ("true" == param_map.at("bRoot")) bRoot = TRUE;
		else bRoot = FALSE;
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_importX509CertToStoreIS(param_map.at("X509Cert"), bRoot)) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspDelX509CertInStore")) { // CspName unused
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bRoot;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_delX509CertInStoreIS(param_map.at("Issuer"), param_map.at("Serial"))) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspImportSignX509Cert")) { // CspName unused
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bRoot;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_importSignX509CertIS(param_map.at("Container"), param_map.at("X509Cert"))) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspImportEncX509Cert")) { // CspName unused
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bExport, bProtect;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		if ("true" == param_map.at("bExport")) bExport = TRUE;
		else bExport = FALSE;
		if ("true" == param_map.at("bProtect")) bProtect = TRUE;
		else bProtect = FALSE;
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_importEncX509CertIS(param_map.at("Container"), param_map.at("X509Cert"), param_map.at("EncPrikey"), param_map.at("UKEK"), bExport, bProtect)) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspImportSignP7Cert")) { // CspName unused
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bRoot;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_importSignP7CertIS(param_map.at("Container"), param_map.at("P7Cert"))) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspImportEncP7Cert")) { // CspName unused
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bExport, bProtect;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		if ("true" == param_map.at("bExport")) bExport = TRUE;
		else bExport = FALSE;
		if ("true" == param_map.at("bProtect")) bProtect = TRUE;
		else bProtect = FALSE;
		SendHttpRsp(connection, "[{\"errorCode\":\"" + to_string(HttpServer::agent.IWSA_rsa_csp_importEncP7CertIS(param_map.at("Container"), param_map.at("P7Cert"), param_map.at("EncPrikey"), param_map.at("UKEK"), bExport, bProtect)) + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspExportSignX509Cert")) { // CspName unused
		DWORD* errorCode = new DWORD();
		string data;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bRoot;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSA_rsa_csp_exportSignX509CertIS(errorCode, data, param_map.at("Container"));
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"Data\":\"" + data + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspExportEncX509Cert")) { // CspName unused
		DWORD* errorCode = new DWORD();
		string data;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bRoot;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		HttpServer::agent.IWSA_rsa_csp_exportEncX509CertIS(errorCode, data, param_map.at("Container"));
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"Data\":\"" + data + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspExportPfxCert")) { // CspName unused
		DWORD* errorCode = new DWORD();
		string data;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bSaveAs;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		if ("true" == param_map.at("bSaveAs")) bSaveAs = TRUE;
		else bSaveAs = FALSE;
		HttpServer::agent.IWSA_rsa_csp_exportPfxCertIS(errorCode, data, param_map.at("Issuer"),param_map.at("Serial"),param_map.at("Password"), bSaveAs);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"Data\":\"" + data + "\"}]");
	}
	else if (route_check(http_req, "/NSRsaCspExportContainerPfxCert")) { // CspName unused
		DWORD* errorCode = new DWORD();
		string data;
		string postData = uri::decode(uri::decode(string(http_req->body.p, http_req->body.len)));
		vector<string> param_vector;
		map<string, string> param_map;
		BOOL bSignCert, bSaveAs;
		tokenize(postData, '&', param_vector);
		for (string p : param_vector) {
			int pos = p.find_first_of("=");
			param_map.insert(pair<string, string>(p.substr(0, pos), p.substr(pos + 1, p.size() - pos - 1)));
		}
		if ("true" == param_map.at("bSignCert")) bSignCert = TRUE;
		else bSignCert = FALSE;
		if ("true" == param_map.at("bSaveAs")) bSaveAs = TRUE;
		else bSaveAs = FALSE;
		HttpServer::agent.IWSA_rsa_csp_exportContainerPfxCertIS(errorCode, data, param_map.at("Container"), bSignCert, param_map.at("Password"), bSaveAs);
		ostringstream stream;
		stream << *errorCode;
		string strErrorCode = stream.str();
		SendHttpRsp(connection, "[{\"errorCode\":\"" + strErrorCode + "\",\"Data\":\"" + data + "\"}]");
	}
	else
	{
		mg_printf(connection, "%s", "HTTP/1.1 501 Not Implemented\r\nContent-Length: 0\r\n\r\n");
	}
}

// ---- websocket ---- //
int HttpServer::isWebsocket(const mg_connection* connection)
{
	return connection->flags & MG_F_IS_WEBSOCKET;
}

void HttpServer::HandleWebsocketMessage(mg_connection* connection, int event_type, websocket_message* ws_msg)
{
	if (event_type == MG_EV_WEBSOCKET_HANDSHAKE_DONE)
	{
		printf("client websocket connected\n");
		// »ñÈ¡Á¬½Ó¿Í»§¶ËµÄIPºÍ¶Ë¿Ú
		char addr[32];
		mg_sock_addr_to_str(&connection->sa, addr, sizeof(addr), MG_SOCK_STRINGIFY_IP | MG_SOCK_STRINGIFY_PORT);
		printf("client addr: %s\n", addr);

		// Ìí¼Ó session
		s_websocket_session_set.insert(connection);

		SendWebsocketMsg(connection, "client websocket connected");
	}
	else if (event_type == MG_EV_WEBSOCKET_FRAME)
	{
		mg_str received_msg = {
			(char*)ws_msg->data, ws_msg->size
		};

		char buff[1024] = { 0 };
		strncpy(buff, received_msg.p, received_msg.len); // must use strncpy, specifiy memory pointer and length

		// do sth to process request
		printf("received msg: %s\n", buff);
		SendWebsocketMsg(connection, "send your msg back: " + std::string(buff));
		//BroadcastWebsocketMsg("broadcast msg: " + std::string(buff));
	}
	else if (event_type == MG_EV_CLOSE)
	{
		if (isWebsocket(connection))
		{
			printf("client websocket closed\n");
			// ÒÆ³ýsession
			if (s_websocket_session_set.find(connection) != s_websocket_session_set.end())
				s_websocket_session_set.erase(connection);
		}
	}
}

void HttpServer::SendWebsocketMsg(mg_connection* connection, std::string msg)
{
	mg_send_websocket_frame(connection, WEBSOCKET_OP_TEXT, msg.c_str(), strlen(msg.c_str()));
}

void HttpServer::BroadcastWebsocketMsg(std::string msg)
{
	for (mg_connection* connection : s_websocket_session_set)
		mg_send_websocket_frame(connection, WEBSOCKET_OP_TEXT, msg.c_str(), strlen(msg.c_str()));
}

bool HttpServer::Close()
{
	mg_mgr_free(&m_mgr);
	return true;
}