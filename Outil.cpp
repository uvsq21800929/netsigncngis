#include<iostream>
#include<string>
#include<vector>
#include<windows.h>
#include <assert.h>
using namespace std;
void tokenize(const string& str, const char delim, vector<string>& out)
{
    // https://www.codegrepper.com/code-examples/delphi/c%2B%2B+split+string+by+delimiter
    size_t start;
    size_t end = 0;

    while ((start = str.find_first_not_of(delim, end)) != string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
}

// Adapter depuis https://docs.microsoft.com/en-us/windows/win32/seccrypto/bytetostr
void ByteToStr( const DWORD cb,const void* pv,string& sz)
    //--------------------------------------------------------------------
    // Parameters passed are:
    //    pv is the array of BYTES to be converted.
    //    cb is the number of BYTEs in the array.
    //    sz is a pointer to the string to be returned.

{
    //--------------------------------------------------------------------
    //  Declare and initialize local variables.

    BYTE* pb = (BYTE*)pv; // local pointer to a BYTE in the BYTE array
    DWORD i;               // local loop counter
    int b;                 // local variable

    //--------------------------------------------------------------------
    //  Begin processing loop.

    for (i = 0; i < cb; i++)
    {
        b = (*pb & 0xF0) >> 4;
        sz += (b <= 9) ? b + '0' : (b - 10) + 'A';
        b = *pb & 0x0F;
        sz += (b <= 9) ? b + '0' : (b - 10) + 'A';
        sz += ' ';
        pb++;
    }
}

// Adapter depuis https://docs.microsoft.com/en-us/windows/win32/seccrypto/bytetostr
void ByteToStrRandom(const DWORD cb, const void* pv, string& sz)
//--------------------------------------------------------------------
// Parameters passed are:
//    pv is the array of BYTES to be converted.
//    cb is the number of BYTEs in the array.
//    sz is a pointer to the string to be returned.

{
    //--------------------------------------------------------------------
    //  Declare and initialize local variables.

    BYTE* pb = (BYTE*)pv; // local pointer to a BYTE in the BYTE array
    DWORD i;               // local loop counter
    int b;                 // local variable

    //--------------------------------------------------------------------
    //  Begin processing loop.

    for (i = 0; i < cb; i++)
    {
        b = (*pb & 0xF0) >> 4;
        sz += (b <= 9) ? b + '0' : (b - 10) + 'A';
        b = *pb & 0x0F;
        sz += (b <= 9) ? b + '0' : (b - 10) + 'A';
        pb++;
    }
}

void vectorToStr(string& str, const vector<string> vec) {
    str = "[";
    for (string s : vec) {
        str += s;
        str += ",";
    }
    if (vec.size() > 0) str.replace(str.end() - 1, str.end(), "]");
    else str += "]";
}

unsigned char FromHex(unsigned char x)
{
    unsigned char y;
    if (x >= 'A' && x <= 'Z') y = x - 'A' + 10;
    else if (x >= 'a' && x <= 'z') y = x - 'a' + 10;
    else if (x >= '0' && x <= '9') y = x - '0';
    else assert(0);
    return y;
}

string UrlDecode(const string& str)
{
    std::string strTemp = "";
    size_t length = str.length();
    for (size_t i = 0; i < length; i++)
    {
        if (str[i] == '+') strTemp += ' ';
        else if (str[i] == '%')
        {
            assert(i + 2 < length);
            unsigned char high = FromHex((unsigned char)str[++i]);
            unsigned char low = FromHex((unsigned char)str[++i]);
            strTemp += high * 16 + low;
        }
        else strTemp += str[i];
    }
    return strTemp;
}