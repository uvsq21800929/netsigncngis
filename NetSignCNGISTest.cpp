#pragma once
#include "gmpxx.h"
#include "CertsListInfoIS.h"
#include "IWSAgentIS.h"
#include "ErrorHandler.h"
#include "Constants.h"
/*
void testIWSAGetAllCertsListInfoISMY0() {
	IWSAGetAllCertsListInfoIS("","MY",0);
}

void testIWSAGetAllCertsListInfoDetailISMY0() {
	IWSAGetAllCertsListInfoDetailIS("", "MY", 0);
}*/


const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
const DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
const string recipientCertStore = ADDRESSBOOK;
const string signerCertStore = MY;
const string digestArithmetic = SHA256;
const string symmetryArithmetic = TDES;
const DWORD recipientCertIndex = 7;
const DWORD signerCertIndex = 0;
const string recipientDefaultDN = "FR, Ldf, Paris, Infosec, Dpt_Buz, User Client Encryption Infosec, enc@infosec.com";    
const string signerDefaultDN = "FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com";

const string form1 = "Infosec form1";
const string form2 = "Infosec form2";

const string filePath1 = "example.txt";
const string filePath2 = PRIVATE_KEY_RSA_1;

string plainText;
string certDN;
DWORD* errorCode = new DWORD();
string envelopedMsg;

IWSAgentIS agent = IWSAgentIS();




void testIWSAGetAllCertsListInfoISNULL() {
	string ret = IWSAGetAllCertsListInfoIS("", "", 0);
	if ("[]" != ret) MyCertHandleError("[x] IWSAGetAllCertsListInfoIS NULL Test Failed.");
}

void testIWSAGetAllCertsListInfoISMY0() {
	IWSAGetAllCertsListInfoIS("", "MY", 0);
}

void testIWSAGetAllCertsListInfoISMY2() {
	string ret = IWSAGetAllCertsListInfoIS("", "MY", 2);
	if (ret.find("encryption") != string::npos || ret.find("envelope") != string::npos) MyCertHandleError("[x] IWSAGetAllCertsListInfoISMY2 Filter Test Failed.");
}

void testIWSAGetAllCertsListInfoISCA0() {
	IWSAGetAllCertsListInfoIS("", "CA", 0);
}

void testIWSAGetAllCertsListInfoISCA2() {
	string ret = IWSAGetAllCertsListInfoIS("", "CA", 2);
	if (ret.find("encryption") != string::npos || ret.find("envelope") != string::npos) MyCertHandleError("[x] IWSAGetAllCertsListInfoISCA2 Filter Test Failed.");
}

void testIWSAGetAllCertsListInfoISROOT0() {
	IWSAGetAllCertsListInfoIS("", "ROOT", 0);
}

void testIWSAGetAllCertsListInfoISROOT2() {
	string ret = IWSAGetAllCertsListInfoIS("", "ROOT", 2);
	if (ret.find("encryption") != string::npos || ret.find("envelope") != string::npos) MyCertHandleError("[x] IWSAGetAllCertsListInfoISROOT2 Filter Test Failed.");
}

void testIWSAGetAllCertsListInfoISTRUST0() {
	IWSAGetAllCertsListInfoIS("", "TRUST", 0);
}

void testIWSAGetAllCertsListInfoISTRUST2() {
	string ret = IWSAGetAllCertsListInfoIS("", "TRUST", 2);
	if (ret.find("encryption") != string::npos || ret.find("envelope") != string::npos) MyCertHandleError("[x] IWSAGetAllCertsListInfoISTRUST2 Filter Test Failed.");
}

void testIWSAGetAllCertsListInfoISADDRESSBOOK0() {
    IWSAGetAllCertsListInfoIS("", "AddressBook", 0);
}

void testIWSAGetAllCertsListInfoISADDRESSBOOK2() {
    string ret = IWSAGetAllCertsListInfoIS("", "AddressBook", 2);
    if (ret.find("encryption") != string::npos || ret.find("envelope") != string::npos) MyCertHandleError("[x] IWSAGetAllCertsListInfoISADDRESSBOOK2 Filter Test Failed.");
}

void testSignAndEncryptIS() {
    //---------------------------------------------------------------
   // Declare and initialize local variables.
   //---------------------------------------------------------------
   //  pbToBeSignedAndEncrypted is the message to be 
   //  encrypted and signed.
    const BYTE* pbToBeSignedAndEncrypted = (const unsigned char*)"Insert the message to be signed here";
    //---------------------------------------------------------------
    // This is the length of the message to be
    // encrypted and signed. Note that it is one
    // more that the length returned by strlen()
    // to include the terminating null character.
    DWORD cbToBeSignedAndEncrypted = lstrlenA((const char*)pbToBeSignedAndEncrypted) + 1;

    //---------------------------------------------------------------
    // Pointer to a buffer that will hold the
    // encrypted and signed message.

    BYTE* pbSignedAndEncryptedBlob;

    //---------------------------------------------------------------
    // A DWORD to hold the length of the signed 
    // and encrypted message.

    DWORD cbSignedAndEncryptedBlob;
    BYTE* pReturnMessage;

    //---------------------------------------------------------------
    // Call the local function SignAndEncrypt.
    // This function returns a pointer to the 
    // signed and encrypted BLOB and also returns
    // the length of that BLOB.

    LPCSTR signerSubject = "FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com";
    LPCSTR recipientSubject = "FR, Ldf, Paris, Infosec, Dpt_Buz, User Client Encryption Infosec, enc@infosec.com";
    LPCSTR signerCertStore = "MY";
    LPCSTR recipientCertStore = "AddressBook";

    pbSignedAndEncryptedBlob = SignAndEncryptIS(pbToBeSignedAndEncrypted, cbToBeSignedAndEncrypted, &cbSignedAndEncryptedBlob, signerSubject, signerCertStore, recipientSubject, recipientCertStore);

}



void testEncryptIS() {
    //---------------------------------------------------------------
   // Declare and initialize local variables.
   //---------------------------------------------------------------
   //  pbToBeSignedAndEncrypted is the message to be 
   //  encrypted and signed.
    const BYTE* pbToBeEncrypted = (const unsigned char*)"Insert the message to be encrypted here";
    //---------------------------------------------------------------
    // This is the length of the message to be
    // encrypted and signed. Note that it is one
    // more that the length returned by strlen()
    // to include the terminating null character.
    DWORD cbToBeEncrypted = lstrlenA((const char*)pbToBeEncrypted) + 1;

    //---------------------------------------------------------------
    // Pointer to a buffer that will hold the
    // encrypted and signed message.

    BYTE* pbEncryptedBlob;

    //---------------------------------------------------------------
    // A DWORD to hold the length of the signed 
    // and encrypted message.

    DWORD cbEncryptedBlob;
    BYTE* pReturnMessage;

    //---------------------------------------------------------------
    // Call the local function SignAndEncrypt.
    // This function returns a pointer to the 
    // signed and encrypted BLOB and also returns
    // the length of that BLOB.

    LPCSTR recipientSubject = "FR, Ldf, Paris, Infosec, Dpt_Buz, User Client Encryption Infosec, enc@infosec.com";
    LPCSTR certStore = "AddressBook";
    pbEncryptedBlob = EncryptIS(pbToBeEncrypted, cbToBeEncrypted, &cbEncryptedBlob, recipientSubject, certStore);

}


void testBase64EncodeIS() {
    const BYTE* pbToBeEncoded = (const unsigned char*)"Insert the message to be encoded here";
    const DWORD cbToBeEncoded = strlen((const char*)pbToBeEncoded) + 1;
    DWORD* errorCode = new DWORD();
    string returnMessage;
    IWSABase64EncodeIS(errorCode, returnMessage, pbToBeEncoded, cbToBeEncoded);
    if (*errorCode != 0 || returnMessage.size() == 0) MyCertHandleError("[x] Base64Encode failed.\n");
}

void testBase64DecodeIS() {
    const BYTE* pbToBeEncoded = (const unsigned char*)"Insert the message to be encoded here";
    const DWORD len = strlen((const char*)pbToBeEncoded) + 1;//inclure \0
    BYTE* pbDecoded;
    DWORD cbBinary;
    DWORD* errorCode = new DWORD();
    string returnMessage;
    IWSABase64EncodeIS(errorCode, returnMessage, pbToBeEncoded, len);
    if (*errorCode != 0 || returnMessage.size() == 0) MyCertHandleError("[x] Base64Encode failed.\n");
    IWSABase64DecodeIS(errorCode, pbDecoded, cbBinary, returnMessage);
    string s1(reinterpret_cast<char const*>(pbToBeEncoded), len);
    string s2(reinterpret_cast<char const*>(pbDecoded), cbBinary);
    if (*errorCode != 0 || s1 != s2) MyCertHandleError("[x] Base64Decode failed.\n");
}


void testSignIS() {
    //---------------------------------------------------------------
   // Declare and initialize local variables.
   //---------------------------------------------------------------
   //  pbToBeSignedAndEncrypted is the message to be 
   //  encrypted and signed.
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    //---------------------------------------------------------------
    // This is the length of the message to be
    // encrypted and signed. Note that it is one
    // more that the length returned by strlen()
    // to include the terminating null character.
    DWORD cbToBeSigned = lstrlenA((const char*)pbToBeSigned) + 1;

    //---------------------------------------------------------------
    // Pointer to a buffer that will hold the
    // encrypted and signed message.

    BYTE* pbSignedBlob;

    //---------------------------------------------------------------
    // A DWORD to hold the length of the signed 
    // and encrypted message.

    DWORD cbSignedBlob;

    //---------------------------------------------------------------
    // Call the local function SignIS.
    // This function returns a pointer to the 
    // signed and encrypted BLOB and also returns
    // the length of that BLOB.

    string signerSubject = "FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com";
    string signerCertStore = "MY";
    string digestArithmetic = "SHA256";

    BOOL ret = SignIS(pbToBeSigned, cbToBeSigned, pbSignedBlob, cbSignedBlob, signerSubject, FALSE, TRUE, digestArithmetic, signerCertStore);
    if (!ret) MyCertHandleError("[x] ERROR SingIS");
}

void testSignByIndexIS(){
    //---------------------------------------------------------------
  // Declare and initialize local variables.
  //---------------------------------------------------------------
  //  pbToBeSignedAndEncrypted is the message to be 
  //  encrypted and signed.
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    //---------------------------------------------------------------
    // This is the length of the message to be
    // encrypted and signed. Note that it is one
    // more that the length returned by strlen()
    // to include the terminating null character.
    DWORD cbToBeSigned = lstrlenA((const char*)pbToBeSigned) + 1;

    //---------------------------------------------------------------
    // Pointer to a buffer that will hold the
    // encrypted and signed message.

    BYTE* pbSignedBlob;

    //---------------------------------------------------------------
    // A DWORD to hold the length of the signed 
    // and encrypted message.

    DWORD cbSignedBlob;

    //---------------------------------------------------------------
    // Call the local function SignIS.
    // This function returns a pointer to the 
    // signed and encrypted BLOB and also returns
    // the length of that BLOB.

    string signerCertStore = "MY";
    string digestArithmetic = "SHA256";
    DWORD signerCertIndex = 0;

    BOOL ret = SignIS(pbToBeSigned, cbToBeSigned, pbSignedBlob, cbSignedBlob, signerCertIndex, FALSE, TRUE, digestArithmetic, signerCertStore);
    if (!ret) MyCertHandleError("[x] ERROR SingIS");
}

void testDetachedSignIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"1234";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned);
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode=new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    IWSADetachedSignIS(errorCode, signedData, portGrade, pbToBeSigned, cbToBeSigned, signerCertIndex, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Detached Sign failed.");
}

void testDetachedSignDefaultDNIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string digestArithmetic = SHA256;
    string signerSubject = "FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com";
    IWSADetachedSignDefaultDNIS(errorCode, signedData, pbToBeSigned, cbToBeSigned, signerSubject, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Detached Sign DefaultDN failed.");
}

void testDetachedVerifyIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    IWSADetachedSignIS(errorCode, signedData, portGrade, pbToBeSigned, cbToBeSigned, signerCertIndex, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Detached Sign failed.");
    IWSADetachedVerifyIS(errorCode, portGrade, signedData, pbToBeSigned, cbToBeSigned);
    if (*errorCode) MyCertHandleError("[x] Detached Verify failed.");
}

void testAttachedSignIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    IWSAAttachedSignIS(errorCode, signedData, portGrade, pbToBeSigned, cbToBeSigned, signerCertIndex, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Attached Sign failed.");
}

void testAttachedSignDefaultDNIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    string signerSubject = "FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com";
    IWSADetachedSignDefaultDNIS(errorCode, signedData, pbToBeSigned, cbToBeSigned, signerSubject, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Attached Sign DefaultDN failed.");
}

void testAttachedVerifyIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    string plainText;
    string certDN;
    IWSAAttachedSignIS(errorCode, signedData, portGrade, pbToBeSigned, cbToBeSigned, signerCertIndex, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Attached Sign failed.");
    IWSAAttachedVerifyIS(errorCode, plainText, certDN, portGrade, signedData);
    if (*errorCode || 0 == plainText.size()|| 0 == certDN.size()) MyCertHandleError("[x] Attached Verify failed.");
}

void testRawSignIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    IWSARawSignIS(errorCode, signedData, portGrade, pbToBeSigned, cbToBeSigned, signerCertIndex, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Raw Sign failed.");
}

void testRawSignFilterCNGIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = MD5;
    IWSARawSignIS(errorCode, signedData, portGrade, pbToBeSigned, cbToBeSigned, signerCertIndex, TRUE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Raw Sign failed.");
}

void testRawSignDefaultDNCNGIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    string signerSubject = "FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com";
    IWSARawSignDefaultDNIS(errorCode, signedData, pbToBeSigned, cbToBeSigned, signerSubject, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Raw Sign failed.");
}

void testRawVerifyIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD signerCertIndex = 0;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    IWSARawSignIS(errorCode, signedData, portGrade, pbToBeSigned, cbToBeSigned, signerCertIndex, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Raw Sign failed.");
    IWSARawVerifyIS(errorCode, signedData, pbToBeSigned, cbToBeSigned, signerCertIndex, SHA256);
    if (*errorCode) MyCertHandleError("[x] Raw Verify failed.");
}

void testRawVerifyDefaultDNIS() {
    const BYTE* pbToBeSigned = (const unsigned char*)"Insert the message to be signed here";
    DWORD cbToBeSigned = strlen((const char*)pbToBeSigned) + 1;
    const string signerCertStore = MY;
    DWORD* errorCode = new DWORD();
    string signedData;
    const string portGrade = PORTGRADE1;
    const string digestArithmetic = SHA256;
    const DWORD signerCertIndex = 0;
    const string signerSubject = "FR, Ldf, Infosec, Dpt_Buz, User Client Sign, sing@infosec.com";
    IWSARawSignIS(errorCode, signedData, portGrade, pbToBeSigned, cbToBeSigned, signerCertIndex, FALSE, digestArithmetic, signerCertStore);
    if (*errorCode) MyCertHandleError("[x] Raw Sign failed.");
    IWSARawVerifyDefaultDNIS(errorCode, signedData, pbToBeSigned, cbToBeSigned, signerSubject, SHA256);
    if (*errorCode) MyCertHandleError("[x] Raw Verify failed.");
}

void testEncryptedEnvelopIS(){
    const BYTE* pbToBeEnveloped = (const unsigned char*)"Insert the message to be signed here";
    const DWORD cbToBeEnveloped = strlen((const char*)pbToBeEnveloped) + 1;
    const string recipientCertStore = ADDRESSBOOK;
    DWORD* errorCode = new DWORD();
    string envelopedMsg;

    const string digestArithmetic = RC4;
    const DWORD recipientCertIndex = 7;

    IWSAEncryptedEnvelopIS(errorCode, envelopedMsg, pbToBeEnveloped, cbToBeEnveloped, recipientCertIndex, digestArithmetic);
    if (*errorCode) MyCertHandleError("[x] EncryptedEnvelop failed.");
    
}

void testEncryptedEnvelopDESIVIS() {
    const BYTE* pbToBeEnveloped = (const unsigned char*)"Insert the message to be signed here";
    const DWORD cbToBeEnveloped = strlen((const char*)pbToBeEnveloped) + 1;
    const string recipientCertStore = ADDRESSBOOK;
    DWORD* errorCode = new DWORD();
    string envelopedMsg;

    const string digestArithmetic = DES;
    const DWORD recipientCertIndex = 7;

    IWSAEncryptedEnvelopIS(errorCode, envelopedMsg, pbToBeEnveloped, cbToBeEnveloped, recipientCertIndex, digestArithmetic);
    if (*errorCode) MyCertHandleError("[x] EncryptedEnvelop failed.");

}

void testEncryptedEnvelopDefaultDNIS() {
    const BYTE* pbToBeEnveloped = (const unsigned char*)"Insert the message to be signed here";
    const DWORD cbToBeEnveloped = strlen((const char*)pbToBeEnveloped) + 1;
    const string recipientCertStore = ADDRESSBOOK;
    DWORD* errorCode = new DWORD();
    string envelopedMsg;

    const string digestArithmetic = RC4;
    const string recipientCertSubject = "FR, Ldf, Paris, Infosec, Dpt_Buz, User Client Encryption Infosec, enc@infosec.com"; // 1.looing for substring 2.espace included

    IWSAEncryptedEnvelopDefaultDNIS(errorCode, envelopedMsg, pbToBeEnveloped, cbToBeEnveloped, recipientCertSubject, digestArithmetic);
    if (*errorCode) MyCertHandleError("[x] EncryptedEnvelop failed.");

}

void testDecryptEnvelopIS() {
    const BYTE* pbToBeEnveloped = (const unsigned char*)"1";
    const DWORD cbToBeEnveloped = strlen((const char*)pbToBeEnveloped);
    const string recipientCertStore = ADDRESSBOOK;
    DWORD* errorCode = new DWORD();
    string envelopedMsg;
    const string symmetryArithmetic = TDES;
    const DWORD recipientCertIndex = 0;
    string plainText;
    string certDN;
    IWSAgentIS agent;
    agent.IWSAEncryptedEnvelopIS(errorCode, envelopedMsg, pbToBeEnveloped, cbToBeEnveloped, recipientCertIndex, symmetryArithmetic);
    if (*errorCode) MyCertHandleError("[x] EncryptedEnvelop failed.");
    agent.IWSADecryptEnvelopIS(errorCode, plainText, certDN, envelopedMsg);
    if (*errorCode) MyCertHandleError("[x] DecryptEnvelop failed.");
}

void testEncryptedSignEnvelopIS(){
    IWSAEncryptedSignEnvelopIS(errorCode, envelopedMsg, pbToBeSigned, cbToBeSigned,  signerCertIndex,  recipientCertIndex,  digestArithmetic,  symmetryArithmetic);
    if (*errorCode) MyCertHandleError("[x] EncryptedSignEnvelop failed.");
}

void testEncryptedSignEnvelopDefaultDNIS() {
    IWSAEncryptedSignEnvelopDefaultDNIS(errorCode, envelopedMsg, pbToBeSigned, cbToBeSigned, signerDefaultDN, recipientDefaultDN, digestArithmetic, symmetryArithmetic);
    if (*errorCode) MyCertHandleError("[x] EncryptedSignEnvelopDefaultDN failed.");
}

void testDecryptSignEnvelopIS() {
    string signerDefaultDN, recipientDefaultDN, plainText;
    IWSAEncryptedSignEnvelopIS(errorCode, envelopedMsg, pbToBeSigned, cbToBeSigned, signerCertIndex, recipientCertIndex, digestArithmetic, symmetryArithmetic);
    if (*errorCode) MyCertHandleError("[x] EncryptedSignEnvelop failed.");
    IWSADecryptSignEnvelopIS(errorCode, plainText, signerDefaultDN, recipientDefaultDN, envelopedMsg);
    if (*errorCode) MyCertHandleError("[x] DecryptSignEnvelop failed.");
}

void testAddFormIS() {
    if (agent.getFormVector().size() != 0) {
        agent.getFormVector().clear();
    }
    agent.IWSAAddFormIS(form1);
    if (1 != agent.getFormVector().size()) { MyCertHandleError("[x] AddFormIS"); }
}

void testAddFormsIS() {
    if (agent.getFormVector().size() != 0) {
        agent.getFormVector().clear();
    }
    agent.IWSAAddFormIS(form1);
    agent.IWSAAddFormIS(form1);
    if (2 != agent.getFormVector().size()) { MyCertHandleError("[x] AddFormsIS"); }
}

void testAddFileIS() {
    if (agent.getFileVector().size() != 0) {
        agent.getFileVector().clear();
    }
    agent.IWSAAddFileIS(filePath1);
    if (1 != agent.getFileVector().size()) { MyCertHandleError("[x] AddFileIS"); }
    agent.IWSAClearFormListIS();
}

void testAddFilesIS() {
    if (agent.getFileVector().size() != 0) {
        agent.getFileVector().clear();
    }
    agent.IWSAAddFileIS(filePath1);
    agent.IWSAAddFileIS(filePath2);
    if (2 != agent.getFileVector().size()) { MyCertHandleError("[x] AddFilesIS"); }
    agent.IWSAClearFileListIS();
}

void testClearFormListIS() {
    agent.IWSAClearFormListIS();
    if (0 != agent.getFormVector().size()) { MyCertHandleError("[x] ClearFormListIS"); }

}

void testClearFileListIS() {
    agent.IWSAClearFileListIS();
    if (0 != agent.getFileVector().size()) { MyCertHandleError("[x] ClearFileListIS"); }
}

void testGetAlreadyFormListIS() {
    if ("[]" != agent.IWSAGetAlreadyFormListIS()) { MyCertHandleError("[x] GetAlreadyFormListIS"); }
    agent.IWSAAddFormIS(form1);
    if (2 >= agent.IWSAGetAlreadyFormListIS().size()) { MyCertHandleError("[x] GetAlreadyFormListIS"); }
    agent.IWSAClearFormListIS();
}

void testGetAlreadyFileListIS() {
    if ("[]" != agent.IWSAGetAlreadyFileListIS()) { MyCertHandleError("[x] GetAlreadyFileListIS"); }
    agent.IWSAAddFileIS(filePath1);
    if (2 >= agent.IWSAGetAlreadyFileListIS().size()) { MyCertHandleError("[x] GetAlreadyFileListIS"); }
    agent.IWSAClearFileListIS();
}







