#include "http_server.h" // premier http_server.h
#include "IWSAgentIS.h" // apres http_server.h

using namespace std;

int mainDebug(int argc, char* argv[])
{
	const BYTE* pbToBeEnveloped = (const unsigned char*)"1";
	const DWORD cbToBeEnveloped = strlen((const char*)pbToBeEnveloped);
	const string recipientCertStore = ADDRESSBOOK;
	DWORD* errorCode = new DWORD();
	string envelopedMsg;
	const string symmetryArithmetic = TDES;
	const DWORD recipientCertIndex = 0;
	string plainText;
	string certDN;
	IWSAgentIS agent;
	agent.IWSAEncryptedEnvelopIS(errorCode, envelopedMsg, pbToBeEnveloped, cbToBeEnveloped, recipientCertIndex, symmetryArithmetic);
	if (*errorCode) MyCertHandleError("[x] EncryptedEnvelop failed.");
	agent.IWSADecryptEnvelopIS(errorCode, plainText, certDN, envelopedMsg);
	if (*errorCode) MyCertHandleError("[x] DecryptEnvelop failed.");
	return 0;
}

